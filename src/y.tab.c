/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 26 "src/p.y"


/*
 * DESCRIPTION
 *   Simple context-free grammar for parsing the control file.
 *
 */

#include "config.h"

#ifdef HAVE_STDIO_H
#include <stdio.h>
#endif

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_ERRNO_H
#include <errno.h>
#endif

#ifdef HAVE_CTYPE_H
#include <ctype.h>
#endif

#ifdef HAVE_PWD_H
#include <pwd.h>
#endif

#ifdef HAVE_GRP_H
#include <grp.h>
#endif

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

#ifdef HAVE_TIME_H
#include <time.h>
#endif

#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif

#ifdef HAVE_ASM_PARAM_H
#include <asm/param.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif

#ifdef HAVE_NETDB_H
#include <netdb.h>
#endif

#ifdef HAVE_SYSLOG_H
#include <syslog.h>
#endif

#ifdef HAVE_NETINET_IN_SYSTM_H
#include <netinet/in_systm.h>
#endif

#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif

#ifdef HAVE_NETINET_IP_H
#include <netinet/ip.h>
#endif

#ifdef HAVE_NETINET_IP_ICMP_H
#include <netinet/ip_icmp.h>
#endif

#ifdef HAVE_REGEX_H
#include <regex.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_OPENSSL
#include <openssl/ssl.h>
#endif

#include "monit.h"
#include "protocol.h"
#include "engine.h"
#include "alert.h"
#include "ProcessTree.h"
#include "device.h"
#include "processor.h"
#include "md5.h"
#include "sha1.h"
#include "checksum.h"
#include "process_sysdep.h"

// libmonit
#include "io/File.h"
#include "util/Str.h"
#include "thread/Thread.h"


/* ------------------------------------------------------------- Definitions */


struct precedence_t {
        bool daemon;
        bool logfile;
        bool pidfile;
};

struct rate_t {
        unsigned int count;
        unsigned int cycles;
};

/* yacc interface */
void  yyerror(const char *,...) __attribute__((format (printf, 1, 2)));
void  yyerror2(const char *,...) __attribute__((format (printf, 1, 2)));
void  yywarning(const char *,...) __attribute__((format (printf, 1, 2)));
void  yywarning2(const char *,...) __attribute__((format (printf, 1, 2)));

/* lexer interface */
int yylex(void);
extern FILE *yyin;
extern int lineno;
extern int arglineno;
extern char *yytext;
extern char *argyytext;
extern char *currentfile;
extern char *argcurrentfile;
extern int buffer_stack_ptr;

/* Local variables */
static int cfg_errflag = 0;
static Service_T tail = NULL;
static Service_T current = NULL;
static Request_T urlrequest = NULL;
static command_t command = NULL;
static command_t command1 = NULL;
static command_t command2 = NULL;
static Service_T depend_list = NULL;
static struct Uid_T uidset = {};
static struct Gid_T gidset = {};
static struct Pid_T pidset = {};
static struct Pid_T ppidset = {};
static struct FsFlag_T fsflagset = {};
static struct NonExist_T nonexistset = {};
static struct Exist_T existset = {};
static struct Status_T statusset = {};
static struct Perm_T permset = {};
static struct Size_T sizeset = {};
static struct Uptime_T uptimeset = {};
static struct ResponseTime_T responsetimeset = {};
static struct LinkStatus_T linkstatusset = {};
static struct LinkSpeed_T linkspeedset = {};
static struct LinkSaturation_T linksaturationset = {};
static struct Bandwidth_T bandwidthset = {};
static struct Match_T matchset = {};
static struct Icmp_T icmpset = {};
static struct Mail_T mailset = {};
static struct SslOptions_T sslset = {};
static struct Port_T portset = {};
static struct MailServer_T mailserverset = {};
static struct Mmonit_T mmonitset = {};
static struct FileSystem_T filesystemset = {};
static struct Resource_T resourceset = {};
static struct Checksum_T checksumset = {};
static struct Timestamp_T timestampset = {};
static struct ActionRate_T actionrateset = {};
static struct precedence_t ihp = {false, false, false};
static struct rate_t rate = {1, 1};
static struct rate_t rate1 = {1, 1};
static struct rate_t rate2 = {1, 1};
static char * htpasswd_file = NULL;
static unsigned int repeat = 0;
static unsigned int repeat1 = 0;
static unsigned int repeat2 = 0;
static Digest_Type digesttype = Digest_Cleartext;

#define BITMAP_MAX (sizeof(long long) * 8)


/* -------------------------------------------------------------- Prototypes */

static void  preparse(void);
static void  postparse(void);
static bool _parseOutgoingAddress(char *ip, Outgoing_T *outgoing);
static void  addmail(char *, Mail_T, Mail_T *);
static Service_T createservice(Service_Type, char *, char *, State_Type (*)(Service_T));
static void  addservice(Service_T);
static void  adddependant(char *);
static void  addservicegroup(char *);
static void  addport(Port_T *, Port_T);
static void  addhttpheader(Port_T, char *);
static void  addresource(Resource_T);
static void  addtimestamp(Timestamp_T);
static void  addactionrate(ActionRate_T);
static void  addsize(Size_T);
static void  adduptime(Uptime_T);
static void  addpid(Pid_T);
static void  addppid(Pid_T);
static void  addfsflag(FsFlag_T);
static void  addnonexist(NonExist_T);
static void  addexist(Exist_T);
static void  addlinkstatus(Service_T, LinkStatus_T);
static void  addlinkspeed(Service_T, LinkSpeed_T);
static void  addlinksaturation(Service_T, LinkSaturation_T);
static void  addbandwidth(Bandwidth_T *, Bandwidth_T);
static void  addfilesystem(FileSystem_T);
static void  addicmp(Icmp_T);
static void  addgeneric(Port_T, char*, char*);
static void  addcommand(int, unsigned);
static void  addargument(char *);
static void  addmmonit(Mmonit_T);
static void  addmailserver(MailServer_T);
static bool addcredentials(char *, char *, Digest_Type, bool);
#ifdef HAVE_LIBPAM
static void  addpamauth(char *, int);
#endif
static void  addhtpasswdentry(char *, char *, Digest_Type);
static uid_t get_uid(char *, uid_t);
static gid_t get_gid(char *, gid_t);
static void  addchecksum(Checksum_T);
static void  addperm(Perm_T);
static void  addmatch(Match_T, int, int);
static void  addmatchpath(Match_T, Action_Type);
static void  addstatus(Status_T);
static Uid_T adduid(Uid_T);
static Gid_T addgid(Gid_T);
static void  addeuid(uid_t);
static void  addegid(gid_t);
static void  addeventaction(EventAction_T *, Action_Type, Action_Type);
static void  prepare_urlrequest(URL_T U);
static void  seturlrequest(int, char *);
static void  setlogfile(char *);
static void  setpidfile(char *);
static void  reset_sslset(void);
static void  reset_mailset(void);
static void  reset_mailserverset(void);
static void  reset_mmonitset(void);
static void  reset_portset(void);
static void  reset_resourceset(void);
static void  reset_timestampset(void);
static void  reset_actionrateset(void);
static void  reset_sizeset(void);
static void  reset_uptimeset(void);
static void  reset_responsetimeset(void);
static void  reset_pidset(void);
static void  reset_ppidset(void);
static void  reset_fsflagset(void);
static void  reset_nonexistset(void);
static void  reset_existset(void);
static void  reset_linkstatusset(void);
static void  reset_linkspeedset(void);
static void  reset_linksaturationset(void);
static void  reset_bandwidthset(void);
static void  reset_checksumset(void);
static void  reset_permset(void);
static void  reset_uidset(void);
static void  reset_gidset(void);
static void  reset_statusset(void);
static void  reset_filesystemset(void);
static void  reset_icmpset(void);
static void  reset_rateset(struct rate_t *);
static void  check_name(char *);
static int   check_perm(int);
static void  check_exec(char *);
static int   cleanup_hash_string(char *);
static void  check_depend(void);
static void  setsyslog(char *);
static command_t copycommand(command_t);
static int verifyMaxForward(int);
static void _setPEM(char **store, char *path, const char *description, bool isFile);
static void _setSSLOptions(SslOptions_T options);
#ifdef HAVE_OPENSSL
static void _setSSLVersion(short version);
#endif
static void _unsetSSLVersion(short version);
static void addsecurityattribute(char *, Action_Type, Action_Type);
static void addfiledescriptors(Operator_Type, bool, long long, float, Action_Type, Action_Type);
static void _sanityCheckEveryStatement(Service_T s);


#line 370 "src/y.tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "y.tab.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_IF = 3,                         /* IF  */
  YYSYMBOL_ELSE = 4,                       /* ELSE  */
  YYSYMBOL_THEN = 5,                       /* THEN  */
  YYSYMBOL_FAILED = 6,                     /* FAILED  */
  YYSYMBOL_SET = 7,                        /* SET  */
  YYSYMBOL_LOGFILE = 8,                    /* LOGFILE  */
  YYSYMBOL_FACILITY = 9,                   /* FACILITY  */
  YYSYMBOL_DAEMON = 10,                    /* DAEMON  */
  YYSYMBOL_SYSLOG = 11,                    /* SYSLOG  */
  YYSYMBOL_MAILSERVER = 12,                /* MAILSERVER  */
  YYSYMBOL_HTTPD = 13,                     /* HTTPD  */
  YYSYMBOL_ALLOW = 14,                     /* ALLOW  */
  YYSYMBOL_REJECTOPT = 15,                 /* REJECTOPT  */
  YYSYMBOL_ADDRESS = 16,                   /* ADDRESS  */
  YYSYMBOL_INIT = 17,                      /* INIT  */
  YYSYMBOL_TERMINAL = 18,                  /* TERMINAL  */
  YYSYMBOL_BATCH = 19,                     /* BATCH  */
  YYSYMBOL_READONLY = 20,                  /* READONLY  */
  YYSYMBOL_CLEARTEXT = 21,                 /* CLEARTEXT  */
  YYSYMBOL_MD5HASH = 22,                   /* MD5HASH  */
  YYSYMBOL_SHA1HASH = 23,                  /* SHA1HASH  */
  YYSYMBOL_CRYPT = 24,                     /* CRYPT  */
  YYSYMBOL_DELAY = 25,                     /* DELAY  */
  YYSYMBOL_PEMFILE = 26,                   /* PEMFILE  */
  YYSYMBOL_PEMKEY = 27,                    /* PEMKEY  */
  YYSYMBOL_PEMCHAIN = 28,                  /* PEMCHAIN  */
  YYSYMBOL_ENABLE = 29,                    /* ENABLE  */
  YYSYMBOL_DISABLE = 30,                   /* DISABLE  */
  YYSYMBOL_SSLTOKEN = 31,                  /* SSLTOKEN  */
  YYSYMBOL_CIPHER = 32,                    /* CIPHER  */
  YYSYMBOL_CLIENTPEMFILE = 33,             /* CLIENTPEMFILE  */
  YYSYMBOL_ALLOWSELFCERTIFICATION = 34,    /* ALLOWSELFCERTIFICATION  */
  YYSYMBOL_SELFSIGNED = 35,                /* SELFSIGNED  */
  YYSYMBOL_VERIFY = 36,                    /* VERIFY  */
  YYSYMBOL_CERTIFICATE = 37,               /* CERTIFICATE  */
  YYSYMBOL_CACERTIFICATEFILE = 38,         /* CACERTIFICATEFILE  */
  YYSYMBOL_CACERTIFICATEPATH = 39,         /* CACERTIFICATEPATH  */
  YYSYMBOL_VALID = 40,                     /* VALID  */
  YYSYMBOL_INTERFACE = 41,                 /* INTERFACE  */
  YYSYMBOL_LINK = 42,                      /* LINK  */
  YYSYMBOL_PACKET = 43,                    /* PACKET  */
  YYSYMBOL_BYTEIN = 44,                    /* BYTEIN  */
  YYSYMBOL_BYTEOUT = 45,                   /* BYTEOUT  */
  YYSYMBOL_PACKETIN = 46,                  /* PACKETIN  */
  YYSYMBOL_PACKETOUT = 47,                 /* PACKETOUT  */
  YYSYMBOL_SPEED = 48,                     /* SPEED  */
  YYSYMBOL_SATURATION = 49,                /* SATURATION  */
  YYSYMBOL_UPLOAD = 50,                    /* UPLOAD  */
  YYSYMBOL_DOWNLOAD = 51,                  /* DOWNLOAD  */
  YYSYMBOL_TOTAL = 52,                     /* TOTAL  */
  YYSYMBOL_UP = 53,                        /* UP  */
  YYSYMBOL_DOWN = 54,                      /* DOWN  */
  YYSYMBOL_IDFILE = 55,                    /* IDFILE  */
  YYSYMBOL_STATEFILE = 56,                 /* STATEFILE  */
  YYSYMBOL_SEND = 57,                      /* SEND  */
  YYSYMBOL_EXPECT = 58,                    /* EXPECT  */
  YYSYMBOL_CYCLE = 59,                     /* CYCLE  */
  YYSYMBOL_COUNT = 60,                     /* COUNT  */
  YYSYMBOL_REMINDER = 61,                  /* REMINDER  */
  YYSYMBOL_REPEAT = 62,                    /* REPEAT  */
  YYSYMBOL_LIMITS = 63,                    /* LIMITS  */
  YYSYMBOL_SENDEXPECTBUFFER = 64,          /* SENDEXPECTBUFFER  */
  YYSYMBOL_EXPECTBUFFER = 65,              /* EXPECTBUFFER  */
  YYSYMBOL_FILECONTENTBUFFER = 66,         /* FILECONTENTBUFFER  */
  YYSYMBOL_HTTPCONTENTBUFFER = 67,         /* HTTPCONTENTBUFFER  */
  YYSYMBOL_PROGRAMOUTPUT = 68,             /* PROGRAMOUTPUT  */
  YYSYMBOL_NETWORKTIMEOUT = 69,            /* NETWORKTIMEOUT  */
  YYSYMBOL_PROGRAMTIMEOUT = 70,            /* PROGRAMTIMEOUT  */
  YYSYMBOL_STARTTIMEOUT = 71,              /* STARTTIMEOUT  */
  YYSYMBOL_STOPTIMEOUT = 72,               /* STOPTIMEOUT  */
  YYSYMBOL_RESTARTTIMEOUT = 73,            /* RESTARTTIMEOUT  */
  YYSYMBOL_PIDFILE = 74,                   /* PIDFILE  */
  YYSYMBOL_START = 75,                     /* START  */
  YYSYMBOL_STOP = 76,                      /* STOP  */
  YYSYMBOL_PATHTOK = 77,                   /* PATHTOK  */
  YYSYMBOL_RSAKEY = 78,                    /* RSAKEY  */
  YYSYMBOL_HOST = 79,                      /* HOST  */
  YYSYMBOL_HOSTNAME = 80,                  /* HOSTNAME  */
  YYSYMBOL_PORT = 81,                      /* PORT  */
  YYSYMBOL_IPV4 = 82,                      /* IPV4  */
  YYSYMBOL_IPV6 = 83,                      /* IPV6  */
  YYSYMBOL_TYPE = 84,                      /* TYPE  */
  YYSYMBOL_UDP = 85,                       /* UDP  */
  YYSYMBOL_TCP = 86,                       /* TCP  */
  YYSYMBOL_TCPSSL = 87,                    /* TCPSSL  */
  YYSYMBOL_PROTOCOL = 88,                  /* PROTOCOL  */
  YYSYMBOL_CONNECTION = 89,                /* CONNECTION  */
  YYSYMBOL_ALERT = 90,                     /* ALERT  */
  YYSYMBOL_NOALERT = 91,                   /* NOALERT  */
  YYSYMBOL_MAILFORMAT = 92,                /* MAILFORMAT  */
  YYSYMBOL_UNIXSOCKET = 93,                /* UNIXSOCKET  */
  YYSYMBOL_SIGNATURE = 94,                 /* SIGNATURE  */
  YYSYMBOL_TIMEOUT = 95,                   /* TIMEOUT  */
  YYSYMBOL_RETRY = 96,                     /* RETRY  */
  YYSYMBOL_RESTART = 97,                   /* RESTART  */
  YYSYMBOL_CHECKSUM = 98,                  /* CHECKSUM  */
  YYSYMBOL_EVERY = 99,                     /* EVERY  */
  YYSYMBOL_NOTEVERY = 100,                 /* NOTEVERY  */
  YYSYMBOL_DEFAULT = 101,                  /* DEFAULT  */
  YYSYMBOL_HTTP = 102,                     /* HTTP  */
  YYSYMBOL_HTTPS = 103,                    /* HTTPS  */
  YYSYMBOL_APACHESTATUS = 104,             /* APACHESTATUS  */
  YYSYMBOL_FTP = 105,                      /* FTP  */
  YYSYMBOL_SMTP = 106,                     /* SMTP  */
  YYSYMBOL_SMTPS = 107,                    /* SMTPS  */
  YYSYMBOL_POP = 108,                      /* POP  */
  YYSYMBOL_POPS = 109,                     /* POPS  */
  YYSYMBOL_IMAP = 110,                     /* IMAP  */
  YYSYMBOL_IMAPS = 111,                    /* IMAPS  */
  YYSYMBOL_CLAMAV = 112,                   /* CLAMAV  */
  YYSYMBOL_NNTP = 113,                     /* NNTP  */
  YYSYMBOL_NTP3 = 114,                     /* NTP3  */
  YYSYMBOL_MYSQL = 115,                    /* MYSQL  */
  YYSYMBOL_MYSQLS = 116,                   /* MYSQLS  */
  YYSYMBOL_DNS = 117,                      /* DNS  */
  YYSYMBOL_WEBSOCKET = 118,                /* WEBSOCKET  */
  YYSYMBOL_MQTT = 119,                     /* MQTT  */
  YYSYMBOL_SSH = 120,                      /* SSH  */
  YYSYMBOL_DWP = 121,                      /* DWP  */
  YYSYMBOL_LDAP2 = 122,                    /* LDAP2  */
  YYSYMBOL_LDAP3 = 123,                    /* LDAP3  */
  YYSYMBOL_RDATE = 124,                    /* RDATE  */
  YYSYMBOL_RSYNC = 125,                    /* RSYNC  */
  YYSYMBOL_TNS = 126,                      /* TNS  */
  YYSYMBOL_PGSQL = 127,                    /* PGSQL  */
  YYSYMBOL_POSTFIXPOLICY = 128,            /* POSTFIXPOLICY  */
  YYSYMBOL_SIP = 129,                      /* SIP  */
  YYSYMBOL_LMTP = 130,                     /* LMTP  */
  YYSYMBOL_GPS = 131,                      /* GPS  */
  YYSYMBOL_RADIUS = 132,                   /* RADIUS  */
  YYSYMBOL_MEMCACHE = 133,                 /* MEMCACHE  */
  YYSYMBOL_REDIS = 134,                    /* REDIS  */
  YYSYMBOL_MONGODB = 135,                  /* MONGODB  */
  YYSYMBOL_SIEVE = 136,                    /* SIEVE  */
  YYSYMBOL_SPAMASSASSIN = 137,             /* SPAMASSASSIN  */
  YYSYMBOL_FAIL2BAN = 138,                 /* FAIL2BAN  */
  YYSYMBOL_STRING = 139,                   /* STRING  */
  YYSYMBOL_PATH = 140,                     /* PATH  */
  YYSYMBOL_MAILADDR = 141,                 /* MAILADDR  */
  YYSYMBOL_MAILFROM = 142,                 /* MAILFROM  */
  YYSYMBOL_MAILREPLYTO = 143,              /* MAILREPLYTO  */
  YYSYMBOL_MAILSUBJECT = 144,              /* MAILSUBJECT  */
  YYSYMBOL_MAILBODY = 145,                 /* MAILBODY  */
  YYSYMBOL_SERVICENAME = 146,              /* SERVICENAME  */
  YYSYMBOL_STRINGNAME = 147,               /* STRINGNAME  */
  YYSYMBOL_NUMBER = 148,                   /* NUMBER  */
  YYSYMBOL_PERCENT = 149,                  /* PERCENT  */
  YYSYMBOL_LOGLIMIT = 150,                 /* LOGLIMIT  */
  YYSYMBOL_CLOSELIMIT = 151,               /* CLOSELIMIT  */
  YYSYMBOL_DNSLIMIT = 152,                 /* DNSLIMIT  */
  YYSYMBOL_KEEPALIVELIMIT = 153,           /* KEEPALIVELIMIT  */
  YYSYMBOL_REPLYLIMIT = 154,               /* REPLYLIMIT  */
  YYSYMBOL_REQUESTLIMIT = 155,             /* REQUESTLIMIT  */
  YYSYMBOL_STARTLIMIT = 156,               /* STARTLIMIT  */
  YYSYMBOL_WAITLIMIT = 157,                /* WAITLIMIT  */
  YYSYMBOL_GRACEFULLIMIT = 158,            /* GRACEFULLIMIT  */
  YYSYMBOL_CLEANUPLIMIT = 159,             /* CLEANUPLIMIT  */
  YYSYMBOL_REAL = 160,                     /* REAL  */
  YYSYMBOL_CHECKPROC = 161,                /* CHECKPROC  */
  YYSYMBOL_CHECKFILESYS = 162,             /* CHECKFILESYS  */
  YYSYMBOL_CHECKFILE = 163,                /* CHECKFILE  */
  YYSYMBOL_CHECKDIR = 164,                 /* CHECKDIR  */
  YYSYMBOL_CHECKHOST = 165,                /* CHECKHOST  */
  YYSYMBOL_CHECKSYSTEM = 166,              /* CHECKSYSTEM  */
  YYSYMBOL_CHECKFIFO = 167,                /* CHECKFIFO  */
  YYSYMBOL_CHECKPROGRAM = 168,             /* CHECKPROGRAM  */
  YYSYMBOL_CHECKNET = 169,                 /* CHECKNET  */
  YYSYMBOL_THREADS = 170,                  /* THREADS  */
  YYSYMBOL_CHILDREN = 171,                 /* CHILDREN  */
  YYSYMBOL_METHOD = 172,                   /* METHOD  */
  YYSYMBOL_GET = 173,                      /* GET  */
  YYSYMBOL_HEAD = 174,                     /* HEAD  */
  YYSYMBOL_STATUS = 175,                   /* STATUS  */
  YYSYMBOL_ORIGIN = 176,                   /* ORIGIN  */
  YYSYMBOL_VERSIONOPT = 177,               /* VERSIONOPT  */
  YYSYMBOL_READ = 178,                     /* READ  */
  YYSYMBOL_WRITE = 179,                    /* WRITE  */
  YYSYMBOL_OPERATION = 180,                /* OPERATION  */
  YYSYMBOL_SERVICETIME = 181,              /* SERVICETIME  */
  YYSYMBOL_DISK = 182,                     /* DISK  */
  YYSYMBOL_RESOURCE = 183,                 /* RESOURCE  */
  YYSYMBOL_MEMORY = 184,                   /* MEMORY  */
  YYSYMBOL_TOTALMEMORY = 185,              /* TOTALMEMORY  */
  YYSYMBOL_LOADAVG1 = 186,                 /* LOADAVG1  */
  YYSYMBOL_LOADAVG5 = 187,                 /* LOADAVG5  */
  YYSYMBOL_LOADAVG15 = 188,                /* LOADAVG15  */
  YYSYMBOL_SWAP = 189,                     /* SWAP  */
  YYSYMBOL_MODE = 190,                     /* MODE  */
  YYSYMBOL_ACTIVE = 191,                   /* ACTIVE  */
  YYSYMBOL_PASSIVE = 192,                  /* PASSIVE  */
  YYSYMBOL_MANUAL = 193,                   /* MANUAL  */
  YYSYMBOL_ONREBOOT = 194,                 /* ONREBOOT  */
  YYSYMBOL_NOSTART = 195,                  /* NOSTART  */
  YYSYMBOL_LASTSTATE = 196,                /* LASTSTATE  */
  YYSYMBOL_CORE = 197,                     /* CORE  */
  YYSYMBOL_CPU = 198,                      /* CPU  */
  YYSYMBOL_TOTALCPU = 199,                 /* TOTALCPU  */
  YYSYMBOL_CPUUSER = 200,                  /* CPUUSER  */
  YYSYMBOL_CPUSYSTEM = 201,                /* CPUSYSTEM  */
  YYSYMBOL_CPUWAIT = 202,                  /* CPUWAIT  */
  YYSYMBOL_CPUNICE = 203,                  /* CPUNICE  */
  YYSYMBOL_CPUHARDIRQ = 204,               /* CPUHARDIRQ  */
  YYSYMBOL_CPUSOFTIRQ = 205,               /* CPUSOFTIRQ  */
  YYSYMBOL_CPUSTEAL = 206,                 /* CPUSTEAL  */
  YYSYMBOL_CPUGUEST = 207,                 /* CPUGUEST  */
  YYSYMBOL_CPUGUESTNICE = 208,             /* CPUGUESTNICE  */
  YYSYMBOL_GROUP = 209,                    /* GROUP  */
  YYSYMBOL_REQUEST = 210,                  /* REQUEST  */
  YYSYMBOL_DEPENDS = 211,                  /* DEPENDS  */
  YYSYMBOL_BASEDIR = 212,                  /* BASEDIR  */
  YYSYMBOL_SLOT = 213,                     /* SLOT  */
  YYSYMBOL_EVENTQUEUE = 214,               /* EVENTQUEUE  */
  YYSYMBOL_SECRET = 215,                   /* SECRET  */
  YYSYMBOL_HOSTHEADER = 216,               /* HOSTHEADER  */
  YYSYMBOL_UID = 217,                      /* UID  */
  YYSYMBOL_EUID = 218,                     /* EUID  */
  YYSYMBOL_GID = 219,                      /* GID  */
  YYSYMBOL_MMONIT = 220,                   /* MMONIT  */
  YYSYMBOL_INSTANCE = 221,                 /* INSTANCE  */
  YYSYMBOL_USERNAME = 222,                 /* USERNAME  */
  YYSYMBOL_PASSWORD = 223,                 /* PASSWORD  */
  YYSYMBOL_DATABASE = 224,                 /* DATABASE  */
  YYSYMBOL_TIME = 225,                     /* TIME  */
  YYSYMBOL_ATIME = 226,                    /* ATIME  */
  YYSYMBOL_CTIME = 227,                    /* CTIME  */
  YYSYMBOL_MTIME = 228,                    /* MTIME  */
  YYSYMBOL_CHANGED = 229,                  /* CHANGED  */
  YYSYMBOL_MILLISECOND = 230,              /* MILLISECOND  */
  YYSYMBOL_SECOND = 231,                   /* SECOND  */
  YYSYMBOL_MINUTE = 232,                   /* MINUTE  */
  YYSYMBOL_HOUR = 233,                     /* HOUR  */
  YYSYMBOL_DAY = 234,                      /* DAY  */
  YYSYMBOL_MONTH = 235,                    /* MONTH  */
  YYSYMBOL_SSLV2 = 236,                    /* SSLV2  */
  YYSYMBOL_SSLV3 = 237,                    /* SSLV3  */
  YYSYMBOL_TLSV1 = 238,                    /* TLSV1  */
  YYSYMBOL_TLSV11 = 239,                   /* TLSV11  */
  YYSYMBOL_TLSV12 = 240,                   /* TLSV12  */
  YYSYMBOL_TLSV13 = 241,                   /* TLSV13  */
  YYSYMBOL_CERTMD5 = 242,                  /* CERTMD5  */
  YYSYMBOL_AUTO = 243,                     /* AUTO  */
  YYSYMBOL_NOSSLV2 = 244,                  /* NOSSLV2  */
  YYSYMBOL_NOSSLV3 = 245,                  /* NOSSLV3  */
  YYSYMBOL_NOTLSV1 = 246,                  /* NOTLSV1  */
  YYSYMBOL_NOTLSV11 = 247,                 /* NOTLSV11  */
  YYSYMBOL_NOTLSV12 = 248,                 /* NOTLSV12  */
  YYSYMBOL_NOTLSV13 = 249,                 /* NOTLSV13  */
  YYSYMBOL_BYTE = 250,                     /* BYTE  */
  YYSYMBOL_KILOBYTE = 251,                 /* KILOBYTE  */
  YYSYMBOL_MEGABYTE = 252,                 /* MEGABYTE  */
  YYSYMBOL_GIGABYTE = 253,                 /* GIGABYTE  */
  YYSYMBOL_INODE = 254,                    /* INODE  */
  YYSYMBOL_SPACE = 255,                    /* SPACE  */
  YYSYMBOL_TFREE = 256,                    /* TFREE  */
  YYSYMBOL_PERMISSION = 257,               /* PERMISSION  */
  YYSYMBOL_SIZE = 258,                     /* SIZE  */
  YYSYMBOL_MATCH = 259,                    /* MATCH  */
  YYSYMBOL_NOT = 260,                      /* NOT  */
  YYSYMBOL_IGNORE = 261,                   /* IGNORE  */
  YYSYMBOL_ACTION = 262,                   /* ACTION  */
  YYSYMBOL_UPTIME = 263,                   /* UPTIME  */
  YYSYMBOL_RESPONSETIME = 264,             /* RESPONSETIME  */
  YYSYMBOL_EXEC = 265,                     /* EXEC  */
  YYSYMBOL_UNMONITOR = 266,                /* UNMONITOR  */
  YYSYMBOL_PING = 267,                     /* PING  */
  YYSYMBOL_PING4 = 268,                    /* PING4  */
  YYSYMBOL_PING6 = 269,                    /* PING6  */
  YYSYMBOL_ICMP = 270,                     /* ICMP  */
  YYSYMBOL_ICMPECHO = 271,                 /* ICMPECHO  */
  YYSYMBOL_NONEXIST = 272,                 /* NONEXIST  */
  YYSYMBOL_EXIST = 273,                    /* EXIST  */
  YYSYMBOL_INVALID = 274,                  /* INVALID  */
  YYSYMBOL_DATA = 275,                     /* DATA  */
  YYSYMBOL_RECOVERED = 276,                /* RECOVERED  */
  YYSYMBOL_PASSED = 277,                   /* PASSED  */
  YYSYMBOL_SUCCEEDED = 278,                /* SUCCEEDED  */
  YYSYMBOL_URL = 279,                      /* URL  */
  YYSYMBOL_CONTENT = 280,                  /* CONTENT  */
  YYSYMBOL_PID = 281,                      /* PID  */
  YYSYMBOL_PPID = 282,                     /* PPID  */
  YYSYMBOL_FSFLAG = 283,                   /* FSFLAG  */
  YYSYMBOL_REGISTER = 284,                 /* REGISTER  */
  YYSYMBOL_CREDENTIALS = 285,              /* CREDENTIALS  */
  YYSYMBOL_URLOBJECT = 286,                /* URLOBJECT  */
  YYSYMBOL_ADDRESSOBJECT = 287,            /* ADDRESSOBJECT  */
  YYSYMBOL_TARGET = 288,                   /* TARGET  */
  YYSYMBOL_TIMESPEC = 289,                 /* TIMESPEC  */
  YYSYMBOL_HTTPHEADER = 290,               /* HTTPHEADER  */
  YYSYMBOL_MAXFORWARD = 291,               /* MAXFORWARD  */
  YYSYMBOL_FIPS = 292,                     /* FIPS  */
  YYSYMBOL_SECURITY = 293,                 /* SECURITY  */
  YYSYMBOL_ATTRIBUTE = 294,                /* ATTRIBUTE  */
  YYSYMBOL_FILEDESCRIPTORS = 295,          /* FILEDESCRIPTORS  */
  YYSYMBOL_GREATER = 296,                  /* GREATER  */
  YYSYMBOL_GREATEROREQUAL = 297,           /* GREATEROREQUAL  */
  YYSYMBOL_LESS = 298,                     /* LESS  */
  YYSYMBOL_LESSOREQUAL = 299,              /* LESSOREQUAL  */
  YYSYMBOL_EQUAL = 300,                    /* EQUAL  */
  YYSYMBOL_NOTEQUAL = 301,                 /* NOTEQUAL  */
  YYSYMBOL_302_ = 302,                     /* '{'  */
  YYSYMBOL_303_ = 303,                     /* '}'  */
  YYSYMBOL_304_ = 304,                     /* ':'  */
  YYSYMBOL_305_ = 305,                     /* '@'  */
  YYSYMBOL_306_ = 306,                     /* '['  */
  YYSYMBOL_307_ = 307,                     /* ']'  */
  YYSYMBOL_YYACCEPT = 308,                 /* $accept  */
  YYSYMBOL_cfgfile = 309,                  /* cfgfile  */
  YYSYMBOL_statement_list = 310,           /* statement_list  */
  YYSYMBOL_statement = 311,                /* statement  */
  YYSYMBOL_optproclist = 312,              /* optproclist  */
  YYSYMBOL_optproc = 313,                  /* optproc  */
  YYSYMBOL_optfilelist = 314,              /* optfilelist  */
  YYSYMBOL_optfile = 315,                  /* optfile  */
  YYSYMBOL_optfilesyslist = 316,           /* optfilesyslist  */
  YYSYMBOL_optfilesys = 317,               /* optfilesys  */
  YYSYMBOL_optdirlist = 318,               /* optdirlist  */
  YYSYMBOL_optdir = 319,                   /* optdir  */
  YYSYMBOL_opthostlist = 320,              /* opthostlist  */
  YYSYMBOL_opthost = 321,                  /* opthost  */
  YYSYMBOL_optnetlist = 322,               /* optnetlist  */
  YYSYMBOL_optnet = 323,                   /* optnet  */
  YYSYMBOL_optsystemlist = 324,            /* optsystemlist  */
  YYSYMBOL_optsystem = 325,                /* optsystem  */
  YYSYMBOL_optfifolist = 326,              /* optfifolist  */
  YYSYMBOL_optfifo = 327,                  /* optfifo  */
  YYSYMBOL_optprogramlist = 328,           /* optprogramlist  */
  YYSYMBOL_optprogram = 329,               /* optprogram  */
  YYSYMBOL_setalert = 330,                 /* setalert  */
  YYSYMBOL_setdaemon = 331,                /* setdaemon  */
  YYSYMBOL_setterminal = 332,              /* setterminal  */
  YYSYMBOL_startdelay = 333,               /* startdelay  */
  YYSYMBOL_setinit = 334,                  /* setinit  */
  YYSYMBOL_setonreboot = 335,              /* setonreboot  */
  YYSYMBOL_setexpectbuffer = 336,          /* setexpectbuffer  */
  YYSYMBOL_setlimits = 337,                /* setlimits  */
  YYSYMBOL_limitlist = 338,                /* limitlist  */
  YYSYMBOL_limit = 339,                    /* limit  */
  YYSYMBOL_setfips = 340,                  /* setfips  */
  YYSYMBOL_setlog = 341,                   /* setlog  */
  YYSYMBOL_seteventqueue = 342,            /* seteventqueue  */
  YYSYMBOL_setidfile = 343,                /* setidfile  */
  YYSYMBOL_setstatefile = 344,             /* setstatefile  */
  YYSYMBOL_setpid = 345,                   /* setpid  */
  YYSYMBOL_setmmonits = 346,               /* setmmonits  */
  YYSYMBOL_mmonitlist = 347,               /* mmonitlist  */
  YYSYMBOL_mmonit = 348,                   /* mmonit  */
  YYSYMBOL_mmonitoptlist = 349,            /* mmonitoptlist  */
  YYSYMBOL_mmonitopt = 350,                /* mmonitopt  */
  YYSYMBOL_credentials = 351,              /* credentials  */
  YYSYMBOL_setssl = 352,                   /* setssl  */
  YYSYMBOL_ssl = 353,                      /* ssl  */
  YYSYMBOL_ssloptionlist = 354,            /* ssloptionlist  */
  YYSYMBOL_ssloption = 355,                /* ssloption  */
  YYSYMBOL_sslexpire = 356,                /* sslexpire  */
  YYSYMBOL_expireoperator = 357,           /* expireoperator  */
  YYSYMBOL_sslchecksum = 358,              /* sslchecksum  */
  YYSYMBOL_checksumoperator = 359,         /* checksumoperator  */
  YYSYMBOL_sslversionlist = 360,           /* sslversionlist  */
  YYSYMBOL_sslversion = 361,               /* sslversion  */
  YYSYMBOL_certmd5 = 362,                  /* certmd5  */
  YYSYMBOL_setmailservers = 363,           /* setmailservers  */
  YYSYMBOL_setmailformat = 364,            /* setmailformat  */
  YYSYMBOL_mailserverlist = 365,           /* mailserverlist  */
  YYSYMBOL_mailserver = 366,               /* mailserver  */
  YYSYMBOL_mailserveroptlist = 367,        /* mailserveroptlist  */
  YYSYMBOL_mailserveropt = 368,            /* mailserveropt  */
  YYSYMBOL_sethttpd = 369,                 /* sethttpd  */
  YYSYMBOL_httpdlist = 370,                /* httpdlist  */
  YYSYMBOL_httpdoption = 371,              /* httpdoption  */
  YYSYMBOL_pemfile = 372,                  /* pemfile  */
  YYSYMBOL_clientpemfile = 373,            /* clientpemfile  */
  YYSYMBOL_allowselfcert = 374,            /* allowselfcert  */
  YYSYMBOL_httpdport = 375,                /* httpdport  */
  YYSYMBOL_httpdsocket = 376,              /* httpdsocket  */
  YYSYMBOL_httpdsocketoptionlist = 377,    /* httpdsocketoptionlist  */
  YYSYMBOL_httpdsocketoption = 378,        /* httpdsocketoption  */
  YYSYMBOL_sigenable = 379,                /* sigenable  */
  YYSYMBOL_sigdisable = 380,               /* sigdisable  */
  YYSYMBOL_signature = 381,                /* signature  */
  YYSYMBOL_bindaddress = 382,              /* bindaddress  */
  YYSYMBOL_allow = 383,                    /* allow  */
  YYSYMBOL_384_1 = 384,                    /* $@1  */
  YYSYMBOL_385_2 = 385,                    /* $@2  */
  YYSYMBOL_386_3 = 386,                    /* $@3  */
  YYSYMBOL_387_4 = 387,                    /* $@4  */
  YYSYMBOL_allowuserlist = 388,            /* allowuserlist  */
  YYSYMBOL_allowuser = 389,                /* allowuser  */
  YYSYMBOL_readonly = 390,                 /* readonly  */
  YYSYMBOL_checkproc = 391,                /* checkproc  */
  YYSYMBOL_checkfile = 392,                /* checkfile  */
  YYSYMBOL_checkfilesys = 393,             /* checkfilesys  */
  YYSYMBOL_checkdir = 394,                 /* checkdir  */
  YYSYMBOL_checkhost = 395,                /* checkhost  */
  YYSYMBOL_checknet = 396,                 /* checknet  */
  YYSYMBOL_checksystem = 397,              /* checksystem  */
  YYSYMBOL_checkfifo = 398,                /* checkfifo  */
  YYSYMBOL_checkprogram = 399,             /* checkprogram  */
  YYSYMBOL_start = 400,                    /* start  */
  YYSYMBOL_stop = 401,                     /* stop  */
  YYSYMBOL_restart = 402,                  /* restart  */
  YYSYMBOL_argumentlist = 403,             /* argumentlist  */
  YYSYMBOL_useroptionlist = 404,           /* useroptionlist  */
  YYSYMBOL_argument = 405,                 /* argument  */
  YYSYMBOL_useroption = 406,               /* useroption  */
  YYSYMBOL_username = 407,                 /* username  */
  YYSYMBOL_password = 408,                 /* password  */
  YYSYMBOL_database = 409,                 /* database  */
  YYSYMBOL_hostname = 410,                 /* hostname  */
  YYSYMBOL_connection = 411,               /* connection  */
  YYSYMBOL_connectionoptlist = 412,        /* connectionoptlist  */
  YYSYMBOL_connectionopt = 413,            /* connectionopt  */
  YYSYMBOL_connectionurl = 414,            /* connectionurl  */
  YYSYMBOL_connectionurloptlist = 415,     /* connectionurloptlist  */
  YYSYMBOL_connectionurlopt = 416,         /* connectionurlopt  */
  YYSYMBOL_connectionunix = 417,           /* connectionunix  */
  YYSYMBOL_connectionuxoptlist = 418,      /* connectionuxoptlist  */
  YYSYMBOL_connectionuxopt = 419,          /* connectionuxopt  */
  YYSYMBOL_icmp = 420,                     /* icmp  */
  YYSYMBOL_icmpoptlist = 421,              /* icmpoptlist  */
  YYSYMBOL_icmpopt = 422,                  /* icmpopt  */
  YYSYMBOL_host = 423,                     /* host  */
  YYSYMBOL_port = 424,                     /* port  */
  YYSYMBOL_unixsocket = 425,               /* unixsocket  */
  YYSYMBOL_ip = 426,                       /* ip  */
  YYSYMBOL_type = 427,                     /* type  */
  YYSYMBOL_typeoptlist = 428,              /* typeoptlist  */
  YYSYMBOL_typeopt = 429,                  /* typeopt  */
  YYSYMBOL_outgoing = 430,                 /* outgoing  */
  YYSYMBOL_protocol = 431,                 /* protocol  */
  YYSYMBOL_sendexpect = 432,               /* sendexpect  */
  YYSYMBOL_websocketlist = 433,            /* websocketlist  */
  YYSYMBOL_websocket = 434,                /* websocket  */
  YYSYMBOL_smtplist = 435,                 /* smtplist  */
  YYSYMBOL_smtp = 436,                     /* smtp  */
  YYSYMBOL_mqttlist = 437,                 /* mqttlist  */
  YYSYMBOL_mqtt = 438,                     /* mqtt  */
  YYSYMBOL_mysqllist = 439,                /* mysqllist  */
  YYSYMBOL_mysql = 440,                    /* mysql  */
  YYSYMBOL_postgresqllist = 441,           /* postgresqllist  */
  YYSYMBOL_postgresql = 442,               /* postgresql  */
  YYSYMBOL_target = 443,                   /* target  */
  YYSYMBOL_maxforward = 444,               /* maxforward  */
  YYSYMBOL_siplist = 445,                  /* siplist  */
  YYSYMBOL_sip = 446,                      /* sip  */
  YYSYMBOL_httplist = 447,                 /* httplist  */
  YYSYMBOL_http = 448,                     /* http  */
  YYSYMBOL_status = 449,                   /* status  */
  YYSYMBOL_method = 450,                   /* method  */
  YYSYMBOL_request = 451,                  /* request  */
  YYSYMBOL_responsesum = 452,              /* responsesum  */
  YYSYMBOL_hostheader = 453,               /* hostheader  */
  YYSYMBOL_httpheaderlist = 454,           /* httpheaderlist  */
  YYSYMBOL_secret = 455,                   /* secret  */
  YYSYMBOL_radiuslist = 456,               /* radiuslist  */
  YYSYMBOL_radius = 457,                   /* radius  */
  YYSYMBOL_apache_stat_list = 458,         /* apache_stat_list  */
  YYSYMBOL_apache_stat = 459,              /* apache_stat  */
  YYSYMBOL_exist = 460,                    /* exist  */
  YYSYMBOL_pid = 461,                      /* pid  */
  YYSYMBOL_ppid = 462,                     /* ppid  */
  YYSYMBOL_uptime = 463,                   /* uptime  */
  YYSYMBOL_responsetime = 464,             /* responsetime  */
  YYSYMBOL_icmpcount = 465,                /* icmpcount  */
  YYSYMBOL_icmpsize = 466,                 /* icmpsize  */
  YYSYMBOL_icmptimeout = 467,              /* icmptimeout  */
  YYSYMBOL_icmpoutgoing = 468,             /* icmpoutgoing  */
  YYSYMBOL_stoptimeout = 469,              /* stoptimeout  */
  YYSYMBOL_starttimeout = 470,             /* starttimeout  */
  YYSYMBOL_restarttimeout = 471,           /* restarttimeout  */
  YYSYMBOL_programtimeout = 472,           /* programtimeout  */
  YYSYMBOL_nettimeout = 473,               /* nettimeout  */
  YYSYMBOL_connectiontimeout = 474,        /* connectiontimeout  */
  YYSYMBOL_retry = 475,                    /* retry  */
  YYSYMBOL_actionrate = 476,               /* actionrate  */
  YYSYMBOL_urloption = 477,                /* urloption  */
  YYSYMBOL_urloperator = 478,              /* urloperator  */
  YYSYMBOL_alert = 479,                    /* alert  */
  YYSYMBOL_alertmail = 480,                /* alertmail  */
  YYSYMBOL_noalertmail = 481,              /* noalertmail  */
  YYSYMBOL_eventoptionlist = 482,          /* eventoptionlist  */
  YYSYMBOL_eventoption = 483,              /* eventoption  */
  YYSYMBOL_formatlist = 484,               /* formatlist  */
  YYSYMBOL_formatoptionlist = 485,         /* formatoptionlist  */
  YYSYMBOL_formatoption = 486,             /* formatoption  */
  YYSYMBOL_every = 487,                    /* every  */
  YYSYMBOL_mode = 488,                     /* mode  */
  YYSYMBOL_onreboot = 489,                 /* onreboot  */
  YYSYMBOL_group = 490,                    /* group  */
  YYSYMBOL_depend = 491,                   /* depend  */
  YYSYMBOL_dependlist = 492,               /* dependlist  */
  YYSYMBOL_dependant = 493,                /* dependant  */
  YYSYMBOL_statusvalue = 494,              /* statusvalue  */
  YYSYMBOL_resourceprocess = 495,          /* resourceprocess  */
  YYSYMBOL_resourceprocesslist = 496,      /* resourceprocesslist  */
  YYSYMBOL_resourceprocessopt = 497,       /* resourceprocessopt  */
  YYSYMBOL_resourcesystem = 498,           /* resourcesystem  */
  YYSYMBOL_resourcesystemlist = 499,       /* resourcesystemlist  */
  YYSYMBOL_resourcesystemopt = 500,        /* resourcesystemopt  */
  YYSYMBOL_resourcecpuproc = 501,          /* resourcecpuproc  */
  YYSYMBOL_resourcecpu = 502,              /* resourcecpu  */
  YYSYMBOL_resourcecpuid = 503,            /* resourcecpuid  */
  YYSYMBOL_resourcemem = 504,              /* resourcemem  */
  YYSYMBOL_resourcememproc = 505,          /* resourcememproc  */
  YYSYMBOL_resourceswap = 506,             /* resourceswap  */
  YYSYMBOL_resourcethreads = 507,          /* resourcethreads  */
  YYSYMBOL_resourcechild = 508,            /* resourcechild  */
  YYSYMBOL_resourceload = 509,             /* resourceload  */
  YYSYMBOL_resourceloadavg = 510,          /* resourceloadavg  */
  YYSYMBOL_coremultiplier = 511,           /* coremultiplier  */
  YYSYMBOL_resourceread = 512,             /* resourceread  */
  YYSYMBOL_resourcewrite = 513,            /* resourcewrite  */
  YYSYMBOL_value = 514,                    /* value  */
  YYSYMBOL_timestamptype = 515,            /* timestamptype  */
  YYSYMBOL_timestamp = 516,                /* timestamp  */
  YYSYMBOL_operator = 517,                 /* operator  */
  YYSYMBOL_time = 518,                     /* time  */
  YYSYMBOL_totaltime = 519,                /* totaltime  */
  YYSYMBOL_currenttime = 520,              /* currenttime  */
  YYSYMBOL_repeat = 521,                   /* repeat  */
  YYSYMBOL_action = 522,                   /* action  */
  YYSYMBOL_action1 = 523,                  /* action1  */
  YYSYMBOL_action2 = 524,                  /* action2  */
  YYSYMBOL_rateXcycles = 525,              /* rateXcycles  */
  YYSYMBOL_rateXYcycles = 526,             /* rateXYcycles  */
  YYSYMBOL_rate1 = 527,                    /* rate1  */
  YYSYMBOL_rate2 = 528,                    /* rate2  */
  YYSYMBOL_recovery_success = 529,         /* recovery_success  */
  YYSYMBOL_recovery_failure = 530,         /* recovery_failure  */
  YYSYMBOL_checksum = 531,                 /* checksum  */
  YYSYMBOL_hashtype = 532,                 /* hashtype  */
  YYSYMBOL_inode = 533,                    /* inode  */
  YYSYMBOL_space = 534,                    /* space  */
  YYSYMBOL_read = 535,                     /* read  */
  YYSYMBOL_write = 536,                    /* write  */
  YYSYMBOL_servicetime = 537,              /* servicetime  */
  YYSYMBOL_fsflag = 538,                   /* fsflag  */
  YYSYMBOL_unit = 539,                     /* unit  */
  YYSYMBOL_permission = 540,               /* permission  */
  YYSYMBOL_programmatch = 541,             /* programmatch  */
  YYSYMBOL_match = 542,                    /* match  */
  YYSYMBOL_matchflagnot = 543,             /* matchflagnot  */
  YYSYMBOL_size = 544,                     /* size  */
  YYSYMBOL_uid = 545,                      /* uid  */
  YYSYMBOL_euid = 546,                     /* euid  */
  YYSYMBOL_secattr = 547,                  /* secattr  */
  YYSYMBOL_filedescriptorssystem = 548,    /* filedescriptorssystem  */
  YYSYMBOL_filedescriptorsprocess = 549,   /* filedescriptorsprocess  */
  YYSYMBOL_filedescriptorsprocesstotal = 550, /* filedescriptorsprocesstotal  */
  YYSYMBOL_gid = 551,                      /* gid  */
  YYSYMBOL_linkstatus = 552,               /* linkstatus  */
  YYSYMBOL_linkspeed = 553,                /* linkspeed  */
  YYSYMBOL_linksaturation = 554,           /* linksaturation  */
  YYSYMBOL_upload = 555,                   /* upload  */
  YYSYMBOL_download = 556,                 /* download  */
  YYSYMBOL_icmptype = 557,                 /* icmptype  */
  YYSYMBOL_reminder = 558                  /* reminder  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  69
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1975

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  308
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  251
/* YYNRULES -- Number of rules.  */
#define YYNRULES  839
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  1627

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   556


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int16 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   304,     2,
       2,     2,     2,     2,   305,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   306,     2,   307,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   302,     2,   303,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,   234,
     235,   236,   237,   238,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   379,   379,   380,   383,   384,   387,   388,   389,   390,
     391,   392,   393,   394,   395,   396,   397,   398,   399,   400,
     401,   402,   403,   404,   405,   406,   407,   408,   409,   410,
     411,   412,   413,   416,   417,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   446,
     447,   450,   451,   452,   453,   454,   455,   456,   457,   458,
     459,   460,   461,   462,   463,   464,   465,   466,   467,   470,
     471,   474,   475,   476,   477,   478,   479,   480,   481,   482,
     483,   484,   485,   486,   487,   488,   489,   490,   491,   492,
     493,   496,   497,   500,   501,   502,   503,   504,   505,   506,
     507,   508,   509,   510,   511,   512,   513,   514,   517,   518,
     521,   522,   523,   524,   525,   526,   527,   528,   529,   530,
     531,   532,   533,   536,   537,   540,   541,   542,   543,   544,
     545,   546,   547,   548,   549,   550,   551,   552,   553,   554,
     557,   558,   561,   562,   563,   564,   565,   566,   567,   568,
     569,   570,   571,   572,   573,   576,   577,   580,   581,   582,
     583,   584,   585,   586,   587,   588,   589,   590,   591,   592,
     593,   594,   597,   598,   601,   602,   603,   604,   605,   606,
     607,   608,   609,   610,   611,   612,   615,   619,   622,   628,
     638,   643,   646,   651,   656,   659,   662,   667,   673,   676,
     677,   680,   683,   686,   689,   692,   695,   698,   701,   704,
     707,   710,   713,   716,   719,   724,   729,   737,   740,   745,
     748,   752,   758,   763,   768,   776,   779,   780,   783,   789,
     790,   793,   796,   797,   798,   799,   802,   803,   808,   813,
     816,   819,   820,   823,   827,   831,   835,   839,   842,   846,
     849,   852,   855,   858,   861,   866,   872,   873,   876,   890,
     897,   906,   907,   910,   911,   914,   921,   924,   931,   934,
     941,   944,   951,   954,   961,   964,   971,   974,   985,   994,
    1001,  1016,  1017,  1020,  1029,  1040,  1041,  1044,  1047,  1050,
    1051,  1052,  1053,  1056,  1083,  1084,  1087,  1088,  1089,  1090,
    1091,  1092,  1093,  1094,  1095,  1099,  1105,  1111,  1117,  1124,
    1130,  1131,  1134,  1139,  1144,  1148,  1152,  1156,  1161,  1162,
    1165,  1166,  1169,  1172,  1177,  1186,  1189,  1197,  1201,  1205,
    1209,  1213,  1213,  1220,  1220,  1227,  1227,  1234,  1234,  1241,
    1248,  1249,  1252,  1258,  1261,  1266,  1269,  1272,  1279,  1288,
    1293,  1296,  1301,  1306,  1311,  1319,  1325,  1340,  1345,  1351,
    1359,  1362,  1367,  1370,  1376,  1379,  1384,  1385,  1388,  1389,
    1392,  1395,  1400,  1404,  1408,  1411,  1416,  1419,  1424,  1429,
    1434,  1437,  1442,  1452,  1462,  1463,  1466,  1467,  1468,  1469,
    1470,  1471,  1472,  1473,  1474,  1475,  1476,  1477,  1480,  1488,
    1498,  1499,  1502,  1503,  1504,  1505,  1506,  1507,  1510,  1517,
    1526,  1527,  1530,  1531,  1532,  1533,  1534,  1535,  1538,  1547,
    1555,  1563,  1571,  1580,  1588,  1596,  1606,  1607,  1610,  1611,
    1612,  1613,  1614,  1617,  1620,  1625,  1630,  1636,  1639,  1644,
    1647,  1651,  1656,  1657,  1660,  1661,  1664,  1669,  1672,  1675,
    1678,  1681,  1684,  1687,  1690,  1693,  1696,  1701,  1704,  1709,
    1712,  1715,  1718,  1721,  1724,  1727,  1730,  1734,  1737,  1741,
    1744,  1747,  1752,  1755,  1758,  1761,  1764,  1767,  1770,  1773,
    1776,  1781,  1784,  1787,  1790,  1795,  1803,  1813,  1814,  1817,
    1820,  1823,  1826,  1831,  1832,  1835,  1838,  1843,  1844,  1847,
    1850,  1855,  1856,  1859,  1862,  1865,  1878,  1884,  1892,  1893,
    1896,  1899,  1902,  1907,  1910,  1915,  1920,  1921,  1924,  1927,
    1932,  1933,  1936,  1939,  1942,  1943,  1944,  1945,  1946,  1947,
    1950,  1960,  1963,  1968,  1972,  1978,  1983,  1989,  1990,  1995,
    2000,  2001,  2004,  2009,  2010,  2013,  2016,  2019,  2022,  2026,
    2030,  2034,  2038,  2042,  2046,  2050,  2054,  2058,  2064,  2068,
    2075,  2081,  2087,  2095,  2099,  2105,  2110,  2120,  2125,  2130,
    2133,  2138,  2141,  2146,  2149,  2154,  2157,  2162,  2165,  2170,
    2175,  2180,  2186,  2194,  2200,  2201,  2204,  2208,  2211,  2215,
    2220,  2223,  2226,  2227,  2230,  2231,  2232,  2233,  2234,  2235,
    2236,  2237,  2238,  2239,  2240,  2241,  2242,  2243,  2244,  2245,
    2246,  2247,  2248,  2249,  2250,  2251,  2252,  2253,  2254,  2255,
    2256,  2257,  2258,  2259,  2262,  2263,  2266,  2267,  2270,  2271,
    2272,  2273,  2276,  2281,  2286,  2293,  2296,  2299,  2305,  2308,
    2312,  2317,  2324,  2327,  2328,  2331,  2334,  2341,  2350,  2356,
    2357,  2360,  2361,  2362,  2363,  2364,  2365,  2366,  2369,  2375,
    2376,  2379,  2380,  2381,  2382,  2385,  2390,  2397,  2404,  2410,
    2416,  2422,  2428,  2434,  2440,  2446,  2452,  2458,  2463,  2468,
    2475,  2480,  2485,  2490,  2497,  2502,  2509,  2516,  2523,  2543,
    2544,  2545,  2548,  2549,  2553,  2558,  2563,  2570,  2575,  2580,
    2587,  2588,  2591,  2592,  2593,  2594,  2597,  2604,  2612,  2613,
    2614,  2615,  2616,  2617,  2618,  2619,  2622,  2623,  2624,  2625,
    2626,  2627,  2630,  2631,  2632,  2634,  2635,  2637,  2640,  2643,
    2651,  2654,  2657,  2661,  2664,  2667,  2670,  2675,  2686,  2697,
    2707,  2719,  2720,  2725,  2732,  2733,  2738,  2745,  2748,  2751,
    2754,  2757,  2762,  2765,  2768,  2773,  2777,  2784,  2790,  2791,
    2792,  2795,  2802,  2809,  2816,  2825,  2832,  2839,  2846,  2855,
    2862,  2871,  2878,  2887,  2894,  2903,  2909,  2910,  2911,  2912,
    2913,  2916,  2921,  2928,  2937,  2945,  2952,  2960,  2968,  2975,
    2981,  2988,  2996,  2999,  3005,  3011,  3018,  3024,  3031,  3037,
    3044,  3047,  3052,  3058,  3066,  3072,  3080,  3088,  3094,  3101,
    3105,  3110,  3117,  3122,  3130,  3138,  3146,  3154,  3162,  3170,
    3180,  3188,  3196,  3204,  3212,  3220,  3230,  3233,  3234,  3235
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "IF", "ELSE", "THEN",
  "FAILED", "SET", "LOGFILE", "FACILITY", "DAEMON", "SYSLOG", "MAILSERVER",
  "HTTPD", "ALLOW", "REJECTOPT", "ADDRESS", "INIT", "TERMINAL", "BATCH",
  "READONLY", "CLEARTEXT", "MD5HASH", "SHA1HASH", "CRYPT", "DELAY",
  "PEMFILE", "PEMKEY", "PEMCHAIN", "ENABLE", "DISABLE", "SSLTOKEN",
  "CIPHER", "CLIENTPEMFILE", "ALLOWSELFCERTIFICATION", "SELFSIGNED",
  "VERIFY", "CERTIFICATE", "CACERTIFICATEFILE", "CACERTIFICATEPATH",
  "VALID", "INTERFACE", "LINK", "PACKET", "BYTEIN", "BYTEOUT", "PACKETIN",
  "PACKETOUT", "SPEED", "SATURATION", "UPLOAD", "DOWNLOAD", "TOTAL", "UP",
  "DOWN", "IDFILE", "STATEFILE", "SEND", "EXPECT", "CYCLE", "COUNT",
  "REMINDER", "REPEAT", "LIMITS", "SENDEXPECTBUFFER", "EXPECTBUFFER",
  "FILECONTENTBUFFER", "HTTPCONTENTBUFFER", "PROGRAMOUTPUT",
  "NETWORKTIMEOUT", "PROGRAMTIMEOUT", "STARTTIMEOUT", "STOPTIMEOUT",
  "RESTARTTIMEOUT", "PIDFILE", "START", "STOP", "PATHTOK", "RSAKEY",
  "HOST", "HOSTNAME", "PORT", "IPV4", "IPV6", "TYPE", "UDP", "TCP",
  "TCPSSL", "PROTOCOL", "CONNECTION", "ALERT", "NOALERT", "MAILFORMAT",
  "UNIXSOCKET", "SIGNATURE", "TIMEOUT", "RETRY", "RESTART", "CHECKSUM",
  "EVERY", "NOTEVERY", "DEFAULT", "HTTP", "HTTPS", "APACHESTATUS", "FTP",
  "SMTP", "SMTPS", "POP", "POPS", "IMAP", "IMAPS", "CLAMAV", "NNTP",
  "NTP3", "MYSQL", "MYSQLS", "DNS", "WEBSOCKET", "MQTT", "SSH", "DWP",
  "LDAP2", "LDAP3", "RDATE", "RSYNC", "TNS", "PGSQL", "POSTFIXPOLICY",
  "SIP", "LMTP", "GPS", "RADIUS", "MEMCACHE", "REDIS", "MONGODB", "SIEVE",
  "SPAMASSASSIN", "FAIL2BAN", "STRING", "PATH", "MAILADDR", "MAILFROM",
  "MAILREPLYTO", "MAILSUBJECT", "MAILBODY", "SERVICENAME", "STRINGNAME",
  "NUMBER", "PERCENT", "LOGLIMIT", "CLOSELIMIT", "DNSLIMIT",
  "KEEPALIVELIMIT", "REPLYLIMIT", "REQUESTLIMIT", "STARTLIMIT",
  "WAITLIMIT", "GRACEFULLIMIT", "CLEANUPLIMIT", "REAL", "CHECKPROC",
  "CHECKFILESYS", "CHECKFILE", "CHECKDIR", "CHECKHOST", "CHECKSYSTEM",
  "CHECKFIFO", "CHECKPROGRAM", "CHECKNET", "THREADS", "CHILDREN", "METHOD",
  "GET", "HEAD", "STATUS", "ORIGIN", "VERSIONOPT", "READ", "WRITE",
  "OPERATION", "SERVICETIME", "DISK", "RESOURCE", "MEMORY", "TOTALMEMORY",
  "LOADAVG1", "LOADAVG5", "LOADAVG15", "SWAP", "MODE", "ACTIVE", "PASSIVE",
  "MANUAL", "ONREBOOT", "NOSTART", "LASTSTATE", "CORE", "CPU", "TOTALCPU",
  "CPUUSER", "CPUSYSTEM", "CPUWAIT", "CPUNICE", "CPUHARDIRQ", "CPUSOFTIRQ",
  "CPUSTEAL", "CPUGUEST", "CPUGUESTNICE", "GROUP", "REQUEST", "DEPENDS",
  "BASEDIR", "SLOT", "EVENTQUEUE", "SECRET", "HOSTHEADER", "UID", "EUID",
  "GID", "MMONIT", "INSTANCE", "USERNAME", "PASSWORD", "DATABASE", "TIME",
  "ATIME", "CTIME", "MTIME", "CHANGED", "MILLISECOND", "SECOND", "MINUTE",
  "HOUR", "DAY", "MONTH", "SSLV2", "SSLV3", "TLSV1", "TLSV11", "TLSV12",
  "TLSV13", "CERTMD5", "AUTO", "NOSSLV2", "NOSSLV3", "NOTLSV1", "NOTLSV11",
  "NOTLSV12", "NOTLSV13", "BYTE", "KILOBYTE", "MEGABYTE", "GIGABYTE",
  "INODE", "SPACE", "TFREE", "PERMISSION", "SIZE", "MATCH", "NOT",
  "IGNORE", "ACTION", "UPTIME", "RESPONSETIME", "EXEC", "UNMONITOR",
  "PING", "PING4", "PING6", "ICMP", "ICMPECHO", "NONEXIST", "EXIST",
  "INVALID", "DATA", "RECOVERED", "PASSED", "SUCCEEDED", "URL", "CONTENT",
  "PID", "PPID", "FSFLAG", "REGISTER", "CREDENTIALS", "URLOBJECT",
  "ADDRESSOBJECT", "TARGET", "TIMESPEC", "HTTPHEADER", "MAXFORWARD",
  "FIPS", "SECURITY", "ATTRIBUTE", "FILEDESCRIPTORS", "GREATER",
  "GREATEROREQUAL", "LESS", "LESSOREQUAL", "EQUAL", "NOTEQUAL", "'{'",
  "'}'", "':'", "'@'", "'['", "']'", "$accept", "cfgfile",
  "statement_list", "statement", "optproclist", "optproc", "optfilelist",
  "optfile", "optfilesyslist", "optfilesys", "optdirlist", "optdir",
  "opthostlist", "opthost", "optnetlist", "optnet", "optsystemlist",
  "optsystem", "optfifolist", "optfifo", "optprogramlist", "optprogram",
  "setalert", "setdaemon", "setterminal", "startdelay", "setinit",
  "setonreboot", "setexpectbuffer", "setlimits", "limitlist", "limit",
  "setfips", "setlog", "seteventqueue", "setidfile", "setstatefile",
  "setpid", "setmmonits", "mmonitlist", "mmonit", "mmonitoptlist",
  "mmonitopt", "credentials", "setssl", "ssl", "ssloptionlist",
  "ssloption", "sslexpire", "expireoperator", "sslchecksum",
  "checksumoperator", "sslversionlist", "sslversion", "certmd5",
  "setmailservers", "setmailformat", "mailserverlist", "mailserver",
  "mailserveroptlist", "mailserveropt", "sethttpd", "httpdlist",
  "httpdoption", "pemfile", "clientpemfile", "allowselfcert", "httpdport",
  "httpdsocket", "httpdsocketoptionlist", "httpdsocketoption", "sigenable",
  "sigdisable", "signature", "bindaddress", "allow", "$@1", "$@2", "$@3",
  "$@4", "allowuserlist", "allowuser", "readonly", "checkproc",
  "checkfile", "checkfilesys", "checkdir", "checkhost", "checknet",
  "checksystem", "checkfifo", "checkprogram", "start", "stop", "restart",
  "argumentlist", "useroptionlist", "argument", "useroption", "username",
  "password", "database", "hostname", "connection", "connectionoptlist",
  "connectionopt", "connectionurl", "connectionurloptlist",
  "connectionurlopt", "connectionunix", "connectionuxoptlist",
  "connectionuxopt", "icmp", "icmpoptlist", "icmpopt", "host", "port",
  "unixsocket", "ip", "type", "typeoptlist", "typeopt", "outgoing",
  "protocol", "sendexpect", "websocketlist", "websocket", "smtplist",
  "smtp", "mqttlist", "mqtt", "mysqllist", "mysql", "postgresqllist",
  "postgresql", "target", "maxforward", "siplist", "sip", "httplist",
  "http", "status", "method", "request", "responsesum", "hostheader",
  "httpheaderlist", "secret", "radiuslist", "radius", "apache_stat_list",
  "apache_stat", "exist", "pid", "ppid", "uptime", "responsetime",
  "icmpcount", "icmpsize", "icmptimeout", "icmpoutgoing", "stoptimeout",
  "starttimeout", "restarttimeout", "programtimeout", "nettimeout",
  "connectiontimeout", "retry", "actionrate", "urloption", "urloperator",
  "alert", "alertmail", "noalertmail", "eventoptionlist", "eventoption",
  "formatlist", "formatoptionlist", "formatoption", "every", "mode",
  "onreboot", "group", "depend", "dependlist", "dependant", "statusvalue",
  "resourceprocess", "resourceprocesslist", "resourceprocessopt",
  "resourcesystem", "resourcesystemlist", "resourcesystemopt",
  "resourcecpuproc", "resourcecpu", "resourcecpuid", "resourcemem",
  "resourcememproc", "resourceswap", "resourcethreads", "resourcechild",
  "resourceload", "resourceloadavg", "coremultiplier", "resourceread",
  "resourcewrite", "value", "timestamptype", "timestamp", "operator",
  "time", "totaltime", "currenttime", "repeat", "action", "action1",
  "action2", "rateXcycles", "rateXYcycles", "rate1", "rate2",
  "recovery_success", "recovery_failure", "checksum", "hashtype", "inode",
  "space", "read", "write", "servicetime", "fsflag", "unit", "permission",
  "programmatch", "match", "matchflagnot", "size", "uid", "euid",
  "secattr", "filedescriptorssystem", "filedescriptorsprocess",
  "filedescriptorsprocesstotal", "gid", "linkstatus", "linkspeed",
  "linksaturation", "upload", "download", "icmptype", "reminder", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-1364)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-752)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     872,    42,   -53,     3,   126,   152,   185,   200,   215,   219,
     225,   117,   872, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364,    36,     9,   251, -1364, -1364,   216,   107,   255,   296,
     154,   280,   368,   400,   176,   -11,    54,   258, -1364,   -43,
     -35,   472,   501,   512,   560, -1364,   517,   519,    70, -1364,
   -1364,  1054,   105,  1103,  1548,  1587,  1592,  1640,  1548,  1670,
     595, -1364,   534,   553,    -8, -1364,  1742, -1364, -1364, -1364,
   -1364, -1364,   751, -1364, -1364,   994, -1364, -1364, -1364,   452,
     464, -1364,   258,   353,   340,   345,  1439,   601,   524,   535,
     475,   542,   539,   548,   587,   559,   591,   596,   614,   104,
     591,   591,   581,   591,  -103,   468,   511,    19,   618,   621,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364,   -41, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364,    96,  -134, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,   169, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
     233, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364,    29, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364,   503, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,  1147,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
      -3, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364,   633,   752, -1364,   632,   717,   636,
   -1364,   709,     8,   671,   680,   735,   740,   541,   705, -1364,
     699,   712,   710, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364,    86,    69, -1364, -1364, -1364,
   -1364, -1364,   580,   593, -1364, -1364,    49, -1364,   665, -1364,
     675,   353,   607, -1364,   994,  1439, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364,   435, -1364,   738, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364,   461, -1364, -1364, -1364,   408,   599,   799,   515,
     515,   515,   515,   582,   515,   515, -1364, -1364, -1364,   515,
     515,   544,   664,   515,   804,    22,   515,  1668, -1364, -1364,
   -1364, -1364, -1364, -1364,   770, -1364, -1364,   616,   714, -1364,
     806,   911, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364,   621, -1364,   673,  1439,   601,   151, -1364, -1364,
   -1364, -1364,   242,   515,   664,   531,   515,   719, -1364,   531,
     722,  -113,   515,   515,   515,   -58,   918,   930,   566,   352,
     376,   949,   802,   515,   515,   515,   824,   956,   515,   515,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
     515,  1606, -1364, -1364,   515, -1364, -1364, -1364,   515,   834,
     531, -1364,   865, -1364,   923,   377,   885, -1364, -1364, -1364,
   -1364, -1364, -1364,   887, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,   800,   903,
   -1364,   906,   922,   924,   764,   947,   962, -1364, -1364, -1364,
   -1364, -1364, -1364,  1068, -1364, -1364, -1364,   810,   812,   837,
     842,   851,   852,   853,   862,   867,   869, -1364, -1364,   871,
     873,   876,   888,   894,   895,   900,   908,   909, -1364, -1364,
   -1364, -1364, -1364, -1364,   955,   957, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364,   383,   625,  1060, -1364,  1113,  1028,   187,
     236,   143, -1364, -1364, -1364,  1062,  1065,   253,   337,   436,
     934,   928,  1142, -1364,   515,  1073, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364,  1076,  1077,    98,    98,   515,   515,    98,
      98,    98,    98,   804,   804,   804,  1084,    23, -1364, -1364,
    1228,   959,  1142, -1364,   197, -1364,  1237, -1364,   515,  1087,
     288, -1364,  1095,   312, -1364,  1098,   317, -1364, -1364, -1364,
    1439,  1192, -1364, -1364, -1364,  1101,  1152,   804,   804,   804,
    1153,  1107, -1364, -1364,   790,  1108,   848,   889,   916,   203,
     257,   284,   804,   515,   285,   515,    98, -1364, -1364, -1364,
    1173, -1364, -1364, -1364,  1173,   804,   804,   804,  1111,  1112,
    1116,   515,   515,   804,    98,    98,   306, -1364,  1261,    98,
    1119,   804,  1129, -1364,   717,    11, -1364, -1364, -1364, -1364,
   -1364, -1364,  1130,  1132,  1133,  1135,  1141,  1068,   127, -1364,
   -1364,    28,  1143,  1144,  1145,  1149,  1146,   927,  1052,  1151,
    1154, -1364,  1134,  1148,  1150,  1162,  1163,  1165,  1168,  1175,
    1176, -1364,  1058, -1364,  1060,   601, -1364,  1094, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364,   804,   804,   804,   804,
     804,   804, -1364,   945,  1178, -1364,   502,  1179,  1270, -1364,
   -1364, -1364, -1364,   751,   751,   338,   402,   499,   801,  1183,
    1188,  1333,  1334,  1335,   929, -1364,  1287,   113, -1364, -1364,
     502,    52,  1207,   113,    98,  1127, -1364,  1138, -1364,  1139,
   -1364,  1273,  1060,   804,     4,  1355,  1359,  1361,   804,   751,
     804,   804,   929,   804,   804, -1364, -1364, -1364, -1364,  1185,
     751,  1193,   751,  1160,  1161,  1367,   410,    52,  1225,    98,
     840,   293,   293,   293,  1120, -1364,   293,   293,   293, -1364,
    1388,  1389,  1390,  1247,    -2,   262,  1250,  1252,  1397,   917,
     931,    52,  1255,   113,  1258,   804,  1403,   804,  1114,  1114,
   -1364,  1276,  1141,  1141,  1141,  1068, -1364,  1141, -1364, -1364,
   -1364, -1364,   463,   493,  1264, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364,  1360,   751,   751,
     751,   751,   864,   868,   878,   881,   890, -1364,   601, -1364,
   -1364,  1413,  1414,  1418,  1420,  1424,  1425,   133,   804,   804,
   -1364,   369,  1293,  1294,   609,  1781,  1288,  1289,   515, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364,  1433,   804,  1435,  1210,
    1210,  1263,   751,  1265,   751, -1364, -1364, -1364, -1364, -1364,
   -1364,   113,   113,   113, -1364, -1364, -1364, -1364, -1364,   804,
   -1364, -1364, -1364, -1364, -1364,   591, -1364, -1364,  1440,   133,
     369,  1442,  1446,   804,  1440, -1364, -1364, -1364, -1364,  1060,
     601,  1447,  1314,  1453,   113,   113,   113,  1454,   804,  1456,
    1458,   804,  1463,  1464,   804,  1210,   804,  1210,   804,   804,
     113,    52,  1321,  1466,   804,   938,   804,   804,  1337,  1329,
    1330,  1331, -1364, -1364, -1364, -1364, -1364, -1364,  1475,  1477,
    1484, -1364,   293,  1486,  1488,  1491,   293,   113,   113,   113,
     804,  1210,  1210,  1210,  1210,   269,   287,   113, -1364, -1364,
   -1364, -1364,  1492,   804,  1440, -1364,  1494,   113,  1495,  1362,
    1363, -1364,  1141,  1141,  1141, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,   113,   113,
     113,   113,   113,   113,    60,   531, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364,  1499,  1500,  1501,  1369, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364,  1505, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364,   920, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364,    44, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364,  1280, -1364,  1364,   113,
    1509,   137, -1364, -1364, -1364, -1364,  1210, -1364,  1210, -1364,
   -1364,  1440,  1512,   349,    53, -1364,  1513,  1514,   113,   113,
    1515, -1364,   601, -1364,   113,   804,   113,  1440, -1364, -1364,
     113,  1516,   113,   113,  1517,   113,   113,  1522,   804,  1524,
     804,  1527,  1528, -1364,  1534,   804,   113,  1535,   804,   804,
    1536,  1537, -1364, -1364,  1313, -1364,   113,   113,   113,  1544,
     113,   113,   113,  1545,  1440,  1561,  1440,  1547,   804,   804,
     804,   804,   270,   425,   513,   565,  1440,   113,  1562, -1364,
     113, -1364,   113, -1364, -1364,  1440,  1440,  1440,  1440,  1440,
    1440,  1272,  1427,   113,   113,   113, -1364,   113,  1140,   413,
     413,  1430,   515,   515,   515,   515,   515,   515,   515,   515,
     515,   515, -1364, -1364,   920, -1364,   901,   901,   -25,   -25,
    1443,  1445,  1438,  1432,    44, -1364,   901,   583,   134,  1366,
   -1364,   897,  1440,   113, -1364, -1364, -1364, -1364, -1364,   113,
    1489,    89, -1364,   626, -1364, -1364,   113,   113,  1561,  1440,
     113, -1364,  1440,  1586,  1440, -1364, -1364,   113, -1364, -1364,
     113, -1364, -1364,   113,  1588,   113,  1589,   113,   113,   113,
    1597,  1440,   113,  1605,  1607,   113,   113, -1364,  1440,  1440,
    1440,   113,  1561,  1561,  1561,   113, -1364,    93, -1364, -1364,
     113,  1608,  1610,  1611,  1612,   693, -1364, -1364, -1364,   804,
     693,   804,   693,   804,   693,   804, -1364,  1440,   113,  1440,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,  1470, -1364,
    1440,  1440,  1440,  1440, -1364, -1364, -1364,  1472,   960,   515,
    1010,  1480, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364,  1473,  1478,  1482,  1483,  1487,  1493,  1496,  1498,
    1502,  1503, -1364, -1364, -1364, -1364,  1538, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,  1481, -1364,
   -1364, -1364, -1364,   424,  1507, -1364, -1364, -1364,  1510, -1364,
   -1364, -1364, -1364, -1364,  1440,  1440,    30, -1364,   804,   804,
     804,  1561,  1561, -1364, -1364,  1440, -1364,   113, -1364,  1440,
    1440,  1440,   113,  1440,   113,  1440,  1440,  1440,   113, -1364,
    1440,   113,   113,  1440,  1440, -1364, -1364, -1364,  1440, -1364,
   -1364, -1364,  1561,  1619, -1364,  1440,   113,   113,   113,   113,
     804,  1628,   804,  1632,   804,  1635,   804,  1637, -1364,  1440,
   -1364,  1431, -1364, -1364, -1364, -1364, -1364, -1364, -1364,  1511,
   -1364, -1364, -1364,  -198,  1520,  1521,  1523,  1525,  1526,  1530,
    1531,  1532,  1539,  1541,    15, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364,  1602, -1364, -1364,  1661,  1666,  1671, -1364,
   -1364, -1364,  1440, -1364, -1364, -1364,  1440, -1364,  1440, -1364,
   -1364, -1364,  1440, -1364,  1440,  1440, -1364, -1364, -1364, -1364,
     804, -1364,  1440,  1440,  1440,  1440,  1680,   113,  1688,   113,
    1689,   113,  1690,   113, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,  1114,
    1114,  1559, -1364,   113,   113,   113, -1364, -1364, -1364, -1364,
   -1364, -1364,  1694, -1364, -1364, -1364, -1364,   113,  1440,   113,
    1440,   113,  1440,   113,  1440,  1564,  1566, -1364, -1364, -1364,
   -1364,   113,  1440, -1364,  1440, -1364,  1440, -1364,  1440, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       2,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     3,     4,     6,     8,     9,    20,    22,    19,
      21,    23,    10,    11,    17,    18,    16,    12,     7,    13,
      14,    15,    33,    59,    79,   101,   118,   133,   150,   165,
     182,     0,     0,     0,   304,   203,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   225,   634,
       0,     0,     0,     0,     0,   366,     0,     0,     0,     1,
       5,    24,    25,    26,    27,    28,    32,    29,    30,    31,
     227,   226,   201,   295,   587,   291,   303,   200,   251,   232,
     233,   209,   786,   234,   600,     0,   204,   205,   206,     0,
       0,   239,   235,   246,     0,     0,     0,   837,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      34,    35,    36,    37,    48,    49,    50,    38,    39,    40,
      47,    51,    52,   634,   599,    53,    54,    55,    56,    57,
      58,    41,    42,    43,    44,    45,    46,   802,   802,    60,
      61,    62,    63,    64,    66,    68,    67,    75,    76,    77,
      78,    65,    72,    69,    74,    73,    70,    71,     0,    80,
      81,    82,    83,    84,    85,    87,    86,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,    88,    89,    90,
       0,   102,   103,   104,   105,   106,   108,   110,   109,   114,
     115,   116,   117,   107,   111,   112,   113,     0,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,     0,   134,   135,   136,   137,   143,   147,   144,
     145,   146,   148,   149,   138,   139,   140,   141,   142,     0,
     151,   152,   153,   154,   163,   155,   156,   157,   158,   159,
     160,   161,   162,   164,   166,   167,   168,   169,   170,   172,
     174,   173,   178,   179,   180,   181,   171,   175,   176,   177,
       0,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,     0,     0,   199,     0,   293,     0,
     292,   390,     0,     0,     0,     0,     0,   249,     0,   317,
       0,     0,     0,   306,   305,   307,   308,   309,   313,   314,
     332,   333,   310,   311,   312,     0,     0,   787,   788,   789,
     790,   207,     0,     0,   640,   641,     0,   636,   229,   231,
     238,   246,     0,   236,     0,     0,   618,   605,   606,   620,
     621,   628,   626,   608,   630,   607,   629,   625,   632,   614,
     616,   631,   622,   627,   604,   633,   611,   615,   619,   612,
     617,   610,   609,   623,   624,   613,     0,   602,     0,   196,
     355,   356,   357,   358,   361,   360,   359,   362,   363,   367,
     380,   381,   585,   376,   364,   365,   443,     0,     0,   718,
     718,   718,   718,     0,   718,   718,   699,   700,   701,   718,
     718,     0,     0,   718,   751,   443,   718,   751,   659,   661,
     662,   663,   664,   665,   702,   666,   667,   581,   579,   601,
     583,     0,   643,   644,   645,   646,   647,   648,   649,   650,
     651,   655,   652,   653,     0,     0,   837,   768,   712,   713,
     714,   715,   768,   718,   803,     0,   718,     0,   803,     0,
       0,     0,   718,   718,   718,     0,   718,   718,     0,   443,
     443,     0,     0,   718,   718,   718,     0,     0,   718,   718,
     687,   678,   679,   680,   681,   682,   683,   684,   685,   686,
     718,   751,   669,   674,   718,   672,   673,   671,   718,     0,
       0,   228,     0,   295,     0,     0,     0,   275,   277,   279,
     281,   283,   285,     0,   287,   276,   278,   280,   282,   284,
     286,   299,   300,   301,   302,   296,   297,   298,     0,     0,
     289,     0,     0,     0,   349,   337,     0,   334,   315,   329,
     331,   251,   316,   353,   320,   328,   330,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   248,   252,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   208,   210,
     638,   639,   290,   637,     0,     0,   240,   242,   243,   244,
     245,   237,   247,     0,     0,   634,   603,   838,     0,     0,
       0,   585,   377,   378,   368,     0,     0,     0,     0,     0,
       0,     0,     0,   420,   718,     0,   725,   719,   720,   721,
     722,   723,   724,     0,     0,     0,     0,   718,   718,     0,
       0,     0,     0,   751,   751,   751,     0,     0,   752,   753,
       0,     0,     0,   420,     0,   660,     0,   703,   718,     0,
     581,   370,     0,   579,   372,     0,   583,   374,   642,   654,
       0,     0,   596,   769,   770,     0,     0,   751,   751,   751,
       0,     0,   594,   595,     0,     0,     0,     0,     0,     0,
       0,     0,   751,   718,     0,   718,     0,   436,   436,   436,
       0,   436,   436,   436,     0,   751,   751,   751,     0,     0,
       0,   718,   718,   751,     0,     0,     0,   670,     0,     0,
       0,   751,     0,   202,   294,   271,   387,   386,   388,   288,
     588,   391,   338,   339,   340,     0,     0,   353,     0,   354,
     318,   319,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   273,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   230,     0,   635,   634,   837,   839,     0,   382,   384,
     383,   385,   379,   369,   444,   446,   751,   751,   751,   751,
     751,   751,   410,     0,     0,   394,   751,     0,     0,   696,
     697,   711,   710,   786,   786,     0,     0,   786,   786,     0,
       0,     0,     0,     0,   726,   749,     0,     0,   410,   394,
     751,   711,     0,     0,     0,     0,   371,     0,   373,     0,
     375,     0,   634,   751,   751,     0,     0,     0,   751,   786,
     751,   751,   726,   751,   751,   797,   796,   801,   800,   711,
     786,   711,   786,   711,     0,     0,     0,   711,     0,     0,
     786,   751,   751,   751,     0,   436,   751,   751,   751,   436,
       0,     0,     0,     0,   786,   786,     0,     0,     0,   786,
     786,   711,     0,     0,     0,   751,     0,   751,   271,   271,
     272,     0,     0,     0,     0,   353,   352,   342,   350,   336,
     250,   327,     0,     0,     0,   321,   259,   261,   260,   258,
     262,   255,   256,   253,   254,   263,   264,   257,   786,   786,
     786,   786,     0,     0,     0,     0,     0,   241,   837,   197,
     586,     0,     0,     0,     0,     0,     0,   751,   751,   751,
     445,   751,     0,     0,     0,     0,     0,     0,   718,   421,
     422,   423,   424,   426,   425,   427,     0,   751,     0,   735,
     735,   711,   786,   711,   786,   691,   690,   693,   692,   675,
     676,     0,     0,     0,   727,   728,   729,   730,   731,   751,
     750,   744,   745,   740,   743,     0,   746,   747,   757,   751,
     751,     0,     0,   751,   757,   698,   582,   580,   584,   634,
     837,     0,     0,     0,     0,     0,     0,     0,   751,     0,
       0,   751,     0,     0,   751,   735,   751,   735,   751,   751,
       0,   711,     0,     0,   751,   786,   751,   751,     0,     0,
       0,     0,   437,   442,   438,   439,   440,   441,     0,     0,
       0,   836,   751,     0,     0,     0,   751,     0,     0,     0,
     751,   735,   735,   735,   735,   786,   786,     0,   689,   688,
     695,   694,     0,   751,   757,   677,     0,     0,     0,     0,
       0,   268,   344,   346,   348,   335,   351,   322,   324,   323,
     325,   326,   274,   211,   212,   213,   214,   215,   216,   217,
     218,   221,   222,   219,   220,   223,   224,   198,     0,     0,
       0,     0,     0,     0,     0,     0,   415,   417,   416,   411,
     413,   414,   412,     0,     0,     0,     0,   447,   448,   405,
     407,   406,   395,   396,   397,   403,   398,   399,   402,   401,
     404,   400,     0,   495,   496,   451,   449,   452,   459,   530,
     530,     0,   463,   503,   503,   480,   481,   467,   468,   458,
     477,   478,   511,   511,   460,     0,   507,   492,   461,   469,
     470,   484,   486,   493,   518,   482,   526,   471,   464,   550,
     472,   485,   473,   487,   491,   462,     0,   590,     0,     0,
       0,     0,   736,   704,   707,   706,   735,   709,   735,   570,
     571,   757,     0,   737,     0,   569,     0,     0,     0,     0,
       0,   658,   837,   597,     0,   751,     0,   757,   805,   717,
       0,     0,     0,     0,     0,     0,     0,     0,   751,     0,
     751,     0,     0,   785,     0,   751,     0,     0,   751,   751,
       0,     0,   578,   575,     0,   576,     0,     0,     0,     0,
       0,     0,     0,     0,   757,   762,   757,     0,   751,   751,
     751,   751,     0,     0,     0,     0,   757,     0,     0,   668,
       0,   657,     0,   269,   270,   757,   757,   757,   757,   757,
     757,   266,     0,     0,     0,     0,   456,     0,   450,   465,
     466,     0,   718,   718,   718,   718,   718,   718,   718,   718,
     718,   718,   555,   556,   457,   553,   489,   490,   475,   476,
       0,     0,     0,     0,   494,   497,   474,   479,   488,   483,
     589,     0,   757,     0,   592,   591,   705,   708,   568,     0,
       0,   737,   741,     0,   748,   758,     0,     0,   762,   757,
       0,   598,   757,     0,   757,   792,   767,     0,   795,   794,
       0,   799,   798,     0,     0,     0,     0,     0,     0,     0,
       0,   757,     0,     0,     0,     0,     0,   577,   757,   757,
     757,     0,   762,   762,   762,     0,   819,     0,   821,   820,
       0,     0,     0,     0,     0,     0,   732,   733,   734,   751,
       0,   751,     0,   751,     0,   751,   822,   757,     0,   757,
     793,   806,   807,   808,   809,   817,   818,   267,     0,   593,
     757,   757,   757,   757,   454,   455,   453,     0,     0,   718,
       0,     0,   547,   532,   533,   531,   536,   537,   534,   535,
     538,   557,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   554,   505,   506,   504,     0,   513,   514,   512,
     501,   499,   502,   500,   498,   509,   510,   508,     0,   520,
     521,   522,   519,     0,     0,   528,   529,   527,     0,   552,
     551,   573,   574,   418,   757,   757,     0,   742,   754,   754,
     754,   762,   762,   419,   814,   757,   791,     0,   765,   757,
     757,   757,     0,   757,     0,   757,   757,   757,     0,   771,
     757,     0,     0,   757,   757,   429,   430,   431,   757,   433,
     434,   435,   762,     0,   763,   757,     0,     0,     0,     0,
     751,     0,   751,     0,   751,     0,   751,     0,   812,   757,
     656,     0,   408,   810,   811,   392,   545,   541,   542,     0,
     544,   543,   546,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   271,   389,   524,   523,   525,   549,
     816,   572,   738,     0,   755,   756,     0,     0,     0,   409,
     393,   815,   757,   804,   716,   780,   757,   782,   757,   783,
     784,   773,   757,   772,   757,   757,   776,   775,   428,   432,
     754,   823,   757,   757,   757,   757,     0,     0,     0,     0,
       0,     0,     0,     0,   813,   265,   540,   548,   539,   558,
     559,   560,   561,   562,   563,   564,   565,   566,   567,   271,
     271,     0,   739,     0,     0,     0,   766,   779,   781,   774,
     778,   777,     0,   827,   824,   833,   830,     0,   757,     0,
     757,     0,   757,     0,   757,     0,     0,   515,   759,   760,
     761,     0,   757,   828,   757,   825,   757,   834,   757,   831,
     516,   517,   764,   829,   826,   835,   832
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
   -1364, -1364, -1364,  1695, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
    1598, -1364, -1364,  1365, -1364,   -83,  1167, -1364,  -781, -1364,
    -320,  -846, -1364,  -339,  -338, -1364, -1364, -1364,  1626,  1214,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
     186,  -706,  -647, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364, -1364,  1230,  1485,  1796,  -104,  -404,  -384,  -555,  -481,
    -428, -1364, -1364,  1643,   935, -1364,  1648,   937, -1364, -1364,
    1093, -1364, -1364,    46, -1364,  -350,  1096,  1312, -1364,  -744,
   -1364, -1364, -1364,  -729,  -681, -1364,   455,   622, -1364, -1364,
   -1364,   612, -1364, -1364, -1364, -1364, -1364, -1364, -1364,   631,
   -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364,   479,   899, -1364, -1364,  1655,  -700, -1364, -1364, -1364,
   -1364,  1104,  1109,  1102,  1159, -1364,  -294,  -283,  1848,  -640,
    -437,  1856,  1743, -1364,  -318,  -351,  -139,  1407,  -296,  1864,
    1872,  1880,  1888,  1896, -1364,  1311, -1364, -1364, -1364,  1338,
   -1364, -1364,  1271, -1364, -1364, -1364, -1364, -1364, -1364, -1364,
   -1364,  -218, -1364, -1364, -1364, -1364,   958,  -215,   365,  -395,
     951,  -461,  -550,   473, -1136,  -200, -1256, -1363,  -943,  -417,
   -1146,   -29,  -751, -1364,  1322, -1364, -1364, -1364, -1364, -1364,
   -1364,   170,   611, -1364, -1364,  1620, -1364,   921, -1364, -1364,
   -1364, -1364, -1364,   944, -1364, -1364, -1364, -1364, -1364,  1081,
    -435
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
       0,    11,    12,    13,    71,   130,    72,   159,    73,   179,
      74,   201,    75,   218,    76,   233,    77,   250,    78,   264,
      79,   281,    14,    15,    16,   296,    17,    18,    19,    20,
     326,   569,    21,    22,    23,    24,    25,    26,    27,   102,
     103,   340,   576,   343,    28,   521,   325,   558,  1077,  1368,
     522,   861,   887,   523,   524,    29,    30,    84,    85,   298,
     525,    31,    86,   314,   315,   316,   317,   318,   319,   721,
     875,   320,   321,   322,   323,   324,   716,   862,   863,   864,
     867,   868,   720,    32,    33,    34,    35,    36,    37,    38,
      39,    40,   131,   132,   133,   392,   591,   393,   593,   526,
     527,  1421,   530,   134,   911,  1092,   135,   907,  1079,   136,
     766,   919,   224,   831,  1002,   602,   765,   603,  1093,   920,
    1248,  1376,  1095,   921,   922,  1274,  1275,  1266,  1405,  1276,
    1417,  1268,  1409,  1277,  1422,  1425,  1426,  1278,  1427,  1249,
    1385,  1386,  1387,  1388,  1389,  1390,  1503,  1429,  1279,  1430,
    1264,  1265,   137,   138,   139,   140,  1003,  1004,  1005,  1006,
    1007,   644,   641,   647,   594,   301,   924,   925,   141,  1082,
     664,   142,   143,   144,   376,   377,   107,   336,   337,   145,
     146,   147,   148,   149,   442,   443,   292,   150,   417,   418,
     262,   491,   492,   419,   493,   494,   495,   420,   496,   421,
     422,   423,   424,   638,   425,   426,   773,   456,   171,   613,
     949,  1349,  1153,  1292,   957,   958,  1295,   628,   629,   630,
    1526,  1165,  1338,   172,   656,   191,   192,   193,   194,   195,
     196,   331,   173,   293,   174,   457,   175,   151,   152,   153,
     263,   154,   155,   156,   244,   245,   246,   247,   248,   835,
     379
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     636,   579,   580,   313,   446,   614,   615,   616,   592,   619,
     620,   652,  1039,  1040,   621,   622,   427,   428,   626,   430,
     578,   634,   667,   640,   643,   586,   646,   584,  1294,   531,
     532,   497,   533,   858,   859,   469,   752,  1579,  1580,   108,
     573,  1021,   109,   592,   592,   431,   592,    80,   871,   104,
      41,   104,    42,  1406,    43,    44,  1293,  -751,   661,    45,
      46,   665,   972,   702,    96,   632,   923,   669,   670,   671,
     869,   674,   676,    47,   698,  1524,  1524,  1524,   688,   689,
     690,  1474,   785,   694,   695,   752,   117,   299,   752,  1522,
     923,   752,  1567,    60,   437,   696,  1473,    48,    49,   699,
    1241,   595,   447,   700,   597,    50,   599,    51,   157,  1568,
     396,   118,   547,   548,   549,   596,    52,    69,   550,   551,
     632,   552,   553,  1270,   554,   555,   458,   651,   951,   952,
    1090,    83,    53,   559,    54,   560,   561,   562,   563,   564,
     565,   566,   567,   953,   655,   398,   459,   534,   535,    61,
     954,  1290,   627,   547,   548,   549,   397,    82,   705,   550,
     551,  1046,   552,   553,   307,   554,   555,  1094,   951,   952,
    1074,   786,   498,   653,   654,   461,    81,   398,  1523,  1090,
     120,   121,  1096,   953,    97,    98,   432,  1524,   951,   952,
     954,   332,   333,   334,   335,    53,   122,   505,   506,   657,
     627,  1294,   123,   953,   124,   125,   781,   782,   783,   767,
     954,  1098,   951,   952,   438,   439,  1094,   105,  1045,   444,
    1271,  1272,   775,   776,   110,   672,   499,   953,   916,   917,
    1097,  1096,  1284,   586,   954,    87,    55,   659,   588,   461,
     805,   806,   807,   794,   398,   872,   771,   873,   327,   328,
     329,   330,   398,   659,  1273,   825,    56,   577,   772,   106,
    1098,   445,    57,   556,   653,   654,    99,   100,   840,   841,
     842,  1101,    62,   497,   399,   400,   848,   500,   826,  1097,
     829,   627,   401,   402,   856,   874,   403,   573,   404,   405,
     406,   407,   408,  1527,  1528,   126,   846,   847,    63,   127,
     586,   631,   409,   410,   556,  1023,   589,   470,   590,   998,
     899,   860,  1222,   536,   128,   860,   129,   398,   955,   956,
    1101,   448,   449,   450,   451,   452,   748,  1608,  1609,  1610,
    1224,    64,   801,   411,    58,   749,  1046,  1046,  1046,   901,
     902,   903,   904,   905,   906,   791,    65,   462,   463,   926,
     464,   819,   572,   999,   453,  1622,   454,   772,   955,   956,
     589,    66,   590,   772,   412,    67,   158,   413,   597,   414,
     599,    68,   568,   961,   962,   750,   455,   414,   955,   956,
    1154,   398,   415,   639,   751,  1086,   971,   973,  1000,   557,
      83,   977,   756,   979,   980,    89,   982,   983,   465,   416,
     307,   757,   955,   956,  1592,   821,  1074,   642,   655,    88,
     993,  1290,   645,  1075,  1008,  1009,  1010,   772,  1345,  1013,
    1014,  1015,  1423,   466,   467,  1424,   912,   913,    92,   412,
     870,   595,   823,   827,  1032,  1188,    90,  1190,  1036,   213,
    1038,   627,   414,   276,   772,   772,   745,  1294,  1294,  1294,
     586,  1087,  1088,   914,   851,   595,    91,   915,   448,   449,
     450,   451,   468,  1067,   916,   917,   772,   448,   449,   450,
     451,  1218,  1219,  1220,  1221,  1294,   758,   346,    95,   347,
     348,   349,   350,   351,   352,   759,   931,   595,   390,   391,
    1083,  1084,  1085,   412,  1102,  1525,  1525,  1525,   772,   657,
     658,   596,  1346,  1347,  1348,   589,   414,   590,    93,   471,
    1150,  1377,   327,   328,   329,   330,   706,   627,   707,   327,
     328,   329,   330,  1148,   353,   332,   333,   334,   335,   589,
     354,   590,  1162,   355,   589,  1173,   590,   327,   328,   329,
     330,    94,  1166,  1167,   101,   472,  1170,  1443,  1052,   111,
     933,  1001,   473,   474,   475,   476,   588,   918,   991,   912,
     913,  1181,   772,  1516,  1184,  1517,   589,  1187,   590,  1189,
     772,  1191,  1192,  1350,  1194,   760,   114,  1197,   112,  1200,
    1201,  1469,  1470,  1471,   761,  1378,   914,  1078,  1379,   113,
     915,  1091,   338,   964,   115,  1209,   116,   916,   917,  1213,
     390,   391,  1047,  1217,   294,   898,  1286,  1525,  1287,   295,
     356,  1048,   339,  1080,   382,   383,  1228,  1099,   357,   677,
     678,   679,   680,  1380,  1081,   597,   598,   599,  1100,  1381,
    1262,   600,  1049,   918,   297,   505,   506,   342,  1242,  1078,
    1091,  1050,   344,   681,   682,   683,   684,   345,   935,  1075,
     627,   398,   358,  1034,   359,   631,   360,  1346,  1347,  1348,
     361,  1352,   378,   970,   380,  1080,  1099,   346,  1581,   347,
     348,   349,   350,   351,   352,   381,  1081,  1100,   589,   386,
     590,   384,   385,  1263,   197,   214,   743,   600,   387,   277,
    1529,  1530,   362,   363,  1105,  1106,  1107,   364,   365,   389,
     366,   601,   434,   435,   436,   367,   307,   368,   369,   370,
     371,   639,   504,  1354,   353,   372,   373,   374,   375,  1382,
     354,  1549,   429,   355,   832,   833,   388,   836,   837,   838,
     390,   391,   477,  1605,  1606,   394,   752,  1301,   585,   545,
     546,  1159,  1160,  1161,   606,  1346,  1347,  1348,   307,   327,
     328,   329,   330,   395,   504,   390,   391,   433,  1303,  1291,
     617,   618,  1351,  1353,  1355,   440,   918,   441,  1383,  1383,
     575,  1314,   501,  1316,  1177,  1178,  1179,   502,  1320,   592,
     503,  1323,  1324,  1262,   528,  1403,  1403,  1407,  1407,   529,
    1193,   448,   449,   450,   451,  1415,  1419,  1346,  1347,  1348,
     356,  1341,  1342,  1343,  1344,   505,   506,  1418,   357,   642,
     537,   607,   608,   609,   610,   611,   612,  1214,  1215,  1216,
     538,  1384,  1384,   657,  1076,   623,   624,  1226,  1089,   539,
    1172,   662,   663,   589,   540,   590,  1263,  1231,  1404,  1404,
    1408,  1408,   358,   541,   359,   542,   360,   543,  1416,  1420,
     361,  1163,   544,   390,   391,   686,   687,  1392,  1393,  1394,
    1395,  1396,  1397,  1398,  1399,  1400,  1401,   570,  1235,  1236,
    1237,  1238,  1239,  1240,   691,   692,  1076,  1089,   574,     1,
     571,  1012,   362,   363,  1480,  1016,   587,   364,   365,  1482,
     366,  1484,   582,  1486,   604,   367,   605,   368,   369,   370,
     371,   645,  1438,  1439,  1440,   372,   373,   374,   375,  1374,
    1375,   507,   508,   509,   510,   511,   512,   513,   514,   515,
     516,   517,   518,   519,   520,  1346,  1347,  1348,   744,   810,
     811,   589,  1481,   590,  1483,  1171,  1485,   625,  1487,   505,
     506,   881,   882,   929,   930,   390,   391,   936,   938,  1282,
     937,  1285,   627,   507,   508,   509,   510,   511,   512,   513,
     514,   515,   516,   517,   518,   519,   520,   637,  1298,  1299,
     648,   163,   183,   205,  1302,   650,  1304,   268,   666,   978,
    1306,   668,  1308,  1309,  1499,  1311,  1312,   813,   814,   996,
     985,   685,   987,   176,   198,   215,  1321,  1251,   693,   278,
     997,   327,   328,   329,   330,  1229,  1328,  1329,  1330,   701,
    1332,  1333,  1334,   703,  1022,  1024,   177,   199,   216,  1029,
    1031,   705,   279,   589,   708,   590,   709,  1357,   815,   816,
    1359,   710,  1360,     2,     3,     4,     5,     6,     7,     8,
       9,    10,   711,  1370,  1371,  1372,   712,  1373,  1042,  1043,
    1044,   327,   328,   329,   330,   817,   818,   119,  1053,  1054,
    1055,  1056,   713,  1556,   714,  1558,  1028,  1560,   715,  1562,
    1252,  1253,  1254,  1255,  1256,  1257,  1258,  1259,  1260,  1261,
    1030,   883,   884,  1434,   908,   909,  -341,  1198,   719,  1435,
     327,   328,   329,   330,  1057,  1058,  1441,  1442,  1059,  1060,
    1445,   717,  1156,   741,  1158,   742,   178,  1449,  1061,  1062,
    1450,  1063,  1064,  1451,   722,  1453,   723,  1455,  1456,  1457,
    1065,  1066,  1460,   505,   506,  1463,  1464,  1431,  1432,   120,
     121,  1468,  1288,  1497,  1498,  1472,   332,   333,   334,   335,
    1475,   724,   505,   506,    53,   122,   725,   606,  1305,  1500,
    1501,   123,   104,   124,   125,   726,   727,   728,  1489,   606,
     944,   945,   946,   947,   948,  1199,   729,   327,   328,   329,
     330,   730,   746,   731,   673,   732,   747,   733,   120,   121,
     734,   327,   328,   329,   330,  1336,   675,  1339,   327,   328,
     329,   330,   735,    53,   122,  1223,  1225,  1356,   736,   737,
     123,   754,   124,   125,   738,   755,  1361,  1362,  1363,  1364,
    1365,  1366,   739,   740,   607,   608,   609,   610,   611,   612,
     762,   768,   763,   764,   769,   770,   607,   608,   609,   610,
     611,   612,   784,   787,   346,   795,   347,   348,   349,   350,
     351,   352,   793,   797,   126,   788,   799,  1532,   127,   803,
     804,   808,  1536,  1433,  1538,   809,   812,   834,  1542,   843,
     844,  1544,  1545,   128,   845,   129,   853,   855,   857,  -343,
    1444,  -345,  -347,  1446,   865,  1448,  1552,  1553,  1554,  1555,
     866,   353,   888,   876,   877,   878,   880,   354,   879,   897,
     355,   885,  1459,   126,   886,   398,   889,   127,   890,  1465,
    1466,  1467,   160,   180,   202,   219,   234,   251,   265,   282,
     891,   892,   128,   893,   129,   346,   894,   347,   348,   349,
     350,   351,   352,   895,   896,   900,   910,   927,  1488,   928,
    1490,   478,   939,   406,   407,   408,   479,   940,   941,   942,
     943,  1492,  1493,  1494,  1495,   480,   950,   481,   482,   483,
     484,   485,   486,   487,   488,   489,   963,  1598,   966,  1600,
     974,  1602,   353,  1604,   975,   984,   976,   356,   354,   967,
     968,   355,   990,   986,   994,   357,   507,   508,   509,   510,
     511,   512,   513,   514,   515,   516,   517,   518,   519,   520,
     988,  1011,   989,  1017,  1018,  1019,  1020,  1612,  1025,  1614,
    1026,  1616,  1027,  1618,  1033,  1520,  1521,  1035,  1037,   358,
     413,   359,  1051,   360,   860,  1041,  1531,   361,  1068,  1069,
    1533,  1534,  1535,  1070,  1537,  1071,  1539,  1540,  1541,  1072,
    1073,  1543,  1103,  1104,  1546,  1547,  1146,  1147,  1149,  1548,
    1151,  1152,   490,  1155,  1164,  1157,  1551,  1168,   356,   362,
     363,  1169,  1174,  1175,   364,   365,   357,   366,  1176,  1180,
    1564,  1182,   367,  1183,   368,   369,   370,   371,  1185,  1186,
    1195,  1196,   372,   373,   374,   375,  1202,  1203,  1204,  1205,
    1206,   346,  1207,   347,   348,   349,   350,   351,   352,  1208,
     358,  1210,   359,  1211,   360,   802,  1212,  1227,   361,  1230,
    1232,  1233,  1234,  1586,  1243,  1244,  1245,  1587,  1246,  1588,
    1247,  1280,  1281,  1589,  1283,  1590,  1591,  1289,  1296,  1297,
    1300,  1307,  1310,  1593,  1594,  1595,  1596,  1313,   353,  1315,
     362,   363,  1317,  1318,   354,   364,   365,   355,   366,  1319,
    1322,  1325,  1326,   367,  1327,   368,   369,   370,   371,  1331,
    1335,   200,  1340,   372,   373,   374,   375,   161,   181,   203,
     220,   235,   252,   266,   283,  1337,  1369,  1358,  1367,  1613,
    1391,  1615,  1413,  1617,   774,  1619,   969,   777,   778,   779,
     780,  1428,  1410,  1623,  1411,  1624,  1412,  1625,  1436,  1626,
     217,  1447,   792,  1452,  1454,   232,   507,   508,   509,   510,
     511,   512,  1458,   514,   515,   516,   517,   518,   519,   520,
    1461,  1496,  1462,  1476,   356,  1477,  1478,  1479,  1491,  1502,
    1515,  1504,   357,   120,   121,  1550,  1505,   820,   822,   824,
    1506,  1507,   828,  1557,   830,  1508,  1514,  1559,    53,   122,
    1561,  1509,  1563,   249,  1510,   123,  1511,   124,   125,  1519,
    1512,  1513,   849,   850,   852,  1518,   358,   854,   359,  1566,
     360,  1582,   120,   121,   361,  1565,  1583,   120,   121,  1569,
    1570,  1584,  1571,   280,  1572,  1573,  1585,    53,   122,  1574,
    1575,  1576,    53,   122,   123,  1597,   124,   125,  1577,   123,
    1578,   124,   125,  1599,  1601,  1603,   362,   363,  1607,  1611,
     341,   364,   365,  1620,   366,  1621,   581,    70,   718,   367,
     300,   368,   369,   370,   371,   120,   121,   704,   222,   372,
     373,   374,   375,   223,   960,   959,   790,   633,   789,  1414,
      53,   122,   254,   932,   934,  1269,  1267,   123,   126,   124,
     125,  1250,   127,  1402,    59,   120,   121,   798,   800,   796,
     753,   583,   965,   649,   627,   635,   302,   128,   303,   129,
      53,   122,   697,   981,  1437,   839,     0,   123,   304,   124,
     125,   305,   306,   307,   660,   308,   309,   126,   460,     0,
       0,   127,   126,     0,   992,     0,   127,   995,     0,     0,
     478,     0,   406,   407,   408,   479,   128,     0,   129,     0,
       0,   128,     0,   129,   480,     0,   481,   482,   483,   484,
     485,   486,   487,   488,   489,     0,   627,     0,     0,     0,
       0,     0,     0,   310,     0,     0,     0,     0,     0,     0,
     126,     0,     0,     0,   127,   311,   312,     0,   399,   400,
       0,     0,     0,     0,     0,     0,   401,   402,     0,   128,
     403,   129,   404,   405,   406,   407,   408,     0,     0,     0,
     126,     0,     0,     0,   127,     0,   409,   410,   162,   182,
     204,   221,   236,   253,   267,   284,     0,     0,     0,   128,
       0,   129,  1108,  1109,  1110,  1111,  1112,  1113,  1114,  1115,
    1116,  1117,  1118,  1119,  1120,  1121,  1122,  1123,  1124,  1125,
    1126,  1127,  1128,  1129,  1130,  1131,  1132,  1133,  1134,  1135,
    1136,  1137,  1138,  1139,  1140,  1141,  1142,  1143,  1144,  1145,
     164,   184,   206,   225,   237,   255,   269,   285,   165,   185,
     207,   226,   238,   256,   270,   286,   166,   186,   208,   227,
     239,   257,   271,   287,   167,   187,   209,   228,   240,   258,
     272,   288,   168,   188,   210,   229,   241,   259,   273,   289,
     169,   189,   211,   230,   242,   260,   274,   290,   170,   190,
     212,   231,   243,   261,   275,   291
};

static const yytype_int16 yycheck[] =
{
     417,   340,   340,    86,   143,   400,   401,   402,   392,   404,
     405,   446,   858,   859,   409,   410,   120,   121,   413,   123,
     340,   416,   459,   427,   428,   376,   430,   345,  1164,    21,
      22,   249,    24,    22,    23,     6,   591,    22,    23,    74,
     336,    43,    77,   427,   428,   148,   430,    11,    20,    92,
       8,    92,    10,    78,    12,    13,     3,     5,   453,    17,
      18,   456,    58,   500,    75,   415,   766,   462,   463,   464,
     717,   466,   467,    31,   491,  1438,  1439,  1440,   473,   474,
     475,  1337,    59,   478,   479,   640,    16,    95,   643,    59,
     790,   646,   290,   146,    75,   490,     3,    55,    56,   494,
      40,    79,     6,   498,   217,    63,   219,    65,     3,   307,
       6,    41,    26,    27,    28,    93,    74,     0,    32,    33,
     470,    35,    36,    79,    38,    39,   260,   445,    75,    76,
     911,   139,    90,    64,    92,    66,    67,    68,    69,    70,
      71,    72,    73,    90,   257,   148,   280,   139,   140,   146,
      97,    62,   148,    26,    27,    28,    52,   148,    98,    32,
      33,   867,    35,    36,    31,    38,    39,   911,    75,    76,
      37,   148,   175,    22,    23,     6,   140,   148,   148,   960,
      75,    76,   911,    90,   195,   196,   289,  1550,    75,    76,
      97,   142,   143,   144,   145,    90,    91,   222,   223,   257,
     148,  1337,    97,    90,    99,   100,   623,   624,   625,   604,
      97,   911,    75,    76,   195,   196,   960,   260,   865,   260,
     176,   177,   617,   618,   259,   283,   229,    90,    95,    96,
     911,   960,    95,   584,    97,    19,   194,   452,    95,     6,
     657,   658,   659,   638,   148,   217,   148,   219,   250,   251,
     252,   253,   148,   468,   210,   672,   214,   340,   160,   302,
     960,   302,   220,   177,    22,    23,   212,   213,   685,   686,
     687,   911,   146,   491,   170,   171,   693,   280,   673,   960,
     675,   148,   178,   179,   701,   257,   182,   583,   184,   185,
     186,   187,   188,  1439,  1440,   190,   691,   692,   146,   194,
     651,   279,   198,   199,   177,    43,   217,   278,   219,    16,
     745,   300,    43,   305,   209,   300,   211,   148,   265,   266,
     960,   225,   226,   227,   228,   229,   139,  1583,  1584,  1585,
      43,   146,   650,   229,   292,   148,  1042,  1043,  1044,   756,
     757,   758,   759,   760,   761,   148,   146,   178,   179,   766,
     181,   148,   303,    60,   258,  1611,   260,   160,   265,   266,
     217,   146,   219,   160,   260,   146,   261,   263,   217,   273,
     219,   146,   303,   790,   791,   139,   280,   273,   265,   266,
     930,   148,   278,    95,   148,    16,   803,   804,    95,   303,
     139,   808,   139,   810,   811,   140,   813,   814,   229,   295,
      31,   148,   265,   266,  1550,   148,    37,    95,   257,   302,
     827,    62,    95,   280,   831,   832,   833,   160,   148,   836,
     837,   838,   288,   254,   255,   291,    57,    58,   148,   260,
     303,    79,   148,   148,   851,   985,   140,   987,   855,    74,
     857,   148,   273,    78,   160,   160,   585,  1583,  1584,  1585,
     801,    82,    83,    84,   148,    79,   302,    88,   225,   226,
     227,   228,   229,   898,    95,    96,   160,   225,   226,   227,
     228,  1021,  1022,  1023,  1024,  1611,   139,    42,   302,    44,
      45,    46,    47,    48,    49,   148,   148,    79,   139,   140,
     907,   908,   909,   260,   911,  1438,  1439,  1440,   160,   257,
     258,    93,   232,   233,   234,   217,   273,   219,   140,     6,
     927,    98,   250,   251,   252,   253,   139,   148,   141,   250,
     251,   252,   253,   918,    89,   142,   143,   144,   145,   217,
      95,   219,   949,    98,   217,   970,   219,   250,   251,   252,
     253,   141,   959,   960,   286,    42,   963,  1298,   887,    77,
     148,   258,    49,    50,    51,    52,    95,   264,   148,    57,
      58,   978,   160,   139,   981,   141,   217,   984,   219,   986,
     160,   988,   989,   148,   991,   139,    16,   994,    77,   996,
     997,  1332,  1333,  1334,   148,   172,    84,   907,   175,    77,
      88,   911,   140,   793,    77,  1012,    77,    95,    96,  1016,
     139,   140,   139,  1020,     9,   744,  1156,  1550,  1158,    75,
     175,   148,   148,   907,   139,   140,  1033,   911,   183,   267,
     268,   269,   270,   210,   907,   217,   218,   219,   911,   216,
    1111,   279,   139,   264,    81,   222,   223,   284,  1075,   959,
     960,   148,   302,   267,   268,   269,   270,   302,   149,   280,
     148,   148,   217,   853,   219,   279,   221,   232,   233,   234,
     225,   148,    61,   802,   140,   959,   960,    42,  1514,    44,
      45,    46,    47,    48,    49,   140,   959,   960,   217,   140,
     219,   139,   140,  1111,    73,    74,   303,   279,   140,    78,
    1441,  1442,   257,   258,    85,    86,    87,   262,   263,   140,
     265,   293,   191,   192,   193,   270,    31,   272,   273,   274,
     275,    95,    37,   148,    89,   280,   281,   282,   283,   306,
      95,  1472,   141,    98,   678,   679,   139,   681,   682,   683,
     139,   140,   229,  1579,  1580,   139,  1291,  1172,   303,    29,
      30,   941,   942,   943,   229,   232,   233,   234,    31,   250,
     251,   252,   253,   139,    37,   139,   140,   289,  1175,  1163,
     178,   179,  1223,  1224,  1225,   147,   264,   146,  1249,  1250,
      95,  1188,   139,  1190,   974,   975,   976,    25,  1195,  1163,
     148,  1198,  1199,  1264,   148,  1266,  1267,  1268,  1269,    80,
     990,   225,   226,   227,   228,  1276,  1277,   232,   233,   234,
     175,  1218,  1219,  1220,  1221,   222,   223,   224,   183,    95,
     139,   296,   297,   298,   299,   300,   301,  1017,  1018,  1019,
     140,  1249,  1250,   257,   907,   281,   282,  1027,   911,    94,
     969,   300,   301,   217,    94,   219,  1264,  1037,  1266,  1267,
    1268,  1269,   217,   302,   219,   140,   221,   148,  1276,  1277,
     225,   955,   140,   139,   140,    53,    54,  1252,  1253,  1254,
    1255,  1256,  1257,  1258,  1259,  1260,  1261,   287,  1068,  1069,
    1070,  1071,  1072,  1073,    50,    51,   959,   960,   213,     7,
     287,   835,   257,   258,  1345,   839,   148,   262,   263,  1350,
     265,  1352,   285,  1354,   295,   270,    97,   272,   273,   274,
     275,    95,   276,   277,   278,   280,   281,   282,   283,  1248,
    1248,   236,   237,   238,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   232,   233,   234,   303,   139,
     140,   217,  1349,   219,  1351,   964,  1353,   273,  1355,   222,
     223,    14,    15,   773,   774,   139,   140,   777,   778,  1149,
     149,  1151,   148,   236,   237,   238,   239,   240,   241,   242,
     243,   244,   245,   246,   247,   248,   249,   197,  1168,  1169,
      59,    72,    73,    74,  1174,   302,  1176,    78,   259,   809,
    1180,   259,  1182,  1183,  1379,  1185,  1186,   139,   140,   149,
     820,    42,   822,    72,    73,    74,  1196,    77,    42,    78,
     830,   250,   251,   252,   253,  1034,  1206,  1207,  1208,   175,
    1210,  1211,  1212,   148,   844,   845,    72,    73,    74,   849,
     850,    98,    78,   217,   139,   219,   139,  1227,   139,   140,
    1230,   231,  1232,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   139,  1243,  1244,  1245,   140,  1247,   862,   863,
     864,   250,   251,   252,   253,   139,   140,     3,   888,   889,
     890,   891,   140,  1480,   140,  1482,   149,  1484,   304,  1486,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     149,    29,    30,  1283,   139,   140,   139,   149,    20,  1289,
     250,   251,   252,   253,   230,   231,  1296,  1297,   230,   231,
    1300,   139,   932,   148,   934,   148,     3,  1307,   230,   231,
    1310,   230,   231,  1313,   304,  1315,   304,  1317,  1318,  1319,
     230,   231,  1322,   222,   223,  1325,  1326,   230,   231,    75,
      76,  1331,  1161,   173,   174,  1335,   142,   143,   144,   145,
    1340,   304,   222,   223,    90,    91,   304,   229,  1177,   139,
     140,    97,    92,    99,   100,   304,   304,   304,  1358,   229,
     231,   232,   233,   234,   235,   995,   304,   250,   251,   252,
     253,   304,    59,   304,   256,   304,   148,   304,    75,    76,
     304,   250,   251,   252,   253,  1214,   256,  1216,   250,   251,
     252,   253,   304,    90,    91,  1025,  1026,  1226,   304,   304,
      97,   139,    99,   100,   304,   140,  1235,  1236,  1237,  1238,
    1239,  1240,   304,   304,   296,   297,   298,   299,   300,   301,
     286,   148,   294,    81,   148,   148,   296,   297,   298,   299,
     300,   301,   148,     5,    42,   148,    44,    45,    46,    47,
      48,    49,     5,   148,   190,   286,   148,  1447,   194,   148,
      98,    98,  1452,  1282,  1454,   148,   148,    84,  1458,   148,
     148,  1461,  1462,   209,   148,   211,     5,   148,   139,   139,
    1299,   139,   139,  1302,   139,  1304,  1476,  1477,  1478,  1479,
     139,    89,   148,   140,   140,   140,   140,    95,   139,   231,
      98,   140,  1321,   190,   140,   148,   148,   194,   148,  1328,
    1329,  1330,    72,    73,    74,    75,    76,    77,    78,    79,
     148,   148,   209,   148,   211,    42,   148,    44,    45,    46,
      47,    48,    49,   148,   148,   231,   148,   148,  1357,    59,
    1359,   184,   149,   186,   187,   188,   189,   149,     5,     5,
       5,  1370,  1371,  1372,  1373,   198,    59,   200,   201,   202,
     203,   204,   205,   206,   207,   208,   149,  1557,   231,  1559,
       5,  1561,    89,  1563,     5,   180,     5,   175,    95,   231,
     231,    98,     5,   180,   149,   183,   236,   237,   238,   239,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     230,   271,   231,     5,     5,     5,   149,  1597,   148,  1599,
     148,  1601,     5,  1603,   149,  1434,  1435,   149,     5,   217,
     263,   219,   148,   221,   300,   139,  1445,   225,     5,     5,
    1449,  1450,  1451,     5,  1453,     5,  1455,  1456,  1457,     5,
       5,  1460,   139,   139,  1463,  1464,   148,   148,     5,  1468,
       5,   231,   295,   180,     4,   180,  1475,     5,   175,   257,
     258,     5,     5,   139,   262,   263,   183,   265,     5,     5,
    1489,     5,   270,     5,   272,   273,   274,   275,     5,     5,
     149,     5,   280,   281,   282,   283,   139,   148,   148,   148,
       5,    42,     5,    44,    45,    46,    47,    48,    49,     5,
     217,     5,   219,     5,   221,   303,     5,     5,   225,     5,
       5,   139,   139,  1532,     5,     5,     5,  1536,   139,  1538,
       5,   231,   148,  1542,     5,  1544,  1545,     5,     5,     5,
       5,     5,     5,  1552,  1553,  1554,  1555,     5,    89,     5,
     257,   258,     5,     5,    95,   262,   263,    98,   265,     5,
       5,     5,     5,   270,   231,   272,   273,   274,   275,     5,
       5,     3,     5,   280,   281,   282,   283,    72,    73,    74,
      75,    76,    77,    78,    79,     4,   139,     5,   296,  1598,
     140,  1600,   140,  1602,   616,  1604,   303,   619,   620,   621,
     622,   215,   139,  1612,   139,  1614,   148,  1616,    99,  1618,
       3,     5,   634,     5,     5,     3,   236,   237,   238,   239,
     240,   241,     5,   243,   244,   245,   246,   247,   248,   249,
       5,   139,     5,     5,   175,     5,     5,     5,   148,   139,
     139,   148,   183,    75,    76,     6,   148,   669,   670,   671,
     148,   148,   674,     5,   676,   148,    98,     5,    90,    91,
       5,   148,     5,     3,   148,    97,   148,    99,   100,   139,
     148,   148,   694,   695,   696,   148,   217,   699,   219,   148,
     221,    59,    75,    76,   225,   234,     5,    75,    76,   149,
     149,     5,   149,     3,   149,   149,     5,    90,    91,   149,
     149,   149,    90,    91,    97,     5,    99,   100,   149,    97,
     149,    99,   100,     5,     5,     5,   257,   258,   139,     5,
     102,   262,   263,   139,   265,   139,   341,    12,   541,   270,
      84,   272,   273,   274,   275,    75,    76,   503,    75,   280,
     281,   282,   283,    75,   789,   788,   633,   415,   632,  1274,
      90,    91,    77,   775,   776,  1123,  1114,    97,   190,    99,
     100,  1110,   194,  1264,     1,    75,    76,   643,   646,   640,
     591,   344,   794,   442,   148,   417,    14,   209,    16,   211,
      90,    91,   491,   812,  1291,   684,    -1,    97,    26,    99,
     100,    29,    30,    31,   452,    33,    34,   190,   158,    -1,
      -1,   194,   190,    -1,   826,    -1,   194,   829,    -1,    -1,
     184,    -1,   186,   187,   188,   189,   209,    -1,   211,    -1,
      -1,   209,    -1,   211,   198,    -1,   200,   201,   202,   203,
     204,   205,   206,   207,   208,    -1,   148,    -1,    -1,    -1,
      -1,    -1,    -1,    81,    -1,    -1,    -1,    -1,    -1,    -1,
     190,    -1,    -1,    -1,   194,    93,    94,    -1,   170,   171,
      -1,    -1,    -1,    -1,    -1,    -1,   178,   179,    -1,   209,
     182,   211,   184,   185,   186,   187,   188,    -1,    -1,    -1,
     190,    -1,    -1,    -1,   194,    -1,   198,   199,    72,    73,
      74,    75,    76,    77,    78,    79,    -1,    -1,    -1,   209,
      -1,   211,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
      72,    73,    74,    75,    76,    77,    78,    79,    72,    73,
      74,    75,    76,    77,    78,    79,    72,    73,    74,    75,
      76,    77,    78,    79,    72,    73,    74,    75,    76,    77,
      78,    79,    72,    73,    74,    75,    76,    77,    78,    79,
      72,    73,    74,    75,    76,    77,    78,    79,    72,    73,
      74,    75,    76,    77,    78,    79
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int16 yystos[] =
{
       0,     7,   161,   162,   163,   164,   165,   166,   167,   168,
     169,   309,   310,   311,   330,   331,   332,   334,   335,   336,
     337,   340,   341,   342,   343,   344,   345,   346,   352,   363,
     364,   369,   391,   392,   393,   394,   395,   396,   397,   398,
     399,     8,    10,    12,    13,    17,    18,    31,    55,    56,
      63,    65,    74,    90,    92,   194,   214,   220,   292,   480,
     146,   146,   146,   146,   146,   146,   146,   146,   146,     0,
     311,   312,   314,   316,   318,   320,   322,   324,   326,   328,
      11,   140,   148,   139,   365,   366,   370,    19,   302,   140,
     140,   302,   148,   140,   141,   302,    75,   195,   196,   212,
     213,   286,   347,   348,    92,   260,   302,   484,    74,    77,
     259,    77,    77,    77,    16,    77,    77,    16,    41,     3,
      75,    76,    91,    97,    99,   100,   190,   194,   209,   211,
     313,   400,   401,   402,   411,   414,   417,   460,   461,   462,
     463,   476,   479,   480,   481,   487,   488,   489,   490,   491,
     495,   545,   546,   547,   549,   550,   551,     3,   261,   315,
     400,   401,   402,   460,   476,   479,   487,   488,   489,   490,
     491,   516,   531,   540,   542,   544,   545,   551,     3,   317,
     400,   401,   402,   460,   476,   479,   487,   488,   489,   490,
     491,   533,   534,   535,   536,   537,   538,   540,   545,   551,
       3,   319,   400,   401,   402,   460,   476,   479,   487,   488,
     489,   490,   491,   516,   540,   545,   551,     3,   321,   400,
     401,   402,   411,   414,   420,   476,   479,   487,   488,   489,
     490,   491,     3,   323,   400,   401,   402,   476,   479,   487,
     488,   489,   490,   491,   552,   553,   554,   555,   556,     3,
     325,   400,   401,   402,   463,   476,   479,   487,   488,   489,
     490,   491,   498,   548,   327,   400,   401,   402,   460,   476,
     479,   487,   488,   489,   490,   491,   516,   540,   545,   551,
       3,   329,   400,   401,   402,   476,   479,   487,   488,   489,
     490,   491,   494,   541,     9,    75,   333,    81,   367,    95,
     366,   473,    14,    16,    26,    29,    30,    31,    33,    34,
      81,    93,    94,   353,   371,   372,   373,   374,   375,   376,
     379,   380,   381,   382,   383,   354,   338,   250,   251,   252,
     253,   539,   142,   143,   144,   145,   485,   486,   140,   148,
     349,   348,   284,   351,   302,   302,    42,    44,    45,    46,
      47,    48,    49,    89,    95,    98,   175,   183,   217,   219,
     221,   225,   257,   258,   262,   263,   265,   270,   272,   273,
     274,   275,   280,   281,   282,   283,   482,   483,    61,   558,
     140,   140,   139,   140,   139,   140,   140,   140,   139,   140,
     139,   140,   403,   405,   139,   139,     6,    52,   148,   170,
     171,   178,   179,   182,   184,   185,   186,   187,   188,   198,
     199,   229,   260,   263,   273,   278,   295,   496,   497,   501,
     505,   507,   508,   509,   510,   512,   513,   403,   403,   141,
     403,   148,   289,   289,   191,   192,   193,    75,   195,   196,
     147,   146,   492,   493,   260,   302,   484,     6,   225,   226,
     227,   228,   229,   258,   260,   280,   515,   543,   260,   280,
     543,     6,   178,   179,   181,   229,   254,   255,   229,     6,
     278,     6,    42,    49,    50,    51,    52,   229,   184,   189,
     198,   200,   201,   202,   203,   204,   205,   206,   207,   208,
     295,   499,   500,   502,   503,   504,   506,   509,   175,   229,
     280,   139,    25,   148,    37,   222,   223,   236,   237,   238,
     239,   240,   241,   242,   243,   244,   245,   246,   247,   248,
     249,   353,   358,   361,   362,   368,   407,   408,   148,    80,
     410,    21,    22,    24,   139,   140,   305,   139,   140,    94,
      94,   302,   140,   148,   140,    29,    30,    26,    27,    28,
      32,    33,    35,    36,    38,    39,   177,   303,   355,    64,
      66,    67,    68,    69,    70,    71,    72,    73,   303,   339,
     287,   287,   303,   486,   213,    95,   350,   353,   358,   361,
     362,   351,   285,   485,   482,   303,   483,   148,    95,   217,
     219,   404,   405,   406,   472,    79,    93,   217,   218,   219,
     279,   293,   423,   425,   295,    97,   229,   296,   297,   298,
     299,   300,   301,   517,   517,   517,   517,   178,   179,   517,
     517,   517,   517,   281,   282,   273,   517,   148,   525,   526,
     527,   279,   423,   425,   517,   497,   527,   197,   511,    95,
     404,   470,    95,   404,   469,    95,   404,   471,    59,   493,
     302,   482,   558,    22,    23,   257,   532,   257,   258,   515,
     532,   517,   300,   301,   478,   517,   259,   478,   259,   517,
     517,   517,   283,   256,   517,   256,   517,   267,   268,   269,
     270,   267,   268,   269,   270,    42,    53,    54,   517,   517,
     517,    50,    51,    42,   517,   517,   517,   500,   527,   517,
     517,   175,   478,   148,   367,    98,   139,   141,   139,   139,
     231,   139,   140,   140,   140,   304,   384,   139,   354,    20,
     390,   377,   304,   304,   304,   304,   304,   304,   304,   304,
     304,   304,   304,   304,   304,   304,   304,   304,   304,   304,
     304,   148,   148,   303,   303,   484,    59,   148,   139,   148,
     139,   148,   406,   472,   139,   140,   139,   148,   139,   148,
     139,   148,   286,   294,    81,   424,   418,   517,   148,   148,
     148,   148,   160,   514,   514,   517,   517,   514,   514,   514,
     514,   527,   527,   527,   148,    59,   148,     5,   286,   424,
     418,   148,   514,     5,   517,   148,   470,   148,   469,   148,
     471,   482,   303,   148,    98,   527,   527,   527,    98,   148,
     139,   140,   148,   139,   140,   139,   140,   139,   140,   148,
     514,   148,   514,   148,   514,   527,   517,   148,   514,   517,
     514,   421,   421,   421,    84,   557,   421,   421,   421,   557,
     527,   527,   527,   148,   148,   148,   517,   517,   527,   514,
     514,   148,   514,     5,   514,   148,   527,   139,    22,    23,
     300,   359,   385,   386,   387,   139,   139,   388,   389,   390,
     303,    20,   217,   219,   257,   378,   140,   140,   140,   139,
     140,    14,    15,    29,    30,   140,   140,   360,   148,   148,
     148,   148,   148,   148,   148,   148,   148,   231,   484,   558,
     231,   527,   527,   527,   527,   527,   527,   415,   139,   140,
     148,   412,    57,    58,    84,    88,    95,    96,   264,   419,
     427,   431,   432,   464,   474,   475,   527,   148,    59,   539,
     539,   148,   514,   148,   514,   149,   539,   149,   539,   149,
     149,     5,     5,     5,   231,   232,   233,   234,   235,   518,
      59,    75,    76,    90,    97,   265,   266,   522,   523,   415,
     412,   527,   527,   149,   523,   514,   231,   231,   231,   303,
     484,   527,    58,   527,     5,     5,     5,   527,   539,   527,
     527,   518,   527,   527,   180,   539,   180,   539,   230,   231,
       5,   148,   514,   527,   149,   514,   149,   539,    16,    60,
      95,   258,   422,   464,   465,   466,   467,   468,   527,   527,
     527,   271,   421,   527,   527,   527,   421,     5,     5,     5,
     149,    43,   539,    43,   539,   148,   148,     5,   149,   539,
     149,   539,   527,   149,   523,   149,   527,     5,   527,   359,
     359,   139,   388,   388,   388,   390,   389,   139,   148,   139,
     148,   148,   361,   539,   539,   539,   539,   230,   231,   230,
     231,   230,   231,   230,   231,   230,   231,   558,     5,     5,
       5,     5,     5,     5,    37,   280,   353,   356,   358,   416,
     474,   475,   477,   527,   527,   527,    16,    82,    83,   353,
     356,   358,   413,   426,   427,   430,   431,   432,   464,   474,
     475,   477,   527,   139,   139,    85,    86,    87,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   148,   148,   517,     5,
     527,     5,   231,   520,   520,   180,   539,   180,   539,   523,
     523,   523,   527,   403,     4,   529,   527,   527,     5,     5,
     527,   529,   484,   558,     5,   139,     5,   523,   523,   523,
       5,   527,     5,     5,   527,     5,     5,   527,   520,   527,
     520,   527,   527,   523,   527,   149,     5,   527,   149,   539,
     527,   527,   139,   148,   148,   148,     5,     5,     5,   527,
       5,     5,     5,   527,   523,   523,   523,   527,   520,   520,
     520,   520,    43,   539,    43,   539,   523,     5,   527,   529,
       5,   523,     5,   139,   139,   523,   523,   523,   523,   523,
     523,    40,   478,     5,     5,     5,   139,     5,   428,   447,
     447,    77,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   407,   408,   458,   459,   435,   435,   439,   439,
      79,   176,   177,   210,   433,   434,   437,   441,   445,   456,
     231,   148,   523,     5,    95,   523,   520,   520,   529,     5,
      62,   404,   521,     3,   522,   524,     5,     5,   523,   523,
       5,   558,   523,   527,   523,   529,   523,     5,   523,   523,
       5,   523,   523,     5,   527,     5,   527,     5,     5,     5,
     527,   523,     5,   527,   527,     5,     5,   231,   523,   523,
     523,     5,   523,   523,   523,     5,   529,     4,   530,   529,
       5,   527,   527,   527,   527,   148,   232,   233,   234,   519,
     148,   519,   148,   519,   148,   519,   529,   523,     5,   523,
     523,   529,   529,   529,   529,   529,   529,   296,   357,   139,
     523,   523,   523,   523,   361,   362,   429,    98,   172,   175,
     210,   216,   306,   407,   408,   448,   449,   450,   451,   452,
     453,   140,   517,   517,   517,   517,   517,   517,   517,   517,
     517,   517,   459,   407,   408,   436,    78,   407,   408,   440,
     139,   139,   148,   140,   434,   407,   408,   438,   224,   407,
     408,   409,   442,   288,   291,   443,   444,   446,   215,   455,
     457,   230,   231,   529,   523,   523,    99,   521,   276,   277,
     278,   523,   523,   530,   529,   523,   529,     5,   529,   523,
     523,   523,     5,   523,     5,   523,   523,   523,     5,   529,
     523,     5,     5,   523,   523,   529,   529,   529,   523,   530,
     530,   530,   523,     3,   524,   523,     5,     5,     5,     5,
     519,   527,   519,   527,   519,   527,   519,   527,   529,   523,
     529,   148,   529,   529,   529,   529,   139,   173,   174,   517,
     139,   140,   139,   454,   148,   148,   148,   148,   148,   148,
     148,   148,   148,   148,    98,   139,   139,   141,   148,   139,
     529,   529,    59,   148,   525,   526,   528,   528,   528,   530,
     530,   529,   523,   529,   529,   529,   523,   529,   523,   529,
     529,   529,   523,   529,   523,   523,   529,   529,   529,   530,
       6,   529,   523,   523,   523,   523,   527,     5,   527,     5,
     527,     5,   527,     5,   529,   234,   148,   290,   307,   149,
     149,   149,   149,   149,   149,   149,   149,   149,   149,    22,
      23,   359,    59,     5,     5,     5,   529,   529,   529,   529,
     529,   529,   528,   529,   529,   529,   529,     5,   523,     5,
     523,     5,   523,     5,   523,   359,   359,   139,   524,   524,
     524,     5,   523,   529,   523,   529,   523,   529,   523,   529,
     139,   139,   524,   529,   529,   529,   529
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int16 yyr1[] =
{
       0,   308,   309,   309,   310,   310,   311,   311,   311,   311,
     311,   311,   311,   311,   311,   311,   311,   311,   311,   311,
     311,   311,   311,   311,   311,   311,   311,   311,   311,   311,
     311,   311,   311,   312,   312,   313,   313,   313,   313,   313,
     313,   313,   313,   313,   313,   313,   313,   313,   313,   313,
     313,   313,   313,   313,   313,   313,   313,   313,   313,   314,
     314,   315,   315,   315,   315,   315,   315,   315,   315,   315,
     315,   315,   315,   315,   315,   315,   315,   315,   315,   316,
     316,   317,   317,   317,   317,   317,   317,   317,   317,   317,
     317,   317,   317,   317,   317,   317,   317,   317,   317,   317,
     317,   318,   318,   319,   319,   319,   319,   319,   319,   319,
     319,   319,   319,   319,   319,   319,   319,   319,   320,   320,
     321,   321,   321,   321,   321,   321,   321,   321,   321,   321,
     321,   321,   321,   322,   322,   323,   323,   323,   323,   323,
     323,   323,   323,   323,   323,   323,   323,   323,   323,   323,
     324,   324,   325,   325,   325,   325,   325,   325,   325,   325,
     325,   325,   325,   325,   325,   326,   326,   327,   327,   327,
     327,   327,   327,   327,   327,   327,   327,   327,   327,   327,
     327,   327,   328,   328,   329,   329,   329,   329,   329,   329,
     329,   329,   329,   329,   329,   329,   330,   330,   330,   331,
     332,   333,   333,   334,   335,   335,   335,   336,   337,   338,
     338,   339,   339,   339,   339,   339,   339,   339,   339,   339,
     339,   339,   339,   339,   339,   340,   341,   341,   341,   342,
     342,   342,   343,   344,   345,   346,   347,   347,   348,   349,
     349,   350,   350,   350,   350,   350,   351,   351,   352,   353,
     353,   354,   354,   355,   355,   355,   355,   355,   355,   355,
     355,   355,   355,   355,   355,   356,   357,   357,   358,   358,
     358,   359,   359,   360,   360,   361,   361,   361,   361,   361,
     361,   361,   361,   361,   361,   361,   361,   361,   362,   363,
     364,   365,   365,   366,   366,   367,   367,   368,   368,   368,
     368,   368,   368,   369,   370,   370,   371,   371,   371,   371,
     371,   371,   371,   371,   371,   372,   373,   374,   375,   376,
     377,   377,   378,   378,   378,   378,   378,   378,   379,   379,
     380,   380,   381,   381,   382,   383,   383,   383,   383,   383,
     383,   384,   383,   385,   383,   386,   383,   387,   383,   383,
     388,   388,   389,   390,   390,   391,   391,   391,   391,   392,
     393,   393,   394,   395,   396,   396,   397,   398,   399,   399,
     400,   400,   401,   401,   402,   402,   403,   403,   404,   404,
     405,   405,   406,   406,   406,   406,   407,   407,   408,   409,
     410,   410,   411,   411,   412,   412,   413,   413,   413,   413,
     413,   413,   413,   413,   413,   413,   413,   413,   414,   414,
     415,   415,   416,   416,   416,   416,   416,   416,   417,   417,
     418,   418,   419,   419,   419,   419,   419,   419,   420,   420,
     420,   420,   420,   420,   420,   420,   421,   421,   422,   422,
     422,   422,   422,   423,   423,   424,   425,   426,   426,   427,
     427,   427,   428,   428,   429,   429,   430,   431,   431,   431,
     431,   431,   431,   431,   431,   431,   431,   431,   431,   431,
     431,   431,   431,   431,   431,   431,   431,   431,   431,   431,
     431,   431,   431,   431,   431,   431,   431,   431,   431,   431,
     431,   431,   431,   431,   431,   432,   432,   433,   433,   434,
     434,   434,   434,   435,   435,   436,   436,   437,   437,   438,
     438,   439,   439,   440,   440,   440,   440,   440,   441,   441,
     442,   442,   442,   443,   443,   444,   445,   445,   446,   446,
     447,   447,   448,   448,   448,   448,   448,   448,   448,   448,
     449,   450,   450,   451,   451,   452,   453,   454,   454,   455,
     456,   456,   457,   458,   458,   459,   459,   459,   459,   459,
     459,   459,   459,   459,   459,   459,   459,   459,   460,   460,
     461,   462,   463,   464,   464,   465,   466,   467,   468,   469,
     469,   470,   470,   471,   471,   472,   472,   473,   473,   474,
     475,   476,   476,   477,   478,   478,   479,   479,   479,   479,
     480,   481,   482,   482,   483,   483,   483,   483,   483,   483,
     483,   483,   483,   483,   483,   483,   483,   483,   483,   483,
     483,   483,   483,   483,   483,   483,   483,   483,   483,   483,
     483,   483,   483,   483,   484,   484,   485,   485,   486,   486,
     486,   486,   487,   487,   487,   488,   488,   488,   489,   489,
     489,   490,   491,   492,   492,   493,   494,   494,   495,   496,
     496,   497,   497,   497,   497,   497,   497,   497,   498,   499,
     499,   500,   500,   500,   500,   501,   501,   502,   503,   503,
     503,   503,   503,   503,   503,   503,   503,   503,   504,   504,
     505,   505,   505,   505,   506,   506,   507,   508,   509,   510,
     510,   510,   511,   511,   512,   512,   512,   513,   513,   513,
     514,   514,   515,   515,   515,   515,   516,   516,   517,   517,
     517,   517,   517,   517,   517,   517,   518,   518,   518,   518,
     518,   518,   519,   519,   519,   520,   520,   521,   521,   521,
     522,   522,   522,   522,   522,   522,   522,   523,   524,   525,
     526,   527,   527,   527,   528,   528,   528,   529,   529,   529,
     529,   529,   530,   530,   530,   531,   531,   531,   532,   532,
     532,   533,   533,   533,   533,   534,   534,   534,   534,   535,
     535,   536,   536,   537,   537,   538,   539,   539,   539,   539,
     539,   540,   540,   541,   542,   542,   542,   542,   542,   542,
     542,   542,   543,   543,   544,   544,   545,   545,   546,   546,
     547,   547,   548,   548,   549,   549,   550,   551,   551,   552,
     552,   552,   553,   554,   555,   555,   555,   555,   555,   555,
     556,   556,   556,   556,   556,   556,   557,   558,   558,   558
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     1,     1,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     0,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     0,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     0,     2,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     0,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       0,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     0,     2,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     0,     2,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     4,     7,     8,     4,
       3,     0,     3,     2,     3,     3,     3,     4,     5,     0,
       2,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     2,     3,     3,     5,     4,
       6,     4,     3,     3,     3,     3,     2,     3,     2,     0,
       2,     3,     1,     1,     1,     1,     0,     2,     5,     1,
       4,     0,     2,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     5,     0,     1,     4,     5,
       5,     0,     1,     0,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     2,     5,
       5,     1,     2,     2,     4,     0,     2,     1,     1,     1,
       1,     1,     1,     3,     0,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     2,     2,     1,     3,     3,
       0,     2,     2,     2,     2,     2,     2,     1,     2,     2,
       2,     2,     1,     1,     2,     5,     4,     2,     3,     3,
       3,     0,     4,     0,     5,     0,     5,     0,     5,     2,
       1,     2,     1,     0,     1,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     2,     4,     5,     6,
       3,     4,     3,     4,     3,     4,     1,     2,     1,     2,
       1,     1,     2,     2,     2,     2,     2,     2,     2,     2,
       0,     2,     9,     9,     0,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     9,     9,
       0,     2,     1,     1,     1,     1,     1,     1,     8,     8,
       0,     2,     1,     1,     1,     1,     1,     1,     9,     8,
       8,     8,     9,     8,     8,     8,     0,     2,     1,     1,
       1,     1,     1,     0,     2,     2,     2,     1,     1,     2,
       3,     2,     0,     2,     1,     1,     2,     3,     2,     2,
       2,     2,     2,     2,     2,     3,     3,     2,     2,     2,
       2,     2,     2,     2,     3,     3,     3,     2,     2,     3,
       2,     2,     2,     3,     2,     2,     2,     2,     3,     3,
       3,     2,     2,     2,     3,     2,     2,     1,     2,     2,
       2,     2,     2,     0,     2,     1,     1,     0,     2,     1,
       1,     0,     2,     1,     1,     4,     5,     5,     0,     2,
       1,     1,     1,     2,     2,     2,     0,     2,     1,     1,
       0,     2,     1,     1,     1,     1,     1,     1,     1,     3,
       3,     2,     2,     2,     2,     2,     2,     0,     2,     2,
       0,     2,     1,     1,     2,     1,     1,     2,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     7,     6,
       6,     6,     9,     4,     4,     2,     2,     3,     2,     0,
       3,     0,     3,     0,     3,     0,     3,     0,     3,     3,
       2,     7,     7,     3,     1,     1,     3,     6,     7,     1,
       2,     2,     1,     2,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     0,     4,     1,     2,     2,     2,
       1,     1,     3,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     1,     2,     1,     8,     6,     6,     1,
       2,     1,     1,     1,     1,     1,     1,     1,     6,     1,
       2,     1,     1,     1,     1,     4,     4,     4,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     4,     4,
       4,     4,     4,     4,     4,     4,     3,     3,     4,     1,
       1,     1,     0,     1,     5,     6,     5,     5,     6,     5,
       1,     1,     1,     1,     1,     1,     9,     6,     0,     1,
       1,     1,     1,     1,     1,     1,     0,     1,     1,     1,
       1,     1,     1,     1,     1,     0,     1,     0,     3,     4,
       1,     3,     4,     1,     1,     1,     1,     1,     1,     2,
       3,     0,     1,     1,     0,     1,     1,     0,     2,     6,
       6,     6,     0,     2,     6,     8,    10,     7,     0,     1,
       1,     8,     9,     9,    10,     9,     9,    10,    10,    10,
       9,    10,     9,     9,     9,     6,     0,     1,     1,     1,
       1,     8,     7,     7,     7,     7,     4,     4,     7,     7,
       4,     4,     0,     1,     9,     6,     8,     8,     8,     8,
       9,     9,     8,     9,     8,     9,     9,     8,     8,     7,
       7,     7,     7,     9,    10,    11,    12,    10,    11,    12,
      10,    11,    12,    10,    11,    12,     2,     0,     2,     3
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 196: /* setalert: SET alertmail formatlist reminder  */
#line 615 "src/p.y"
                                                    {
                        mailset.events = Event_All;
                        addmail((yyvsp[-2].string), &mailset, &Run.maillist);
                  }
#line 3212 "src/y.tab.c"
    break;

  case 197: /* setalert: SET alertmail '{' eventoptionlist '}' formatlist reminder  */
#line 619 "src/p.y"
                                                                            {
                        addmail((yyvsp[-5].string), &mailset, &Run.maillist);
                  }
#line 3220 "src/y.tab.c"
    break;

  case 198: /* setalert: SET alertmail NOT '{' eventoptionlist '}' formatlist reminder  */
#line 622 "src/p.y"
                                                                                {
                        mailset.events = ~mailset.events;
                        addmail((yyvsp[-6].string), &mailset, &Run.maillist);
                  }
#line 3229 "src/y.tab.c"
    break;

  case 199: /* setdaemon: SET DAEMON NUMBER startdelay  */
#line 628 "src/p.y"
                                               {
                        if (! (Run.flags & Run_Daemon) || ihp.daemon) {
                                ihp.daemon     = true;
                                Run.flags      |= Run_Daemon;
                                Run.polltime   = (yyvsp[-1].number);
                                Run.startdelay = (yyvsp[0].number);
                        }
                  }
#line 3242 "src/y.tab.c"
    break;

  case 200: /* setterminal: SET TERMINAL BATCH  */
#line 638 "src/p.y"
                                     {
                        Run.flags |= Run_Batch;
                  }
#line 3250 "src/y.tab.c"
    break;

  case 201: /* startdelay: %empty  */
#line 643 "src/p.y"
                              {
                        (yyval.number) = 0;
                  }
#line 3258 "src/y.tab.c"
    break;

  case 202: /* startdelay: START DELAY NUMBER  */
#line 646 "src/p.y"
                                     {
                        (yyval.number) = (yyvsp[0].number);
                  }
#line 3266 "src/y.tab.c"
    break;

  case 203: /* setinit: SET INIT  */
#line 651 "src/p.y"
                           {
                        Run.flags |= Run_Foreground;
                  }
#line 3274 "src/y.tab.c"
    break;

  case 204: /* setonreboot: SET ONREBOOT START  */
#line 656 "src/p.y"
                                     {
                        Run.onreboot = Onreboot_Start;
                  }
#line 3282 "src/y.tab.c"
    break;

  case 205: /* setonreboot: SET ONREBOOT NOSTART  */
#line 659 "src/p.y"
                                       {
                        Run.onreboot = Onreboot_Nostart;
                  }
#line 3290 "src/y.tab.c"
    break;

  case 206: /* setonreboot: SET ONREBOOT LASTSTATE  */
#line 662 "src/p.y"
                                         {
                        Run.onreboot = Onreboot_Laststate;
                  }
#line 3298 "src/y.tab.c"
    break;

  case 207: /* setexpectbuffer: SET EXPECTBUFFER NUMBER unit  */
#line 667 "src/p.y"
                                               {
                        // Note: deprecated (replaced by "set limits" statement's "sendExpectBuffer" option)
                        Run.limits.sendExpectBuffer = (yyvsp[-1].number) * (yyvsp[0].number);
                  }
#line 3307 "src/y.tab.c"
    break;

  case 211: /* limit: SENDEXPECTBUFFER ':' NUMBER unit  */
#line 680 "src/p.y"
                                                   {
                        Run.limits.sendExpectBuffer = (yyvsp[-1].number) * (yyvsp[0].number);
                  }
#line 3315 "src/y.tab.c"
    break;

  case 212: /* limit: FILECONTENTBUFFER ':' NUMBER unit  */
#line 683 "src/p.y"
                                                    {
                        Run.limits.fileContentBuffer = (yyvsp[-1].number) * (yyvsp[0].number);
                  }
#line 3323 "src/y.tab.c"
    break;

  case 213: /* limit: HTTPCONTENTBUFFER ':' NUMBER unit  */
#line 686 "src/p.y"
                                                    {
                        Run.limits.httpContentBuffer = (yyvsp[-1].number) * (yyvsp[0].number);
                  }
#line 3331 "src/y.tab.c"
    break;

  case 214: /* limit: PROGRAMOUTPUT ':' NUMBER unit  */
#line 689 "src/p.y"
                                                {
                        Run.limits.programOutput = (yyvsp[-1].number) * (yyvsp[0].number);
                  }
#line 3339 "src/y.tab.c"
    break;

  case 215: /* limit: NETWORKTIMEOUT ':' NUMBER MILLISECOND  */
#line 692 "src/p.y"
                                                        {
                        Run.limits.networkTimeout = (yyvsp[-1].number);
                  }
#line 3347 "src/y.tab.c"
    break;

  case 216: /* limit: NETWORKTIMEOUT ':' NUMBER SECOND  */
#line 695 "src/p.y"
                                                   {
                        Run.limits.networkTimeout = (yyvsp[-1].number) * 1000;
                  }
#line 3355 "src/y.tab.c"
    break;

  case 217: /* limit: PROGRAMTIMEOUT ':' NUMBER MILLISECOND  */
#line 698 "src/p.y"
                                                        {
                        Run.limits.programTimeout = (yyvsp[-1].number);
                  }
#line 3363 "src/y.tab.c"
    break;

  case 218: /* limit: PROGRAMTIMEOUT ':' NUMBER SECOND  */
#line 701 "src/p.y"
                                                   {
                        Run.limits.programTimeout = (yyvsp[-1].number) * 1000;
                  }
#line 3371 "src/y.tab.c"
    break;

  case 219: /* limit: STOPTIMEOUT ':' NUMBER MILLISECOND  */
#line 704 "src/p.y"
                                                     {
                        Run.limits.stopTimeout = (yyvsp[-1].number);
                  }
#line 3379 "src/y.tab.c"
    break;

  case 220: /* limit: STOPTIMEOUT ':' NUMBER SECOND  */
#line 707 "src/p.y"
                                                {
                        Run.limits.stopTimeout = (yyvsp[-1].number) * 1000;
                  }
#line 3387 "src/y.tab.c"
    break;

  case 221: /* limit: STARTTIMEOUT ':' NUMBER MILLISECOND  */
#line 710 "src/p.y"
                                                      {
                        Run.limits.startTimeout = (yyvsp[-1].number);
                  }
#line 3395 "src/y.tab.c"
    break;

  case 222: /* limit: STARTTIMEOUT ':' NUMBER SECOND  */
#line 713 "src/p.y"
                                                 {
                        Run.limits.startTimeout = (yyvsp[-1].number) * 1000;
                  }
#line 3403 "src/y.tab.c"
    break;

  case 223: /* limit: RESTARTTIMEOUT ':' NUMBER MILLISECOND  */
#line 716 "src/p.y"
                                                        {
                        Run.limits.restartTimeout = (yyvsp[-1].number);
                  }
#line 3411 "src/y.tab.c"
    break;

  case 224: /* limit: RESTARTTIMEOUT ':' NUMBER SECOND  */
#line 719 "src/p.y"
                                                   {
                        Run.limits.restartTimeout = (yyvsp[-1].number) * 1000;
                  }
#line 3419 "src/y.tab.c"
    break;

  case 225: /* setfips: SET FIPS  */
#line 724 "src/p.y"
                           {
                        Run.flags |= Run_FipsEnabled;
                  }
#line 3427 "src/y.tab.c"
    break;

  case 226: /* setlog: SET LOGFILE PATH  */
#line 729 "src/p.y"
                                     {
                        if (! Run.files.log || ihp.logfile) {
                                ihp.logfile = true;
                                setlogfile((yyvsp[0].string));
                                Run.flags &= ~Run_UseSyslog;
                                Run.flags |= Run_Log;
                        }
                  }
#line 3440 "src/y.tab.c"
    break;

  case 227: /* setlog: SET LOGFILE SYSLOG  */
#line 737 "src/p.y"
                                     {
                        setsyslog(NULL);
                  }
#line 3448 "src/y.tab.c"
    break;

  case 228: /* setlog: SET LOGFILE SYSLOG FACILITY STRING  */
#line 740 "src/p.y"
                                                     {
                        setsyslog((yyvsp[0].string)); FREE((yyvsp[0].string));
                  }
#line 3456 "src/y.tab.c"
    break;

  case 229: /* seteventqueue: SET EVENTQUEUE BASEDIR PATH  */
#line 745 "src/p.y"
                                              {
                        Run.eventlist_dir = (yyvsp[0].string);
                  }
#line 3464 "src/y.tab.c"
    break;

  case 230: /* seteventqueue: SET EVENTQUEUE BASEDIR PATH SLOT NUMBER  */
#line 748 "src/p.y"
                                                          {
                        Run.eventlist_dir = (yyvsp[-2].string);
                        Run.eventlist_slots = (yyvsp[0].number);
                  }
#line 3473 "src/y.tab.c"
    break;

  case 231: /* seteventqueue: SET EVENTQUEUE SLOT NUMBER  */
#line 752 "src/p.y"
                                             {
                        Run.eventlist_dir = Str_dup(MYEVENTLISTBASE);
                        Run.eventlist_slots = (yyvsp[0].number);
                  }
#line 3482 "src/y.tab.c"
    break;

  case 232: /* setidfile: SET IDFILE PATH  */
#line 758 "src/p.y"
                                  {
                        Run.files.id = (yyvsp[0].string);
                  }
#line 3490 "src/y.tab.c"
    break;

  case 233: /* setstatefile: SET STATEFILE PATH  */
#line 763 "src/p.y"
                                     {
                        Run.files.state = (yyvsp[0].string);
                  }
#line 3498 "src/y.tab.c"
    break;

  case 234: /* setpid: SET PIDFILE PATH  */
#line 768 "src/p.y"
                                   {
                        if (! Run.files.pid || ihp.pidfile) {
                                ihp.pidfile = true;
                                setpidfile((yyvsp[0].string));
                        }
                  }
#line 3509 "src/y.tab.c"
    break;

  case 238: /* mmonit: URLOBJECT mmonitoptlist  */
#line 783 "src/p.y"
                                          {
                        mmonitset.url = (yyvsp[-1].url);
                        addmmonit(&mmonitset);
                  }
#line 3518 "src/y.tab.c"
    break;

  case 241: /* mmonitopt: TIMEOUT NUMBER SECOND  */
#line 793 "src/p.y"
                                        {
                        mmonitset.timeout = (yyvsp[-1].number) * 1000; // net timeout is in milliseconds internally
                  }
#line 3526 "src/y.tab.c"
    break;

  case 247: /* credentials: REGISTER CREDENTIALS  */
#line 803 "src/p.y"
                                       {
                        Run.flags &= ~Run_MmonitCredentials;
                  }
#line 3534 "src/y.tab.c"
    break;

  case 248: /* setssl: SET SSLTOKEN '{' ssloptionlist '}'  */
#line 808 "src/p.y"
                                                     {
                        _setSSLOptions(&(Run.ssl));
                  }
#line 3542 "src/y.tab.c"
    break;

  case 249: /* ssl: SSLTOKEN  */
#line 813 "src/p.y"
                           {
                        sslset.flags = SSL_Enabled;
                  }
#line 3550 "src/y.tab.c"
    break;

  case 253: /* ssloption: VERIFY ':' ENABLE  */
#line 823 "src/p.y"
                                    {
                        sslset.flags = SSL_Enabled;
                        sslset.verify = true;
                  }
#line 3559 "src/y.tab.c"
    break;

  case 254: /* ssloption: VERIFY ':' DISABLE  */
#line 827 "src/p.y"
                                     {
                        sslset.flags = SSL_Enabled;
                        sslset.verify = false;
                  }
#line 3568 "src/y.tab.c"
    break;

  case 255: /* ssloption: SELFSIGNED ':' ALLOW  */
#line 831 "src/p.y"
                                       {
                        sslset.flags = SSL_Enabled;
                        sslset.allowSelfSigned = true;
                  }
#line 3577 "src/y.tab.c"
    break;

  case 256: /* ssloption: SELFSIGNED ':' REJECTOPT  */
#line 835 "src/p.y"
                                           {
                        sslset.flags = SSL_Enabled;
                        sslset.allowSelfSigned = false;
                  }
#line 3586 "src/y.tab.c"
    break;

  case 257: /* ssloption: VERSIONOPT ':' sslversionlist  */
#line 839 "src/p.y"
                                                {
                        sslset.flags = SSL_Enabled;
                  }
#line 3594 "src/y.tab.c"
    break;

  case 258: /* ssloption: CIPHER ':' STRING  */
#line 842 "src/p.y"
                                    {
                        FREE(sslset.ciphers);
                        sslset.ciphers = (yyvsp[0].string);
                  }
#line 3603 "src/y.tab.c"
    break;

  case 259: /* ssloption: PEMFILE ':' PATH  */
#line 846 "src/p.y"
                                   {
                        _setPEM(&(sslset.pemfile), (yyvsp[0].string), "SSL server PEM file", true);
                  }
#line 3611 "src/y.tab.c"
    break;

  case 260: /* ssloption: PEMCHAIN ':' PATH  */
#line 849 "src/p.y"
                                    {
                        _setPEM(&(sslset.pemchain), (yyvsp[0].string), "SSL certificate chain PEM file", true);
                  }
#line 3619 "src/y.tab.c"
    break;

  case 261: /* ssloption: PEMKEY ':' PATH  */
#line 852 "src/p.y"
                                  {
                        _setPEM(&(sslset.pemkey), (yyvsp[0].string), "SSL server private key PEM file", true);
                  }
#line 3627 "src/y.tab.c"
    break;

  case 262: /* ssloption: CLIENTPEMFILE ':' PATH  */
#line 855 "src/p.y"
                                         {
                        _setPEM(&(sslset.clientpemfile), (yyvsp[0].string), "SSL client PEM file", true);
                  }
#line 3635 "src/y.tab.c"
    break;

  case 263: /* ssloption: CACERTIFICATEFILE ':' PATH  */
#line 858 "src/p.y"
                                             {
                        _setPEM(&(sslset.CACertificateFile), (yyvsp[0].string), "SSL CA certificates file", true);
                  }
#line 3643 "src/y.tab.c"
    break;

  case 264: /* ssloption: CACERTIFICATEPATH ':' PATH  */
#line 861 "src/p.y"
                                             {
                        _setPEM(&(sslset.CACertificatePath), (yyvsp[0].string), "SSL CA certificates directory", false);
                  }
#line 3651 "src/y.tab.c"
    break;

  case 265: /* sslexpire: CERTIFICATE VALID expireoperator NUMBER DAY  */
#line 866 "src/p.y"
                                                              {
                        sslset.flags = SSL_Enabled;
                        portset.target.net.ssl.certificate.minimumDays = (yyvsp[-1].number);
                  }
#line 3660 "src/y.tab.c"
    break;

  case 268: /* sslchecksum: CERTIFICATE CHECKSUM checksumoperator STRING  */
#line 876 "src/p.y"
                                                               {
                        sslset.flags = SSL_Enabled;
                        sslset.checksum = (yyvsp[0].string);
                        switch (cleanup_hash_string(sslset.checksum)) {
                                case 32:
                                        sslset.checksumType = Hash_Md5;
                                        break;
                                case 40:
                                        sslset.checksumType = Hash_Sha1;
                                        break;
                                default:
                                        yyerror2("Unknown checksum type: [%s] is not MD5 nor SHA1", sslset.checksum);
                        }
                  }
#line 3679 "src/y.tab.c"
    break;

  case 269: /* sslchecksum: CERTIFICATE CHECKSUM MD5HASH checksumoperator STRING  */
#line 890 "src/p.y"
                                                                       {
                        sslset.flags = SSL_Enabled;
                        sslset.checksum = (yyvsp[0].string);
                        if (cleanup_hash_string(sslset.checksum) != 32)
                                yyerror2("Unknown checksum type: [%s] is not MD5", sslset.checksum);
                        sslset.checksumType = Hash_Md5;
                  }
#line 3691 "src/y.tab.c"
    break;

  case 270: /* sslchecksum: CERTIFICATE CHECKSUM SHA1HASH checksumoperator STRING  */
#line 897 "src/p.y"
                                                                        {
                        sslset.flags = SSL_Enabled;
                        sslset.checksum = (yyvsp[0].string);
                        if (cleanup_hash_string(sslset.checksum) != 40)
                                yyerror2("Unknown checksum type: [%s] is not SHA1", sslset.checksum);
                        sslset.checksumType = Hash_Sha1;
                  }
#line 3703 "src/y.tab.c"
    break;

  case 275: /* sslversion: SSLV2  */
#line 914 "src/p.y"
                        {
#if defined OPENSSL_NO_SSL2 || ! defined HAVE_SSLV2 || ! defined HAVE_OPENSSL
                        yyerror("Your SSL Library does not support SSL version 2");
#else
                        _setSSLVersion(SSL_V2);
#endif
                  }
#line 3715 "src/y.tab.c"
    break;

  case 276: /* sslversion: NOSSLV2  */
#line 921 "src/p.y"
                          {
                        _unsetSSLVersion(SSL_V2);
                  }
#line 3723 "src/y.tab.c"
    break;

  case 277: /* sslversion: SSLV3  */
#line 924 "src/p.y"
                        {
#if defined OPENSSL_NO_SSL3 || ! defined HAVE_OPENSSL
                        yyerror("Your SSL Library does not support SSL version 3");
#else
                        _setSSLVersion(SSL_V3);
#endif
                  }
#line 3735 "src/y.tab.c"
    break;

  case 278: /* sslversion: NOSSLV3  */
#line 931 "src/p.y"
                          {
                        _unsetSSLVersion(SSL_V3);
                  }
#line 3743 "src/y.tab.c"
    break;

  case 279: /* sslversion: TLSV1  */
#line 934 "src/p.y"
                        {
#if defined OPENSSL_NO_TLS1_METHOD || ! defined HAVE_OPENSSL
                        yyerror("Your SSL Library does not support TLS version 1.0");
#else
                        _setSSLVersion(SSL_TLSV1);
#endif
                  }
#line 3755 "src/y.tab.c"
    break;

  case 280: /* sslversion: NOTLSV1  */
#line 941 "src/p.y"
                          {
                        _unsetSSLVersion(SSL_TLSV1);
                  }
#line 3763 "src/y.tab.c"
    break;

  case 281: /* sslversion: TLSV11  */
#line 944 "src/p.y"
                         {
#if defined OPENSSL_NO_TLS1_1_METHOD || ! defined HAVE_TLSV1_1 || ! defined HAVE_OPENSSL
                        yyerror("Your SSL Library does not support TLS version 1.1");
#else
                        _setSSLVersion(SSL_TLSV11);
#endif
                }
#line 3775 "src/y.tab.c"
    break;

  case 282: /* sslversion: NOTLSV11  */
#line 951 "src/p.y"
                           {
                        _unsetSSLVersion(SSL_TLSV11);
                  }
#line 3783 "src/y.tab.c"
    break;

  case 283: /* sslversion: TLSV12  */
#line 954 "src/p.y"
                         {
#if defined OPENSSL_NO_TLS1_2_METHOD || ! defined HAVE_TLSV1_2 || ! defined HAVE_OPENSSL
                        yyerror("Your SSL Library does not support TLS version 1.2");
#else
                        _setSSLVersion(SSL_TLSV12);
#endif
                }
#line 3795 "src/y.tab.c"
    break;

  case 284: /* sslversion: NOTLSV12  */
#line 961 "src/p.y"
                           {
                        _unsetSSLVersion(SSL_TLSV12);
                  }
#line 3803 "src/y.tab.c"
    break;

  case 285: /* sslversion: TLSV13  */
#line 964 "src/p.y"
                         {
#if defined OPENSSL_NO_TLS1_3_METHOD || ! defined HAVE_TLSV1_3 || ! defined HAVE_OPENSSL
                        yyerror("Your SSL Library does not support TLS version 1.3");
#else
                        _setSSLVersion(SSL_TLSV13);
#endif
                }
#line 3815 "src/y.tab.c"
    break;

  case 286: /* sslversion: NOTLSV13  */
#line 971 "src/p.y"
                           {
                        _unsetSSLVersion(SSL_TLSV13);
                  }
#line 3823 "src/y.tab.c"
    break;

  case 287: /* sslversion: AUTO  */
#line 974 "src/p.y"
                       {
                        // Enable just TLS 1.2 and 1.3 by default
#if ! defined OPENSSL_NO_TLS1_2_METHOD && defined HAVE_TLSV1_2 && defined HAVE_OPENSSL
                        _setSSLVersion(SSL_TLSV12);
#endif
#if ! defined OPENSSL_NO_TLS1_3_METHOD && defined HAVE_TLSV1_3 && defined HAVE_OPENSSL
                        _setSSLVersion(SSL_TLSV13);
#endif
                  }
#line 3837 "src/y.tab.c"
    break;

  case 288: /* certmd5: CERTMD5 STRING  */
#line 985 "src/p.y"
                                 { // Backward compatibility
                        sslset.flags = SSL_Enabled;
                        sslset.checksum = (yyvsp[0].string);
                        if (cleanup_hash_string(sslset.checksum) != 32)
                                yyerror2("Unknown checksum type: [%s] is not MD5", sslset.checksum);
                        sslset.checksumType = Hash_Md5;
                  }
#line 3849 "src/y.tab.c"
    break;

  case 289: /* setmailservers: SET MAILSERVER mailserverlist nettimeout hostname  */
#line 994 "src/p.y"
                                                                    {
                        if (((yyvsp[-1].number)) > SMTP_TIMEOUT)
                                Run.mailserver_timeout = (yyvsp[-1].number);
                        Run.mail_hostname = (yyvsp[0].string);
                  }
#line 3859 "src/y.tab.c"
    break;

  case 290: /* setmailformat: SET MAILFORMAT '{' formatoptionlist '}'  */
#line 1001 "src/p.y"
                                                          {
                        if (mailset.from) {
                                Run.MailFormat.from = mailset.from;
                        } else {
                                Run.MailFormat.from = Address_new();
                                Run.MailFormat.from->address = Str_dup(ALERT_FROM);
                        }
                        if (mailset.replyto)
                                Run.MailFormat.replyto = mailset.replyto;
                        Run.MailFormat.subject = mailset.subject ?  mailset.subject : Str_dup(ALERT_SUBJECT);
                        Run.MailFormat.message = mailset.message ?  mailset.message : Str_dup(ALERT_MESSAGE);
                        reset_mailset();
                  }
#line 3877 "src/y.tab.c"
    break;

  case 293: /* mailserver: STRING mailserveroptlist  */
#line 1020 "src/p.y"
                                           {
                        /* Restore the current text overridden by lookahead */
                        FREE(argyytext);
                        argyytext = Str_dup((yyvsp[-1].string));

                        mailserverset.host = (yyvsp[-1].string);
                        mailserverset.port = PORT_SMTP;
                        addmailserver(&mailserverset);
                  }
#line 3891 "src/y.tab.c"
    break;

  case 294: /* mailserver: STRING PORT NUMBER mailserveroptlist  */
#line 1029 "src/p.y"
                                                       {
                        /* Restore the current text overridden by lookahead */
                        FREE(argyytext);
                        argyytext = Str_dup((yyvsp[-3].string));

                        mailserverset.host = (yyvsp[-3].string);
                        mailserverset.port = (yyvsp[-1].number);
                        addmailserver(&mailserverset);
                  }
#line 3905 "src/y.tab.c"
    break;

  case 297: /* mailserveropt: username  */
#line 1044 "src/p.y"
                           {
                        mailserverset.username = (yyvsp[0].string);
                  }
#line 3913 "src/y.tab.c"
    break;

  case 298: /* mailserveropt: password  */
#line 1047 "src/p.y"
                           {
                        mailserverset.password = (yyvsp[0].string);
                  }
#line 3921 "src/y.tab.c"
    break;

  case 303: /* sethttpd: SET HTTPD httpdlist  */
#line 1056 "src/p.y"
                                      {
                        if (sslset.flags & SSL_Enabled) {
#ifdef HAVE_OPENSSL
                                if (sslset.pemfile) {
                                        if (sslset.pemchain || sslset.pemkey) {
                                                yyerror("SSL server option pemfile and pemchain|pemkey are mutually exclusive");
                                        } else if (! file_checkStat(sslset.pemfile, "SSL server PEM file", S_IRWXU | S_IRGRP | S_IXGRP)) {
                                                yyerror("SSL server PEM file permissions check failed");
                                        } else {
                                                _setSSLOptions(&(Run.httpd.socket.net.ssl));
                                        }
                                } else if (sslset.pemchain && sslset.pemkey) {
                                        if (! file_checkStat(sslset.pemkey, "SSL server private key PEM file", S_IRWXU | S_IRGRP | S_IXGRP)) {
                                                yyerror("SSL server private key PEM file permissions check failed");
                                        } else {
                                                _setSSLOptions(&(Run.httpd.socket.net.ssl));
                                        }
                                } else {
                                        yyerror("SSL server PEM file is required (please use ssl pemfile option)");
                                }
#else
                                yyerror("SSL is not supported");
#endif
                        }
                  }
#line 3951 "src/y.tab.c"
    break;

  case 315: /* pemfile: PEMFILE PATH  */
#line 1099 "src/p.y"
                               {
                        _setPEM(&(sslset.pemfile), (yyvsp[0].string), "SSL server PEM file", true);
                  }
#line 3959 "src/y.tab.c"
    break;

  case 316: /* clientpemfile: CLIENTPEMFILE PATH  */
#line 1105 "src/p.y"
                                     {
                        _setPEM(&(sslset.clientpemfile), (yyvsp[0].string), "SSL client PEM file", true);
                  }
#line 3967 "src/y.tab.c"
    break;

  case 317: /* allowselfcert: ALLOWSELFCERTIFICATION  */
#line 1111 "src/p.y"
                                         {
                        sslset.flags = SSL_Enabled;
                        sslset.allowSelfSigned = true;
                  }
#line 3976 "src/y.tab.c"
    break;

  case 318: /* httpdport: PORT NUMBER readonly  */
#line 1117 "src/p.y"
                                       {
                        Run.httpd.flags |= Httpd_Net;
                        Run.httpd.socket.net.port = (yyvsp[-1].number);
                        Run.httpd.socket.net.readonly = (yyvsp[0].number);
                  }
#line 3986 "src/y.tab.c"
    break;

  case 319: /* httpdsocket: UNIXSOCKET PATH httpdsocketoptionlist  */
#line 1124 "src/p.y"
                                                        {
                        Run.httpd.flags |= Httpd_Unix;
                        Run.httpd.socket.unix.path = (yyvsp[-1].string);
                  }
#line 3995 "src/y.tab.c"
    break;

  case 322: /* httpdsocketoption: UID STRING  */
#line 1134 "src/p.y"
                               {
                        Run.httpd.flags |= Httpd_UnixUid;
                        Run.httpd.socket.unix.uid = get_uid((yyvsp[0].string), 0);
                        FREE((yyvsp[0].string));
                    }
#line 4005 "src/y.tab.c"
    break;

  case 323: /* httpdsocketoption: GID STRING  */
#line 1139 "src/p.y"
                               {
                        Run.httpd.flags |= Httpd_UnixGid;
                        Run.httpd.socket.unix.gid = get_gid((yyvsp[0].string), 0);
                        FREE((yyvsp[0].string));
                    }
#line 4015 "src/y.tab.c"
    break;

  case 324: /* httpdsocketoption: UID NUMBER  */
#line 1144 "src/p.y"
                               {
                        Run.httpd.flags |= Httpd_UnixUid;
                        Run.httpd.socket.unix.uid = get_uid(NULL, (yyvsp[0].number));
                    }
#line 4024 "src/y.tab.c"
    break;

  case 325: /* httpdsocketoption: GID NUMBER  */
#line 1148 "src/p.y"
                               {
                        Run.httpd.flags |= Httpd_UnixGid;
                        Run.httpd.socket.unix.gid = get_gid(NULL, (yyvsp[0].number));
                    }
#line 4033 "src/y.tab.c"
    break;

  case 326: /* httpdsocketoption: PERMISSION NUMBER  */
#line 1152 "src/p.y"
                                      {
                        Run.httpd.flags |= Httpd_UnixPermission;
                        Run.httpd.socket.unix.permission = check_perm((yyvsp[0].number));
                    }
#line 4042 "src/y.tab.c"
    break;

  case 327: /* httpdsocketoption: READONLY  */
#line 1156 "src/p.y"
                             {
                        Run.httpd.socket.unix.readonly = true;
                    }
#line 4050 "src/y.tab.c"
    break;

  case 332: /* signature: sigenable  */
#line 1169 "src/p.y"
                             {
                        Run.httpd.flags |= Httpd_Signature;
                  }
#line 4058 "src/y.tab.c"
    break;

  case 333: /* signature: sigdisable  */
#line 1172 "src/p.y"
                             {
                        Run.httpd.flags &= ~Httpd_Signature;
                  }
#line 4066 "src/y.tab.c"
    break;

  case 334: /* bindaddress: ADDRESS STRING  */
#line 1177 "src/p.y"
                                 {
                        if (Run.httpd.socket.net.address) {
                                yywarning2("The 'address' option can be specified only once, the last value will be used\n");
                                FREE(Run.httpd.socket.net.address);
                        }
                        Run.httpd.socket.net.address = (yyvsp[0].string);
                  }
#line 4078 "src/y.tab.c"
    break;

  case 335: /* allow: ALLOW STRING ':' STRING readonly  */
#line 1186 "src/p.y"
                                                 {
                        addcredentials((yyvsp[-3].string), (yyvsp[-1].string), Digest_Cleartext, (yyvsp[0].number));
                  }
#line 4086 "src/y.tab.c"
    break;

  case 336: /* allow: ALLOW '@' STRING readonly  */
#line 1189 "src/p.y"
                                           {
#ifdef HAVE_LIBPAM
                        addpamauth((yyvsp[-1].string), (yyvsp[0].number));
#else
                        yyerror("PAM is not supported");
                        FREE((yyvsp[-1].string));
#endif
                  }
#line 4099 "src/y.tab.c"
    break;

  case 337: /* allow: ALLOW PATH  */
#line 1197 "src/p.y"
                             {
                        addhtpasswdentry((yyvsp[0].string), NULL, Digest_Cleartext);
                        FREE((yyvsp[0].string));
                  }
#line 4108 "src/y.tab.c"
    break;

  case 338: /* allow: ALLOW CLEARTEXT PATH  */
#line 1201 "src/p.y"
                                       {
                        addhtpasswdentry((yyvsp[0].string), NULL, Digest_Cleartext);
                        FREE((yyvsp[0].string));
                  }
#line 4117 "src/y.tab.c"
    break;

  case 339: /* allow: ALLOW MD5HASH PATH  */
#line 1205 "src/p.y"
                                     {
                        addhtpasswdentry((yyvsp[0].string), NULL, Digest_Md5);
                        FREE((yyvsp[0].string));
                  }
#line 4126 "src/y.tab.c"
    break;

  case 340: /* allow: ALLOW CRYPT PATH  */
#line 1209 "src/p.y"
                                   {
                        addhtpasswdentry((yyvsp[0].string), NULL, Digest_Crypt);
                        FREE((yyvsp[0].string));
                  }
#line 4135 "src/y.tab.c"
    break;

  case 341: /* $@1: %empty  */
#line 1213 "src/p.y"
                             {
                        htpasswd_file = (yyvsp[0].string);
                        digesttype = Digest_Cleartext;
                  }
#line 4144 "src/y.tab.c"
    break;

  case 342: /* allow: ALLOW PATH $@1 allowuserlist  */
#line 1217 "src/p.y"
                                {
                        FREE(htpasswd_file);
                  }
#line 4152 "src/y.tab.c"
    break;

  case 343: /* $@2: %empty  */
#line 1220 "src/p.y"
                                       {
                        htpasswd_file = (yyvsp[0].string);
                        digesttype = Digest_Cleartext;
                  }
#line 4161 "src/y.tab.c"
    break;

  case 344: /* allow: ALLOW CLEARTEXT PATH $@2 allowuserlist  */
#line 1224 "src/p.y"
                                {
                        FREE(htpasswd_file);
                  }
#line 4169 "src/y.tab.c"
    break;

  case 345: /* $@3: %empty  */
#line 1227 "src/p.y"
                                     {
                        htpasswd_file = (yyvsp[0].string);
                        digesttype = Digest_Md5;
                  }
#line 4178 "src/y.tab.c"
    break;

  case 346: /* allow: ALLOW MD5HASH PATH $@3 allowuserlist  */
#line 1231 "src/p.y"
                                {
                        FREE(htpasswd_file);
                  }
#line 4186 "src/y.tab.c"
    break;

  case 347: /* $@4: %empty  */
#line 1234 "src/p.y"
                                   {
                        htpasswd_file = (yyvsp[0].string);
                        digesttype = Digest_Crypt;
                  }
#line 4195 "src/y.tab.c"
    break;

  case 348: /* allow: ALLOW CRYPT PATH $@4 allowuserlist  */
#line 1238 "src/p.y"
                                {
                        FREE(htpasswd_file);
                  }
#line 4203 "src/y.tab.c"
    break;

  case 349: /* allow: ALLOW STRING  */
#line 1241 "src/p.y"
                               {
                        if (! Engine_addAllow((yyvsp[0].string)))
                                yywarning2("invalid allow option: %s", (yyvsp[0].string));
                        FREE((yyvsp[0].string));
                  }
#line 4213 "src/y.tab.c"
    break;

  case 352: /* allowuser: STRING  */
#line 1252 "src/p.y"
                         {
                        addhtpasswdentry(htpasswd_file, (yyvsp[0].string), digesttype);
                        FREE((yyvsp[0].string));
                  }
#line 4222 "src/y.tab.c"
    break;

  case 353: /* readonly: %empty  */
#line 1258 "src/p.y"
                              {
                        (yyval.number) = false;
                  }
#line 4230 "src/y.tab.c"
    break;

  case 354: /* readonly: READONLY  */
#line 1261 "src/p.y"
                           {
                        (yyval.number) = true;
                  }
#line 4238 "src/y.tab.c"
    break;

  case 355: /* checkproc: CHECKPROC SERVICENAME PIDFILE PATH  */
#line 1266 "src/p.y"
                                                     {
                        createservice(Service_Process, (yyvsp[-2].string), (yyvsp[0].string), check_process);
                  }
#line 4246 "src/y.tab.c"
    break;

  case 356: /* checkproc: CHECKPROC SERVICENAME PATHTOK PATH  */
#line 1269 "src/p.y"
                                                     {
                        createservice(Service_Process, (yyvsp[-2].string), (yyvsp[0].string), check_process);
                  }
#line 4254 "src/y.tab.c"
    break;

  case 357: /* checkproc: CHECKPROC SERVICENAME MATCH STRING  */
#line 1272 "src/p.y"
                                                     {
                        createservice(Service_Process, (yyvsp[-2].string), (yyvsp[0].string), check_process);
                        matchset.ignore = false;
                        matchset.match_path = NULL;
                        matchset.match_string = Str_dup((yyvsp[0].string));
                        addmatch(&matchset, Action_Ignored, 0);
                  }
#line 4266 "src/y.tab.c"
    break;

  case 358: /* checkproc: CHECKPROC SERVICENAME MATCH PATH  */
#line 1279 "src/p.y"
                                                   {
                        createservice(Service_Process, (yyvsp[-2].string), (yyvsp[0].string), check_process);
                        matchset.ignore = false;
                        matchset.match_path = NULL;
                        matchset.match_string = Str_dup((yyvsp[0].string));
                        addmatch(&matchset, Action_Ignored, 0);
                  }
#line 4278 "src/y.tab.c"
    break;

  case 359: /* checkfile: CHECKFILE SERVICENAME PATHTOK PATH  */
#line 1288 "src/p.y"
                                                     {
                        createservice(Service_File, (yyvsp[-2].string), (yyvsp[0].string), check_file);
                  }
#line 4286 "src/y.tab.c"
    break;

  case 360: /* checkfilesys: CHECKFILESYS SERVICENAME PATHTOK PATH  */
#line 1293 "src/p.y"
                                                        {
                        createservice(Service_Filesystem, (yyvsp[-2].string), (yyvsp[0].string), check_filesystem);
                  }
#line 4294 "src/y.tab.c"
    break;

  case 361: /* checkfilesys: CHECKFILESYS SERVICENAME PATHTOK STRING  */
#line 1296 "src/p.y"
                                                          {
                        createservice(Service_Filesystem, (yyvsp[-2].string), (yyvsp[0].string), check_filesystem);
                  }
#line 4302 "src/y.tab.c"
    break;

  case 362: /* checkdir: CHECKDIR SERVICENAME PATHTOK PATH  */
#line 1301 "src/p.y"
                                                    {
                        createservice(Service_Directory, (yyvsp[-2].string), (yyvsp[0].string), check_directory);
                  }
#line 4310 "src/y.tab.c"
    break;

  case 363: /* checkhost: CHECKHOST SERVICENAME ADDRESS STRING  */
#line 1306 "src/p.y"
                                                       {
                        createservice(Service_Host, (yyvsp[-2].string), (yyvsp[0].string), check_remote_host);
                  }
#line 4318 "src/y.tab.c"
    break;

  case 364: /* checknet: CHECKNET SERVICENAME ADDRESS STRING  */
#line 1311 "src/p.y"
                                                      {
                        if (Link_isGetByAddressSupported()) {
                                createservice(Service_Net, (yyvsp[-2].string), (yyvsp[0].string), check_net);
                                current->inf.net->stats = Link_createForAddress((yyvsp[0].string));
                        } else {
                                yyerror("Network monitoring by IP address is not supported on this platform, please use 'check network <foo> with interface <bar>' instead");
                        }
                  }
#line 4331 "src/y.tab.c"
    break;

  case 365: /* checknet: CHECKNET SERVICENAME INTERFACE STRING  */
#line 1319 "src/p.y"
                                                        {
                        createservice(Service_Net, (yyvsp[-2].string), (yyvsp[0].string), check_net);
                        current->inf.net->stats = Link_createForInterface((yyvsp[0].string));
                  }
#line 4340 "src/y.tab.c"
    break;

  case 366: /* checksystem: CHECKSYSTEM SERVICENAME  */
#line 1325 "src/p.y"
                                          {
                        char *servicename = (yyvsp[0].string);
                        if (Str_sub(servicename, "$HOST")) {
                                char hostname[STRLEN];
                                if (gethostname(hostname, sizeof(hostname))) {
                                        Log_error("System hostname error -- %s\n", STRERROR);
                                        cfg_errflag++;
                                } else {
                                        Util_replaceString(&servicename, "$HOST", hostname);
                                }
                        }
                        Run.system = createservice(Service_System, servicename, NULL, check_system); // The name given in the 'check system' statement overrides system hostname
                  }
#line 4358 "src/y.tab.c"
    break;

  case 367: /* checkfifo: CHECKFIFO SERVICENAME PATHTOK PATH  */
#line 1340 "src/p.y"
                                                     {
                        createservice(Service_Fifo, (yyvsp[-2].string), (yyvsp[0].string), check_fifo);
                  }
#line 4366 "src/y.tab.c"
    break;

  case 368: /* checkprogram: CHECKPROGRAM SERVICENAME PATHTOK argumentlist programtimeout  */
#line 1345 "src/p.y"
                                                                               {
                        createservice(Service_Program, (yyvsp[-3].string), NULL, check_program);
                        current->program->timeout = (yyvsp[0].number);
                        current->program->lastOutput = StringBuffer_create(64);
                        current->program->inprogressOutput = StringBuffer_create(64);
                 }
#line 4377 "src/y.tab.c"
    break;

  case 369: /* checkprogram: CHECKPROGRAM SERVICENAME PATHTOK argumentlist useroptionlist programtimeout  */
#line 1351 "src/p.y"
                                                                                              {
                        createservice(Service_Program, (yyvsp[-4].string), NULL, check_program);
                        current->program->timeout = (yyvsp[0].number);
                        current->program->lastOutput = StringBuffer_create(64);
                        current->program->inprogressOutput = StringBuffer_create(64);
                 }
#line 4388 "src/y.tab.c"
    break;

  case 370: /* start: START argumentlist starttimeout  */
#line 1359 "src/p.y"
                                                  {
                        addcommand(START, (yyvsp[0].number));
                  }
#line 4396 "src/y.tab.c"
    break;

  case 371: /* start: START argumentlist useroptionlist starttimeout  */
#line 1362 "src/p.y"
                                                                 {
                        addcommand(START, (yyvsp[0].number));
                  }
#line 4404 "src/y.tab.c"
    break;

  case 372: /* stop: STOP argumentlist stoptimeout  */
#line 1367 "src/p.y"
                                                {
                        addcommand(STOP, (yyvsp[0].number));
                  }
#line 4412 "src/y.tab.c"
    break;

  case 373: /* stop: STOP argumentlist useroptionlist stoptimeout  */
#line 1370 "src/p.y"
                                                               {
                        addcommand(STOP, (yyvsp[0].number));
                  }
#line 4420 "src/y.tab.c"
    break;

  case 374: /* restart: RESTART argumentlist restarttimeout  */
#line 1376 "src/p.y"
                                                      {
                        addcommand(RESTART, (yyvsp[0].number));
                  }
#line 4428 "src/y.tab.c"
    break;

  case 375: /* restart: RESTART argumentlist useroptionlist restarttimeout  */
#line 1379 "src/p.y"
                                                                     {
                        addcommand(RESTART, (yyvsp[0].number));
                  }
#line 4436 "src/y.tab.c"
    break;

  case 380: /* argument: STRING  */
#line 1392 "src/p.y"
                         {
                        addargument((yyvsp[0].string));
                  }
#line 4444 "src/y.tab.c"
    break;

  case 381: /* argument: PATH  */
#line 1395 "src/p.y"
                       {
                        addargument((yyvsp[0].string));
                  }
#line 4452 "src/y.tab.c"
    break;

  case 382: /* useroption: UID STRING  */
#line 1400 "src/p.y"
                             {
                        addeuid(get_uid((yyvsp[0].string), 0));
                        FREE((yyvsp[0].string));
                  }
#line 4461 "src/y.tab.c"
    break;

  case 383: /* useroption: GID STRING  */
#line 1404 "src/p.y"
                             {
                        addegid(get_gid((yyvsp[0].string), 0));
                        FREE((yyvsp[0].string));
                  }
#line 4470 "src/y.tab.c"
    break;

  case 384: /* useroption: UID NUMBER  */
#line 1408 "src/p.y"
                             {
                        addeuid(get_uid(NULL, (yyvsp[0].number)));
                  }
#line 4478 "src/y.tab.c"
    break;

  case 385: /* useroption: GID NUMBER  */
#line 1411 "src/p.y"
                             {
                        addegid(get_gid(NULL, (yyvsp[0].number)));
                  }
#line 4486 "src/y.tab.c"
    break;

  case 386: /* username: USERNAME MAILADDR  */
#line 1416 "src/p.y"
                                    {
                        (yyval.string) = (yyvsp[0].string);
                  }
#line 4494 "src/y.tab.c"
    break;

  case 387: /* username: USERNAME STRING  */
#line 1419 "src/p.y"
                                  {
                        (yyval.string) = (yyvsp[0].string);
                  }
#line 4502 "src/y.tab.c"
    break;

  case 388: /* password: PASSWORD STRING  */
#line 1424 "src/p.y"
                                  {
                        (yyval.string) = (yyvsp[0].string);
                  }
#line 4510 "src/y.tab.c"
    break;

  case 389: /* database: DATABASE STRING  */
#line 1429 "src/p.y"
                                  {
                        (yyval.string) = (yyvsp[0].string);
                  }
#line 4518 "src/y.tab.c"
    break;

  case 390: /* hostname: %empty  */
#line 1434 "src/p.y"
                                  {
                        (yyval.string) = NULL;
                  }
#line 4526 "src/y.tab.c"
    break;

  case 391: /* hostname: HOSTNAME STRING  */
#line 1437 "src/p.y"
                                  {
                        (yyval.string) = (yyvsp[0].string);
                  }
#line 4534 "src/y.tab.c"
    break;

  case 392: /* connection: IF FAILED host port connectionoptlist rate1 THEN action1 recovery_success  */
#line 1442 "src/p.y"
                                                                                            {
                        /* This is a workaround to support content match without having to create an URL object. 'urloption' creates the Request_T object we need minus the URL object, but with enough information to perform content test.
                           TODO: Parser is in need of refactoring */
                        portset.url_request = urlrequest;
                        portset.check_invers = false;
                        portset.responsetime.operator = responsetimeset.operator;
                        portset.responsetime.limit = responsetimeset.limit;
                        addeventaction(&(portset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addport(&(current->portlist), &portset);
                  }
#line 4549 "src/y.tab.c"
    break;

  case 393: /* connection: IF SUCCEEDED host port connectionoptlist rate1 THEN action1 recovery_failure  */
#line 1452 "src/p.y"
                                                                                               {
                        portset.url_request = urlrequest;
                        portset.check_invers = true;
                        portset.responsetime.operator = responsetimeset.operator;
                        portset.responsetime.limit = responsetimeset.limit;
                        addeventaction(&(portset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addport(&(current->portlist), &portset);
                  }
#line 4562 "src/y.tab.c"
    break;

  case 408: /* connectionurl: IF FAILED URL URLOBJECT connectionurloptlist rate1 THEN action1 recovery_success  */
#line 1480 "src/p.y"
                                                                                                   {
                        portset.check_invers = false;
                        portset.responsetime.operator = responsetimeset.operator;
                        portset.responsetime.limit = responsetimeset.limit;
                        prepare_urlrequest((yyvsp[-5].url));
                        addeventaction(&(portset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addport(&(current->portlist), &portset);
                  }
#line 4575 "src/y.tab.c"
    break;

  case 409: /* connectionurl: IF SUCCEEDED URL URLOBJECT connectionurloptlist rate1 THEN action1 recovery_failure  */
#line 1488 "src/p.y"
                                                                                                      {
                        portset.check_invers = true;
                        portset.responsetime.operator = responsetimeset.operator;
                        portset.responsetime.limit = responsetimeset.limit;
                        prepare_urlrequest((yyvsp[-5].url));
                        addeventaction(&(portset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addport(&(current->portlist), &portset);
                  }
#line 4588 "src/y.tab.c"
    break;

  case 418: /* connectionunix: IF FAILED unixsocket connectionuxoptlist rate1 THEN action1 recovery_success  */
#line 1510 "src/p.y"
                                                                                               {
                        portset.check_invers = false;
                        portset.responsetime.operator = responsetimeset.operator;
                        portset.responsetime.limit = responsetimeset.limit;
                        addeventaction(&(portset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addport(&(current->socketlist), &portset);
                  }
#line 4600 "src/y.tab.c"
    break;

  case 419: /* connectionunix: IF SUCCEEDED unixsocket connectionuxoptlist rate1 THEN action1 recovery_failure  */
#line 1517 "src/p.y"
                                                                                                  {
                        portset.check_invers = true;
                        portset.responsetime.operator = responsetimeset.operator;
                        portset.responsetime.limit = responsetimeset.limit;
                        addeventaction(&(portset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addport(&(current->socketlist), &portset);
                  }
#line 4612 "src/y.tab.c"
    break;

  case 428: /* icmp: IF FAILED ICMP icmptype icmpoptlist rate1 THEN action1 recovery_success  */
#line 1538 "src/p.y"
                                                                                          {
                        icmpset.family = Socket_Ip;
                        icmpset.check_invers = false;
                        icmpset.type = (yyvsp[-5].number);
                        icmpset.responsetime.operator = responsetimeset.operator;
                        icmpset.responsetime.limit = responsetimeset.limit;
                        addeventaction(&(icmpset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addicmp(&icmpset);
                  }
#line 4626 "src/y.tab.c"
    break;

  case 429: /* icmp: IF FAILED PING icmpoptlist rate1 THEN action1 recovery_success  */
#line 1547 "src/p.y"
                                                                                 {
                        icmpset.family = Socket_Ip;
                        icmpset.check_invers = false;
                        icmpset.responsetime.operator = responsetimeset.operator;
                        icmpset.responsetime.limit = responsetimeset.limit;
                        addeventaction(&(icmpset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addicmp(&icmpset);
                 }
#line 4639 "src/y.tab.c"
    break;

  case 430: /* icmp: IF FAILED PING4 icmpoptlist rate1 THEN action1 recovery_success  */
#line 1555 "src/p.y"
                                                                                  {
                        icmpset.family = Socket_Ip4;
                        icmpset.check_invers = false;
                        icmpset.responsetime.operator = responsetimeset.operator;
                        icmpset.responsetime.limit = responsetimeset.limit;
                        addeventaction(&(icmpset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addicmp(&icmpset);
                 }
#line 4652 "src/y.tab.c"
    break;

  case 431: /* icmp: IF FAILED PING6 icmpoptlist rate1 THEN action1 recovery_success  */
#line 1563 "src/p.y"
                                                                                  {
                        icmpset.family = Socket_Ip6;
                        icmpset.check_invers = false;
                        icmpset.responsetime.operator = responsetimeset.operator;
                        icmpset.responsetime.limit = responsetimeset.limit;
                        addeventaction(&(icmpset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addicmp(&icmpset);
                 }
#line 4665 "src/y.tab.c"
    break;

  case 432: /* icmp: IF SUCCEEDED ICMP icmptype icmpoptlist rate1 THEN action1 recovery_failure  */
#line 1571 "src/p.y"
                                                                                             {
                        icmpset.family = Socket_Ip;
                        icmpset.check_invers = true;
                        icmpset.type = (yyvsp[-5].number);
                        icmpset.responsetime.operator = responsetimeset.operator;
                        icmpset.responsetime.limit = responsetimeset.limit;
                        addeventaction(&(icmpset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addicmp(&icmpset);
                  }
#line 4679 "src/y.tab.c"
    break;

  case 433: /* icmp: IF SUCCEEDED PING icmpoptlist rate1 THEN action1 recovery_failure  */
#line 1580 "src/p.y"
                                                                                    {
                        icmpset.family = Socket_Ip;
                        icmpset.check_invers = true;
                        icmpset.responsetime.operator = responsetimeset.operator;
                        icmpset.responsetime.limit = responsetimeset.limit;
                        addeventaction(&(icmpset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addicmp(&icmpset);
                 }
#line 4692 "src/y.tab.c"
    break;

  case 434: /* icmp: IF SUCCEEDED PING4 icmpoptlist rate1 THEN action1 recovery_failure  */
#line 1588 "src/p.y"
                                                                                     {
                        icmpset.family = Socket_Ip4;
                        icmpset.check_invers = true;
                        icmpset.responsetime.operator = responsetimeset.operator;
                        icmpset.responsetime.limit = responsetimeset.limit;
                        addeventaction(&(icmpset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addicmp(&icmpset);
                 }
#line 4705 "src/y.tab.c"
    break;

  case 435: /* icmp: IF SUCCEEDED PING6 icmpoptlist rate1 THEN action1 recovery_failure  */
#line 1596 "src/p.y"
                                                                                     {
                        icmpset.family = Socket_Ip6;
                        icmpset.check_invers = true;
                        icmpset.responsetime.operator = responsetimeset.operator;
                        icmpset.responsetime.limit = responsetimeset.limit;
                        addeventaction(&(icmpset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addicmp(&icmpset);
                 }
#line 4718 "src/y.tab.c"
    break;

  case 443: /* host: %empty  */
#line 1617 "src/p.y"
                              {
                        portset.hostname = Str_dup(current->type == Service_Host ? current->path : LOCALHOST);
                  }
#line 4726 "src/y.tab.c"
    break;

  case 444: /* host: HOST STRING  */
#line 1620 "src/p.y"
                              {
                        portset.hostname = (yyvsp[0].string);
                  }
#line 4734 "src/y.tab.c"
    break;

  case 445: /* port: PORT NUMBER  */
#line 1625 "src/p.y"
                              {
                        portset.target.net.port = (yyvsp[0].number);
                  }
#line 4742 "src/y.tab.c"
    break;

  case 446: /* unixsocket: UNIXSOCKET PATH  */
#line 1630 "src/p.y"
                                  {
                        portset.family = Socket_Unix;
                        portset.target.unix.pathname = (yyvsp[0].string);
                  }
#line 4751 "src/y.tab.c"
    break;

  case 447: /* ip: IPV4  */
#line 1636 "src/p.y"
                       {
                        portset.family = Socket_Ip4;
                  }
#line 4759 "src/y.tab.c"
    break;

  case 448: /* ip: IPV6  */
#line 1639 "src/p.y"
                       {
                        portset.family = Socket_Ip6;
                  }
#line 4767 "src/y.tab.c"
    break;

  case 449: /* type: TYPE TCP  */
#line 1644 "src/p.y"
                           {
                        portset.type = Socket_Tcp;
                  }
#line 4775 "src/y.tab.c"
    break;

  case 450: /* type: TYPE TCPSSL typeoptlist  */
#line 1647 "src/p.y"
                                          { // The typelist is kept for backward compatibility (replaced by ssloptionlist)
                        portset.type = Socket_Tcp;
                        sslset.flags = SSL_Enabled;
                  }
#line 4784 "src/y.tab.c"
    break;

  case 451: /* type: TYPE UDP  */
#line 1651 "src/p.y"
                           {
                        portset.type = Socket_Udp;
                  }
#line 4792 "src/y.tab.c"
    break;

  case 456: /* outgoing: ADDRESS STRING  */
#line 1664 "src/p.y"
                                 {
                        _parseOutgoingAddress((yyvsp[0].string), &(portset.outgoing));
                  }
#line 4800 "src/y.tab.c"
    break;

  case 457: /* protocol: PROTOCOL APACHESTATUS apache_stat_list  */
#line 1669 "src/p.y"
                                                         {
                        portset.protocol = Protocol_get(Protocol_APACHESTATUS);
                  }
#line 4808 "src/y.tab.c"
    break;

  case 458: /* protocol: PROTOCOL CLAMAV  */
#line 1672 "src/p.y"
                                  {
                        portset.protocol = Protocol_get(Protocol_CLAMAV);
                  }
#line 4816 "src/y.tab.c"
    break;

  case 459: /* protocol: PROTOCOL DEFAULT  */
#line 1675 "src/p.y"
                                   {
                        portset.protocol = Protocol_get(Protocol_DEFAULT);
                  }
#line 4824 "src/y.tab.c"
    break;

  case 460: /* protocol: PROTOCOL DNS  */
#line 1678 "src/p.y"
                               {
                        portset.protocol = Protocol_get(Protocol_DNS);
                  }
#line 4832 "src/y.tab.c"
    break;

  case 461: /* protocol: PROTOCOL DWP  */
#line 1681 "src/p.y"
                                {
                        portset.protocol = Protocol_get(Protocol_DWP);
                  }
#line 4840 "src/y.tab.c"
    break;

  case 462: /* protocol: PROTOCOL FAIL2BAN  */
#line 1684 "src/p.y"
                                    {
                        portset.protocol = Protocol_get(Protocol_FAIL2BAN);
                }
#line 4848 "src/y.tab.c"
    break;

  case 463: /* protocol: PROTOCOL FTP  */
#line 1687 "src/p.y"
                               {
                        portset.protocol = Protocol_get(Protocol_FTP);
                  }
#line 4856 "src/y.tab.c"
    break;

  case 464: /* protocol: PROTOCOL GPS  */
#line 1690 "src/p.y"
                               {
                        portset.protocol = Protocol_get(Protocol_GPS);
                  }
#line 4864 "src/y.tab.c"
    break;

  case 465: /* protocol: PROTOCOL HTTP httplist  */
#line 1693 "src/p.y"
                                         {
                        portset.protocol = Protocol_get(Protocol_HTTP);
                  }
#line 4872 "src/y.tab.c"
    break;

  case 466: /* protocol: PROTOCOL HTTPS httplist  */
#line 1696 "src/p.y"
                                          {
                        sslset.flags = SSL_Enabled;
                        portset.type = Socket_Tcp;
                        portset.protocol = Protocol_get(Protocol_HTTP);
                 }
#line 4882 "src/y.tab.c"
    break;

  case 467: /* protocol: PROTOCOL IMAP  */
#line 1701 "src/p.y"
                                {
                        portset.protocol = Protocol_get(Protocol_IMAP);
                  }
#line 4890 "src/y.tab.c"
    break;

  case 468: /* protocol: PROTOCOL IMAPS  */
#line 1704 "src/p.y"
                                 {
                        sslset.flags = SSL_Enabled;
                        portset.type = Socket_Tcp;
                        portset.protocol = Protocol_get(Protocol_IMAP);
                  }
#line 4900 "src/y.tab.c"
    break;

  case 469: /* protocol: PROTOCOL LDAP2  */
#line 1709 "src/p.y"
                                 {
                        portset.protocol = Protocol_get(Protocol_LDAP2);
                  }
#line 4908 "src/y.tab.c"
    break;

  case 470: /* protocol: PROTOCOL LDAP3  */
#line 1712 "src/p.y"
                                 {
                        portset.protocol = Protocol_get(Protocol_LDAP3);
                  }
#line 4916 "src/y.tab.c"
    break;

  case 471: /* protocol: PROTOCOL LMTP  */
#line 1715 "src/p.y"
                                {
                        portset.protocol = Protocol_get(Protocol_LMTP);
                  }
#line 4924 "src/y.tab.c"
    break;

  case 472: /* protocol: PROTOCOL MEMCACHE  */
#line 1718 "src/p.y"
                                    {
                        portset.protocol = Protocol_get(Protocol_MEMCACHE);
                  }
#line 4932 "src/y.tab.c"
    break;

  case 473: /* protocol: PROTOCOL MONGODB  */
#line 1721 "src/p.y"
                                    {
                        portset.protocol = Protocol_get(Protocol_MONGODB);
                  }
#line 4940 "src/y.tab.c"
    break;

  case 474: /* protocol: PROTOCOL MQTT mqttlist  */
#line 1724 "src/p.y"
                                         {
                        portset.protocol = Protocol_get(Protocol_MQTT);
                  }
#line 4948 "src/y.tab.c"
    break;

  case 475: /* protocol: PROTOCOL MYSQL mysqllist  */
#line 1727 "src/p.y"
                                           {
                        portset.protocol = Protocol_get(Protocol_MYSQL);
                  }
#line 4956 "src/y.tab.c"
    break;

  case 476: /* protocol: PROTOCOL MYSQLS mysqllist  */
#line 1730 "src/p.y"
                                            {
                        sslset.flags = SSL_StartTLS;
                        portset.protocol = Protocol_get(Protocol_MYSQL);
                  }
#line 4965 "src/y.tab.c"
    break;

  case 477: /* protocol: PROTOCOL NNTP  */
#line 1734 "src/p.y"
                                {
                        portset.protocol = Protocol_get(Protocol_NNTP);
                  }
#line 4973 "src/y.tab.c"
    break;

  case 478: /* protocol: PROTOCOL NTP3  */
#line 1737 "src/p.y"
                                 {
                        portset.protocol = Protocol_get(Protocol_NTP3);
                        portset.type = Socket_Udp;
                  }
#line 4982 "src/y.tab.c"
    break;

  case 479: /* protocol: PROTOCOL PGSQL postgresqllist  */
#line 1741 "src/p.y"
                                                {
                        portset.protocol = Protocol_get(Protocol_PGSQL);
                  }
#line 4990 "src/y.tab.c"
    break;

  case 480: /* protocol: PROTOCOL POP  */
#line 1744 "src/p.y"
                               {
                        portset.protocol = Protocol_get(Protocol_POP);
                  }
#line 4998 "src/y.tab.c"
    break;

  case 481: /* protocol: PROTOCOL POPS  */
#line 1747 "src/p.y"
                                {
                        sslset.flags = SSL_Enabled;
                        portset.type = Socket_Tcp;
                        portset.protocol = Protocol_get(Protocol_POP);
                  }
#line 5008 "src/y.tab.c"
    break;

  case 482: /* protocol: PROTOCOL POSTFIXPOLICY  */
#line 1752 "src/p.y"
                                         {
                        portset.protocol = Protocol_get(Protocol_POSTFIXPOLICY);
                  }
#line 5016 "src/y.tab.c"
    break;

  case 483: /* protocol: PROTOCOL RADIUS radiuslist  */
#line 1755 "src/p.y"
                                             {
                        portset.protocol = Protocol_get(Protocol_RADIUS);
                  }
#line 5024 "src/y.tab.c"
    break;

  case 484: /* protocol: PROTOCOL RDATE  */
#line 1758 "src/p.y"
                                 {
                        portset.protocol = Protocol_get(Protocol_RDATE);
                  }
#line 5032 "src/y.tab.c"
    break;

  case 485: /* protocol: PROTOCOL REDIS  */
#line 1761 "src/p.y"
                                  {
                        portset.protocol = Protocol_get(Protocol_REDIS);
                  }
#line 5040 "src/y.tab.c"
    break;

  case 486: /* protocol: PROTOCOL RSYNC  */
#line 1764 "src/p.y"
                                 {
                        portset.protocol = Protocol_get(Protocol_RSYNC);
                  }
#line 5048 "src/y.tab.c"
    break;

  case 487: /* protocol: PROTOCOL SIEVE  */
#line 1767 "src/p.y"
                                 {
                        portset.protocol = Protocol_get(Protocol_SIEVE);
                  }
#line 5056 "src/y.tab.c"
    break;

  case 488: /* protocol: PROTOCOL SIP siplist  */
#line 1770 "src/p.y"
                                       {
                        portset.protocol = Protocol_get(Protocol_SIP);
                  }
#line 5064 "src/y.tab.c"
    break;

  case 489: /* protocol: PROTOCOL SMTP smtplist  */
#line 1773 "src/p.y"
                                         {
                        portset.protocol = Protocol_get(Protocol_SMTP);
                  }
#line 5072 "src/y.tab.c"
    break;

  case 490: /* protocol: PROTOCOL SMTPS smtplist  */
#line 1776 "src/p.y"
                                          {
                        sslset.flags = SSL_Enabled;
                        portset.type = Socket_Tcp;
                        portset.protocol = Protocol_get(Protocol_SMTP);
                 }
#line 5082 "src/y.tab.c"
    break;

  case 491: /* protocol: PROTOCOL SPAMASSASSIN  */
#line 1781 "src/p.y"
                                        {
                        portset.protocol = Protocol_get(Protocol_SPAMASSASSIN);
                  }
#line 5090 "src/y.tab.c"
    break;

  case 492: /* protocol: PROTOCOL SSH  */
#line 1784 "src/p.y"
                                {
                        portset.protocol = Protocol_get(Protocol_SSH);
                  }
#line 5098 "src/y.tab.c"
    break;

  case 493: /* protocol: PROTOCOL TNS  */
#line 1787 "src/p.y"
                               {
                        portset.protocol = Protocol_get(Protocol_TNS);
                  }
#line 5106 "src/y.tab.c"
    break;

  case 494: /* protocol: PROTOCOL WEBSOCKET websocketlist  */
#line 1790 "src/p.y"
                                                   {
                        portset.protocol = Protocol_get(Protocol_WEBSOCKET);
                  }
#line 5114 "src/y.tab.c"
    break;

  case 495: /* sendexpect: SEND STRING  */
#line 1795 "src/p.y"
                              {
                        if (portset.protocol->check == check_default || portset.protocol->check == check_generic) {
                                portset.protocol = Protocol_get(Protocol_GENERIC);
                                addgeneric(&portset, (yyvsp[0].string), NULL);
                        } else {
                                yyerror("The SEND statement is not allowed in the %s protocol context", portset.protocol->name);
                        }
                  }
#line 5127 "src/y.tab.c"
    break;

  case 496: /* sendexpect: EXPECT STRING  */
#line 1803 "src/p.y"
                                {
                        if (portset.protocol->check == check_default || portset.protocol->check == check_generic) {
                                portset.protocol = Protocol_get(Protocol_GENERIC);
                                addgeneric(&portset, NULL, (yyvsp[0].string));
                        } else {
                                yyerror("The EXPECT statement is not allowed in the %s protocol context", portset.protocol->name);
                        }
                  }
#line 5140 "src/y.tab.c"
    break;

  case 499: /* websocket: ORIGIN STRING  */
#line 1817 "src/p.y"
                                {
                        portset.parameters.websocket.origin = (yyvsp[0].string);
                  }
#line 5148 "src/y.tab.c"
    break;

  case 500: /* websocket: REQUEST PATH  */
#line 1820 "src/p.y"
                               {
                        portset.parameters.websocket.request = (yyvsp[0].string);
                  }
#line 5156 "src/y.tab.c"
    break;

  case 501: /* websocket: HOST STRING  */
#line 1823 "src/p.y"
                              {
                        portset.parameters.websocket.host = (yyvsp[0].string);
                  }
#line 5164 "src/y.tab.c"
    break;

  case 502: /* websocket: VERSIONOPT NUMBER  */
#line 1826 "src/p.y"
                                    {
                        portset.parameters.websocket.version = (yyvsp[0].number);
                  }
#line 5172 "src/y.tab.c"
    break;

  case 505: /* smtp: username  */
#line 1835 "src/p.y"
                           {
                        portset.parameters.smtp.username = (yyvsp[0].string);
                  }
#line 5180 "src/y.tab.c"
    break;

  case 506: /* smtp: password  */
#line 1838 "src/p.y"
                           {
                        portset.parameters.smtp.password = (yyvsp[0].string);
                  }
#line 5188 "src/y.tab.c"
    break;

  case 509: /* mqtt: username  */
#line 1847 "src/p.y"
                           {
                        portset.parameters.mqtt.username = (yyvsp[0].string);
                  }
#line 5196 "src/y.tab.c"
    break;

  case 510: /* mqtt: password  */
#line 1850 "src/p.y"
                           {
                        portset.parameters.mqtt.password = (yyvsp[0].string);
                  }
#line 5204 "src/y.tab.c"
    break;

  case 513: /* mysql: username  */
#line 1859 "src/p.y"
                           {
                        portset.parameters.mysql.username = (yyvsp[0].string);
                  }
#line 5212 "src/y.tab.c"
    break;

  case 514: /* mysql: password  */
#line 1862 "src/p.y"
                           {
                        portset.parameters.mysql.password = (yyvsp[0].string);
                  }
#line 5220 "src/y.tab.c"
    break;

  case 515: /* mysql: RSAKEY CHECKSUM checksumoperator STRING  */
#line 1865 "src/p.y"
                                                          {
                        portset.parameters.mysql.rsaChecksum = (yyvsp[0].string);
                        switch (cleanup_hash_string(portset.parameters.mysql.rsaChecksum)) {
                                case 32:
                                        portset.parameters.mysql.rsaChecksumType = Hash_Md5;
                                        break;
                                case 40:
                                        portset.parameters.mysql.rsaChecksumType = Hash_Sha1;
                                        break;
                                default:
                                        yyerror2("Unknown checksum type: [%s] is not MD5 nor SHA1", portset.parameters.mysql.rsaChecksum);
                        }
                  }
#line 5238 "src/y.tab.c"
    break;

  case 516: /* mysql: RSAKEY CHECKSUM MD5HASH checksumoperator STRING  */
#line 1878 "src/p.y"
                                                                  {
                        portset.parameters.mysql.rsaChecksum = (yyvsp[0].string);
                        if (cleanup_hash_string(portset.parameters.mysql.rsaChecksum) != 32)
                                yyerror2("Unknown checksum type: [%s] is not MD5", portset.parameters.mysql.rsaChecksum);
                        portset.parameters.mysql.rsaChecksumType = Hash_Md5;
                  }
#line 5249 "src/y.tab.c"
    break;

  case 517: /* mysql: RSAKEY CHECKSUM SHA1HASH checksumoperator STRING  */
#line 1884 "src/p.y"
                                                                   {
                        portset.parameters.mysql.rsaChecksum = (yyvsp[0].string);
                        if (cleanup_hash_string(portset.parameters.mysql.rsaChecksum) != 40)
                                yyerror2("Unknown checksum type: [%s] is not SHA1", portset.parameters.mysql.rsaChecksum);
                        portset.parameters.mysql.rsaChecksumType = Hash_Sha1;
                  }
#line 5260 "src/y.tab.c"
    break;

  case 520: /* postgresql: username  */
#line 1896 "src/p.y"
                           {
                        portset.parameters.postgresql.username = (yyvsp[0].string);
                  }
#line 5268 "src/y.tab.c"
    break;

  case 521: /* postgresql: password  */
#line 1899 "src/p.y"
                           {
                        portset.parameters.postgresql.password = (yyvsp[0].string);
                  }
#line 5276 "src/y.tab.c"
    break;

  case 522: /* postgresql: database  */
#line 1902 "src/p.y"
                           {
                        portset.parameters.postgresql.database = (yyvsp[0].string);
                  }
#line 5284 "src/y.tab.c"
    break;

  case 523: /* target: TARGET MAILADDR  */
#line 1907 "src/p.y"
                                  {
                        (yyval.string) = (yyvsp[0].string);
                  }
#line 5292 "src/y.tab.c"
    break;

  case 524: /* target: TARGET STRING  */
#line 1910 "src/p.y"
                                {
                        (yyval.string) = (yyvsp[0].string);
                  }
#line 5300 "src/y.tab.c"
    break;

  case 525: /* maxforward: MAXFORWARD NUMBER  */
#line 1915 "src/p.y"
                                    {
                        (yyval.number) = verifyMaxForward((yyvsp[0].number));
                  }
#line 5308 "src/y.tab.c"
    break;

  case 528: /* sip: target  */
#line 1924 "src/p.y"
                         {
                        portset.parameters.sip.target = (yyvsp[0].string);
                  }
#line 5316 "src/y.tab.c"
    break;

  case 529: /* sip: maxforward  */
#line 1927 "src/p.y"
                             {
                        portset.parameters.sip.maxforward = (yyvsp[0].number);
                  }
#line 5324 "src/y.tab.c"
    break;

  case 532: /* http: username  */
#line 1936 "src/p.y"
                           {
                        portset.parameters.http.username = (yyvsp[0].string);
                  }
#line 5332 "src/y.tab.c"
    break;

  case 533: /* http: password  */
#line 1939 "src/p.y"
                           {
                        portset.parameters.http.password = (yyvsp[0].string);
                  }
#line 5340 "src/y.tab.c"
    break;

  case 540: /* status: STATUS operator NUMBER  */
#line 1950 "src/p.y"
                                         {
                        if ((yyvsp[0].number) < 0) {
                                yyerror2("The status value must be greater or equal to 0");
                        }
                        portset.parameters.http.operator = (yyvsp[-1].number);
                        portset.parameters.http.status = (yyvsp[0].number);
                        portset.parameters.http.hasStatus = true;
                  }
#line 5353 "src/y.tab.c"
    break;

  case 541: /* method: METHOD GET  */
#line 1960 "src/p.y"
                             {
                        portset.parameters.http.method = Http_Get;
                  }
#line 5361 "src/y.tab.c"
    break;

  case 542: /* method: METHOD HEAD  */
#line 1963 "src/p.y"
                              {
                        portset.parameters.http.method = Http_Head;
                  }
#line 5369 "src/y.tab.c"
    break;

  case 543: /* request: REQUEST PATH  */
#line 1968 "src/p.y"
                               {
                        portset.parameters.http.request = Util_urlEncode((yyvsp[0].string), false);
                        FREE((yyvsp[0].string));
                  }
#line 5378 "src/y.tab.c"
    break;

  case 544: /* request: REQUEST STRING  */
#line 1972 "src/p.y"
                                 {
                        portset.parameters.http.request = Util_urlEncode((yyvsp[0].string), false);
                        FREE((yyvsp[0].string));
                  }
#line 5387 "src/y.tab.c"
    break;

  case 545: /* responsesum: CHECKSUM STRING  */
#line 1978 "src/p.y"
                                  {
                        portset.parameters.http.checksum = (yyvsp[0].string);
                  }
#line 5395 "src/y.tab.c"
    break;

  case 546: /* hostheader: HOSTHEADER STRING  */
#line 1983 "src/p.y"
                                    {
                        addhttpheader(&portset, Str_cat("Host:%s", (yyvsp[0].string)));
                        FREE((yyvsp[0].string));
                  }
#line 5404 "src/y.tab.c"
    break;

  case 548: /* httpheaderlist: httpheaderlist HTTPHEADER  */
#line 1990 "src/p.y"
                                            {
                        addhttpheader(&portset, (yyvsp[0].string));
                 }
#line 5412 "src/y.tab.c"
    break;

  case 549: /* secret: SECRET STRING  */
#line 1995 "src/p.y"
                                {
                        (yyval.string) = (yyvsp[0].string);
                  }
#line 5420 "src/y.tab.c"
    break;

  case 552: /* radius: secret  */
#line 2004 "src/p.y"
                         {
                        portset.parameters.radius.secret = (yyvsp[0].string);
                  }
#line 5428 "src/y.tab.c"
    break;

  case 555: /* apache_stat: username  */
#line 2013 "src/p.y"
                           {
                        portset.parameters.apachestatus.username = (yyvsp[0].string);
                  }
#line 5436 "src/y.tab.c"
    break;

  case 556: /* apache_stat: password  */
#line 2016 "src/p.y"
                           {
                        portset.parameters.apachestatus.password = (yyvsp[0].string);
                  }
#line 5444 "src/y.tab.c"
    break;

  case 557: /* apache_stat: PATHTOK PATH  */
#line 2019 "src/p.y"
                               {
                        portset.parameters.apachestatus.path = (yyvsp[0].string);
                  }
#line 5452 "src/y.tab.c"
    break;

  case 558: /* apache_stat: LOGLIMIT operator NUMBER PERCENT  */
#line 2022 "src/p.y"
                                                   {
                        portset.parameters.apachestatus.loglimitOP = (yyvsp[-2].number);
                        portset.parameters.apachestatus.loglimit = (yyvsp[-1].number);
                  }
#line 5461 "src/y.tab.c"
    break;

  case 559: /* apache_stat: CLOSELIMIT operator NUMBER PERCENT  */
#line 2026 "src/p.y"
                                                     {
                        portset.parameters.apachestatus.closelimitOP = (yyvsp[-2].number);
                        portset.parameters.apachestatus.closelimit = (yyvsp[-1].number);
                  }
#line 5470 "src/y.tab.c"
    break;

  case 560: /* apache_stat: DNSLIMIT operator NUMBER PERCENT  */
#line 2030 "src/p.y"
                                                   {
                        portset.parameters.apachestatus.dnslimitOP = (yyvsp[-2].number);
                        portset.parameters.apachestatus.dnslimit = (yyvsp[-1].number);
                  }
#line 5479 "src/y.tab.c"
    break;

  case 561: /* apache_stat: KEEPALIVELIMIT operator NUMBER PERCENT  */
#line 2034 "src/p.y"
                                                         {
                        portset.parameters.apachestatus.keepalivelimitOP = (yyvsp[-2].number);
                        portset.parameters.apachestatus.keepalivelimit = (yyvsp[-1].number);
                  }
#line 5488 "src/y.tab.c"
    break;

  case 562: /* apache_stat: REPLYLIMIT operator NUMBER PERCENT  */
#line 2038 "src/p.y"
                                                     {
                        portset.parameters.apachestatus.replylimitOP = (yyvsp[-2].number);
                        portset.parameters.apachestatus.replylimit = (yyvsp[-1].number);
                  }
#line 5497 "src/y.tab.c"
    break;

  case 563: /* apache_stat: REQUESTLIMIT operator NUMBER PERCENT  */
#line 2042 "src/p.y"
                                                       {
                        portset.parameters.apachestatus.requestlimitOP = (yyvsp[-2].number);
                        portset.parameters.apachestatus.requestlimit = (yyvsp[-1].number);
                  }
#line 5506 "src/y.tab.c"
    break;

  case 564: /* apache_stat: STARTLIMIT operator NUMBER PERCENT  */
#line 2046 "src/p.y"
                                                     {
                        portset.parameters.apachestatus.startlimitOP = (yyvsp[-2].number);
                        portset.parameters.apachestatus.startlimit = (yyvsp[-1].number);
                  }
#line 5515 "src/y.tab.c"
    break;

  case 565: /* apache_stat: WAITLIMIT operator NUMBER PERCENT  */
#line 2050 "src/p.y"
                                                    {
                        portset.parameters.apachestatus.waitlimitOP = (yyvsp[-2].number);
                        portset.parameters.apachestatus.waitlimit = (yyvsp[-1].number);
                  }
#line 5524 "src/y.tab.c"
    break;

  case 566: /* apache_stat: GRACEFULLIMIT operator NUMBER PERCENT  */
#line 2054 "src/p.y"
                                                        {
                        portset.parameters.apachestatus.gracefullimitOP = (yyvsp[-2].number);
                        portset.parameters.apachestatus.gracefullimit = (yyvsp[-1].number);
                  }
#line 5533 "src/y.tab.c"
    break;

  case 567: /* apache_stat: CLEANUPLIMIT operator NUMBER PERCENT  */
#line 2058 "src/p.y"
                                                       {
                        portset.parameters.apachestatus.cleanuplimitOP = (yyvsp[-2].number);
                        portset.parameters.apachestatus.cleanuplimit = (yyvsp[-1].number);
                  }
#line 5542 "src/y.tab.c"
    break;

  case 568: /* exist: IF NOT EXIST rate1 THEN action1 recovery_success  */
#line 2064 "src/p.y"
                                                                   {
                        addeventaction(&(nonexistset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addnonexist(&nonexistset);
                  }
#line 5551 "src/y.tab.c"
    break;

  case 569: /* exist: IF EXIST rate1 THEN action1 recovery_success  */
#line 2068 "src/p.y"
                                                               {
                        addeventaction(&(existset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addexist(&existset);
                  }
#line 5560 "src/y.tab.c"
    break;

  case 570: /* pid: IF CHANGED PID rate1 THEN action1  */
#line 2075 "src/p.y"
                                                    {
                        addeventaction(&(pidset).action, (yyvsp[0].number), Action_Ignored);
                        addpid(&pidset);
                  }
#line 5569 "src/y.tab.c"
    break;

  case 571: /* ppid: IF CHANGED PPID rate1 THEN action1  */
#line 2081 "src/p.y"
                                                     {
                        addeventaction(&(ppidset).action, (yyvsp[0].number), Action_Ignored);
                        addppid(&ppidset);
                  }
#line 5578 "src/y.tab.c"
    break;

  case 572: /* uptime: IF UPTIME operator NUMBER time rate1 THEN action1 recovery_success  */
#line 2087 "src/p.y"
                                                                                     {
                        uptimeset.operator = (yyvsp[-6].number);
                        uptimeset.uptime = ((unsigned long long)(yyvsp[-5].number) * (yyvsp[-4].number));
                        addeventaction(&(uptimeset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        adduptime(&uptimeset);
                  }
#line 5589 "src/y.tab.c"
    break;

  case 573: /* responsetime: RESPONSETIME operator NUMBER MILLISECOND  */
#line 2095 "src/p.y"
                                                           {
                        responsetimeset.operator = (yyvsp[-2].number);
                        responsetimeset.limit = (yyvsp[-1].number);
                  }
#line 5598 "src/y.tab.c"
    break;

  case 574: /* responsetime: RESPONSETIME operator NUMBER SECOND  */
#line 2099 "src/p.y"
                                                      {
                        responsetimeset.operator = (yyvsp[-2].number);
                        responsetimeset.limit = (yyvsp[-1].number) * 1000;
                  }
#line 5607 "src/y.tab.c"
    break;

  case 575: /* icmpcount: COUNT NUMBER  */
#line 2105 "src/p.y"
                               {
                        icmpset.count = (yyvsp[0].number);
                 }
#line 5615 "src/y.tab.c"
    break;

  case 576: /* icmpsize: SIZE NUMBER  */
#line 2110 "src/p.y"
                              {
                        icmpset.size = (yyvsp[0].number);
                        if (icmpset.size < 8) {
                                yyerror2("The minimum ping size is 8 bytes");
                        } else if (icmpset.size > 1492) {
                                yyerror2("The maximum ping size is 1492 bytes");
                        }
                 }
#line 5628 "src/y.tab.c"
    break;

  case 577: /* icmptimeout: TIMEOUT NUMBER SECOND  */
#line 2120 "src/p.y"
                                        {
                        icmpset.timeout = (yyvsp[-1].number) * 1000; // timeout is in milliseconds internally
                    }
#line 5636 "src/y.tab.c"
    break;

  case 578: /* icmpoutgoing: ADDRESS STRING  */
#line 2125 "src/p.y"
                                 {
                        _parseOutgoingAddress((yyvsp[0].string), &(icmpset.outgoing));
                  }
#line 5644 "src/y.tab.c"
    break;

  case 579: /* stoptimeout: %empty  */
#line 2130 "src/p.y"
                              {
                        (yyval.number) = Run.limits.stopTimeout;
                  }
#line 5652 "src/y.tab.c"
    break;

  case 580: /* stoptimeout: TIMEOUT NUMBER SECOND  */
#line 2133 "src/p.y"
                                        {
                        (yyval.number) = (yyvsp[-1].number) * 1000; // milliseconds internally
                  }
#line 5660 "src/y.tab.c"
    break;

  case 581: /* starttimeout: %empty  */
#line 2138 "src/p.y"
                              {
                        (yyval.number) = Run.limits.startTimeout;
                  }
#line 5668 "src/y.tab.c"
    break;

  case 582: /* starttimeout: TIMEOUT NUMBER SECOND  */
#line 2141 "src/p.y"
                                        {
                        (yyval.number) = (yyvsp[-1].number) * 1000; // milliseconds internally
                  }
#line 5676 "src/y.tab.c"
    break;

  case 583: /* restarttimeout: %empty  */
#line 2146 "src/p.y"
                              {
                        (yyval.number) = Run.limits.restartTimeout;
                  }
#line 5684 "src/y.tab.c"
    break;

  case 584: /* restarttimeout: TIMEOUT NUMBER SECOND  */
#line 2149 "src/p.y"
                                        {
                        (yyval.number) = (yyvsp[-1].number) * 1000; // milliseconds internally
                  }
#line 5692 "src/y.tab.c"
    break;

  case 585: /* programtimeout: %empty  */
#line 2154 "src/p.y"
                              {
                        (yyval.number) = Run.limits.programTimeout;
                  }
#line 5700 "src/y.tab.c"
    break;

  case 586: /* programtimeout: TIMEOUT NUMBER SECOND  */
#line 2157 "src/p.y"
                                        {
                        (yyval.number) = (yyvsp[-1].number) * 1000; // milliseconds internally
                  }
#line 5708 "src/y.tab.c"
    break;

  case 587: /* nettimeout: %empty  */
#line 2162 "src/p.y"
                              {
                        (yyval.number) = Run.limits.networkTimeout;
                  }
#line 5716 "src/y.tab.c"
    break;

  case 588: /* nettimeout: TIMEOUT NUMBER SECOND  */
#line 2165 "src/p.y"
                                        {
                        (yyval.number) = (yyvsp[-1].number) * 1000; // net timeout is in milliseconds internally
                  }
#line 5724 "src/y.tab.c"
    break;

  case 589: /* connectiontimeout: TIMEOUT NUMBER SECOND  */
#line 2170 "src/p.y"
                                          {
                        portset.timeout = (yyvsp[-1].number) * 1000; // timeout is in milliseconds internally
                    }
#line 5732 "src/y.tab.c"
    break;

  case 590: /* retry: RETRY NUMBER  */
#line 2175 "src/p.y"
                               {
                        portset.retry = (yyvsp[0].number);
                  }
#line 5740 "src/y.tab.c"
    break;

  case 591: /* actionrate: IF NUMBER RESTART NUMBER CYCLE THEN action1  */
#line 2180 "src/p.y"
                                                              {
                        actionrateset.count = (yyvsp[-5].number);
                        actionrateset.cycle = (yyvsp[-3].number);
                        addeventaction(&(actionrateset).action, (yyvsp[0].number), Action_Alert);
                        addactionrate(&actionrateset);
                  }
#line 5751 "src/y.tab.c"
    break;

  case 592: /* actionrate: IF NUMBER RESTART NUMBER CYCLE THEN TIMEOUT  */
#line 2186 "src/p.y"
                                                              {
                        actionrateset.count = (yyvsp[-5].number);
                        actionrateset.cycle = (yyvsp[-3].number);
                        addeventaction(&(actionrateset).action, Action_Unmonitor, Action_Alert);
                        addactionrate(&actionrateset);
                  }
#line 5762 "src/y.tab.c"
    break;

  case 593: /* urloption: CONTENT urloperator STRING  */
#line 2194 "src/p.y"
                                             {
                        seturlrequest((yyvsp[-1].number), (yyvsp[0].string));
                        FREE((yyvsp[0].string));
                  }
#line 5771 "src/y.tab.c"
    break;

  case 594: /* urloperator: EQUAL  */
#line 2200 "src/p.y"
                           { (yyval.number) = Operator_Equal; }
#line 5777 "src/y.tab.c"
    break;

  case 595: /* urloperator: NOTEQUAL  */
#line 2201 "src/p.y"
                           { (yyval.number) = Operator_NotEqual; }
#line 5783 "src/y.tab.c"
    break;

  case 596: /* alert: alertmail formatlist reminder  */
#line 2204 "src/p.y"
                                                {
                        mailset.events = Event_All;
                        addmail((yyvsp[-2].string), &mailset, &current->maillist);
                  }
#line 5792 "src/y.tab.c"
    break;

  case 597: /* alert: alertmail '{' eventoptionlist '}' formatlist reminder  */
#line 2208 "src/p.y"
                                                                        {
                        addmail((yyvsp[-5].string), &mailset, &current->maillist);
                  }
#line 5800 "src/y.tab.c"
    break;

  case 598: /* alert: alertmail NOT '{' eventoptionlist '}' formatlist reminder  */
#line 2211 "src/p.y"
                                                                            {
                        mailset.events = ~mailset.events;
                        addmail((yyvsp[-6].string), &mailset, &current->maillist);
                  }
#line 5809 "src/y.tab.c"
    break;

  case 599: /* alert: noalertmail  */
#line 2215 "src/p.y"
                              {
                        addmail((yyvsp[0].string), &mailset, &current->maillist);
                  }
#line 5817 "src/y.tab.c"
    break;

  case 600: /* alertmail: ALERT MAILADDR  */
#line 2220 "src/p.y"
                                 { (yyval.string) = (yyvsp[0].string); }
#line 5823 "src/y.tab.c"
    break;

  case 601: /* noalertmail: NOALERT MAILADDR  */
#line 2223 "src/p.y"
                                   { (yyval.string) = (yyvsp[0].string); }
#line 5829 "src/y.tab.c"
    break;

  case 604: /* eventoption: ACTION  */
#line 2230 "src/p.y"
                                  { mailset.events |= Event_Action; }
#line 5835 "src/y.tab.c"
    break;

  case 605: /* eventoption: BYTEIN  */
#line 2231 "src/p.y"
                                  { mailset.events |= Event_ByteIn; }
#line 5841 "src/y.tab.c"
    break;

  case 606: /* eventoption: BYTEOUT  */
#line 2232 "src/p.y"
                                  { mailset.events |= Event_ByteOut; }
#line 5847 "src/y.tab.c"
    break;

  case 607: /* eventoption: CHECKSUM  */
#line 2233 "src/p.y"
                                  { mailset.events |= Event_Checksum; }
#line 5853 "src/y.tab.c"
    break;

  case 608: /* eventoption: CONNECTION  */
#line 2234 "src/p.y"
                                  { mailset.events |= Event_Connection; }
#line 5859 "src/y.tab.c"
    break;

  case 609: /* eventoption: CONTENT  */
#line 2235 "src/p.y"
                                  { mailset.events |= Event_Content; }
#line 5865 "src/y.tab.c"
    break;

  case 610: /* eventoption: DATA  */
#line 2236 "src/p.y"
                                  { mailset.events |= Event_Data; }
#line 5871 "src/y.tab.c"
    break;

  case 611: /* eventoption: EXEC  */
#line 2237 "src/p.y"
                                  { mailset.events |= Event_Exec; }
#line 5877 "src/y.tab.c"
    break;

  case 612: /* eventoption: EXIST  */
#line 2238 "src/p.y"
                                  { mailset.events |= Event_Exist; }
#line 5883 "src/y.tab.c"
    break;

  case 613: /* eventoption: FSFLAG  */
#line 2239 "src/p.y"
                                  { mailset.events |= Event_FsFlag; }
#line 5889 "src/y.tab.c"
    break;

  case 614: /* eventoption: GID  */
#line 2240 "src/p.y"
                                  { mailset.events |= Event_Gid; }
#line 5895 "src/y.tab.c"
    break;

  case 615: /* eventoption: ICMP  */
#line 2241 "src/p.y"
                                  { mailset.events |= Event_Icmp; }
#line 5901 "src/y.tab.c"
    break;

  case 616: /* eventoption: INSTANCE  */
#line 2242 "src/p.y"
                                  { mailset.events |= Event_Instance; }
#line 5907 "src/y.tab.c"
    break;

  case 617: /* eventoption: INVALID  */
#line 2243 "src/p.y"
                                  { mailset.events |= Event_Invalid; }
#line 5913 "src/y.tab.c"
    break;

  case 618: /* eventoption: LINK  */
#line 2244 "src/p.y"
                                  { mailset.events |= Event_Link; }
#line 5919 "src/y.tab.c"
    break;

  case 619: /* eventoption: NONEXIST  */
#line 2245 "src/p.y"
                                  { mailset.events |= Event_NonExist; }
#line 5925 "src/y.tab.c"
    break;

  case 620: /* eventoption: PACKETIN  */
#line 2246 "src/p.y"
                                  { mailset.events |= Event_PacketIn; }
#line 5931 "src/y.tab.c"
    break;

  case 621: /* eventoption: PACKETOUT  */
#line 2247 "src/p.y"
                                  { mailset.events |= Event_PacketOut; }
#line 5937 "src/y.tab.c"
    break;

  case 622: /* eventoption: PERMISSION  */
#line 2248 "src/p.y"
                                  { mailset.events |= Event_Permission; }
#line 5943 "src/y.tab.c"
    break;

  case 623: /* eventoption: PID  */
#line 2249 "src/p.y"
                                  { mailset.events |= Event_Pid; }
#line 5949 "src/y.tab.c"
    break;

  case 624: /* eventoption: PPID  */
#line 2250 "src/p.y"
                                  { mailset.events |= Event_PPid; }
#line 5955 "src/y.tab.c"
    break;

  case 625: /* eventoption: RESOURCE  */
#line 2251 "src/p.y"
                                  { mailset.events |= Event_Resource; }
#line 5961 "src/y.tab.c"
    break;

  case 626: /* eventoption: SATURATION  */
#line 2252 "src/p.y"
                                  { mailset.events |= Event_Saturation; }
#line 5967 "src/y.tab.c"
    break;

  case 627: /* eventoption: SIZE  */
#line 2253 "src/p.y"
                                  { mailset.events |= Event_Size; }
#line 5973 "src/y.tab.c"
    break;

  case 628: /* eventoption: SPEED  */
#line 2254 "src/p.y"
                                  { mailset.events |= Event_Speed; }
#line 5979 "src/y.tab.c"
    break;

  case 629: /* eventoption: STATUS  */
#line 2255 "src/p.y"
                                  { mailset.events |= Event_Status; }
#line 5985 "src/y.tab.c"
    break;

  case 630: /* eventoption: TIMEOUT  */
#line 2256 "src/p.y"
                                  { mailset.events |= Event_Timeout; }
#line 5991 "src/y.tab.c"
    break;

  case 631: /* eventoption: TIME  */
#line 2257 "src/p.y"
                                  { mailset.events |= Event_Timestamp; }
#line 5997 "src/y.tab.c"
    break;

  case 632: /* eventoption: UID  */
#line 2258 "src/p.y"
                                  { mailset.events |= Event_Uid; }
#line 6003 "src/y.tab.c"
    break;

  case 633: /* eventoption: UPTIME  */
#line 2259 "src/p.y"
                                  { mailset.events |= Event_Uptime; }
#line 6009 "src/y.tab.c"
    break;

  case 638: /* formatoption: MAILFROM ADDRESSOBJECT  */
#line 2270 "src/p.y"
                                         { mailset.from = (yyvsp[-1].address); }
#line 6015 "src/y.tab.c"
    break;

  case 639: /* formatoption: MAILREPLYTO ADDRESSOBJECT  */
#line 2271 "src/p.y"
                                            { mailset.replyto = (yyvsp[-1].address); }
#line 6021 "src/y.tab.c"
    break;

  case 640: /* formatoption: MAILSUBJECT  */
#line 2272 "src/p.y"
                              { mailset.subject = (yyvsp[0].string); }
#line 6027 "src/y.tab.c"
    break;

  case 641: /* formatoption: MAILBODY  */
#line 2273 "src/p.y"
                           { mailset.message = (yyvsp[0].string); }
#line 6033 "src/y.tab.c"
    break;

  case 642: /* every: EVERY NUMBER CYCLE  */
#line 2276 "src/p.y"
                                     {
                        _sanityCheckEveryStatement(current);
                        current->every.type = Every_SkipCycles;
                        current->every.spec.cycle.counter = current->every.spec.cycle.number = (yyvsp[-1].number);
                 }
#line 6043 "src/y.tab.c"
    break;

  case 643: /* every: EVERY TIMESPEC  */
#line 2281 "src/p.y"
                                 {
                        _sanityCheckEveryStatement(current);
                        current->every.type = Every_Cron;
                        current->every.spec.cron = (yyvsp[0].string);
                 }
#line 6053 "src/y.tab.c"
    break;

  case 644: /* every: NOTEVERY TIMESPEC  */
#line 2286 "src/p.y"
                                    {
                        _sanityCheckEveryStatement(current);
                        current->every.type = Every_NotInCron;
                        current->every.spec.cron = (yyvsp[0].string);
                 }
#line 6063 "src/y.tab.c"
    break;

  case 645: /* mode: MODE ACTIVE  */
#line 2293 "src/p.y"
                              {
                        current->mode = Monitor_Active;
                  }
#line 6071 "src/y.tab.c"
    break;

  case 646: /* mode: MODE PASSIVE  */
#line 2296 "src/p.y"
                               {
                        current->mode = Monitor_Passive;
                  }
#line 6079 "src/y.tab.c"
    break;

  case 647: /* mode: MODE MANUAL  */
#line 2299 "src/p.y"
                              {
                        // Deprecated since monit 5.18
                        current->onreboot = Onreboot_Laststate;
                  }
#line 6088 "src/y.tab.c"
    break;

  case 648: /* onreboot: ONREBOOT START  */
#line 2305 "src/p.y"
                                 {
                        current->onreboot = Onreboot_Start;
                  }
#line 6096 "src/y.tab.c"
    break;

  case 649: /* onreboot: ONREBOOT NOSTART  */
#line 2308 "src/p.y"
                                   {
                        current->onreboot = Onreboot_Nostart;
                        current->monitor = Monitor_Not;
                  }
#line 6105 "src/y.tab.c"
    break;

  case 650: /* onreboot: ONREBOOT LASTSTATE  */
#line 2312 "src/p.y"
                                     {
                        current->onreboot = Onreboot_Laststate;
                  }
#line 6113 "src/y.tab.c"
    break;

  case 651: /* group: GROUP STRINGNAME  */
#line 2317 "src/p.y"
                                   {
                        addservicegroup((yyvsp[0].string));
                        FREE((yyvsp[0].string));
                  }
#line 6122 "src/y.tab.c"
    break;

  case 655: /* dependant: SERVICENAME  */
#line 2331 "src/p.y"
                              { adddependant((yyvsp[0].string)); }
#line 6128 "src/y.tab.c"
    break;

  case 656: /* statusvalue: IF STATUS operator NUMBER rate1 THEN action1 recovery_success  */
#line 2334 "src/p.y"
                                                                                {
                        statusset.initialized = true;
                        statusset.operator = (yyvsp[-5].number);
                        statusset.return_value = (yyvsp[-4].number);
                        addeventaction(&(statusset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addstatus(&statusset);
                   }
#line 6140 "src/y.tab.c"
    break;

  case 657: /* statusvalue: IF CHANGED STATUS rate1 THEN action1  */
#line 2341 "src/p.y"
                                                       {
                        statusset.initialized = false;
                        statusset.operator = Operator_Changed;
                        statusset.return_value = 0;
                        addeventaction(&(statusset).action, (yyvsp[0].number), Action_Ignored);
                        addstatus(&statusset);
                   }
#line 6152 "src/y.tab.c"
    break;

  case 658: /* resourceprocess: IF resourceprocesslist rate1 THEN action1 recovery_success  */
#line 2350 "src/p.y"
                                                                             {
                        addeventaction(&(resourceset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addresource(&resourceset);
                   }
#line 6161 "src/y.tab.c"
    break;

  case 668: /* resourcesystem: IF resourcesystemlist rate1 THEN action1 recovery_success  */
#line 2369 "src/p.y"
                                                                            {
                        addeventaction(&(resourceset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addresource(&resourceset);
                   }
#line 6170 "src/y.tab.c"
    break;

  case 675: /* resourcecpuproc: CPU operator value PERCENT  */
#line 2385 "src/p.y"
                                             {
                        resourceset.resource_id = Resource_CpuPercent;
                        resourceset.operator = (yyvsp[-2].number);
                        resourceset.limit = (yyvsp[-1].real);
                  }
#line 6180 "src/y.tab.c"
    break;

  case 676: /* resourcecpuproc: TOTALCPU operator value PERCENT  */
#line 2390 "src/p.y"
                                                  {
                        resourceset.resource_id = Resource_CpuPercentTotal;
                        resourceset.operator = (yyvsp[-2].number);
                        resourceset.limit = (yyvsp[-1].real);
                  }
#line 6190 "src/y.tab.c"
    break;

  case 677: /* resourcecpu: resourcecpuid operator value PERCENT  */
#line 2397 "src/p.y"
                                                       {
                        resourceset.resource_id = (yyvsp[-3].number);
                        resourceset.operator = (yyvsp[-2].number);
                        resourceset.limit = (yyvsp[-1].real);
                  }
#line 6200 "src/y.tab.c"
    break;

  case 678: /* resourcecpuid: CPUUSER  */
#line 2404 "src/p.y"
                          {
                        if (systeminfo.statisticsAvailable & Statistics_CpuUser)
                                (yyval.number) = Resource_CpuUser;
                        else
                                yywarning2("The CPU user usage statistics is not available on this system\n");
                  }
#line 6211 "src/y.tab.c"
    break;

  case 679: /* resourcecpuid: CPUSYSTEM  */
#line 2410 "src/p.y"
                            {
                        if (systeminfo.statisticsAvailable & Statistics_CpuSystem)
                                (yyval.number) = Resource_CpuSystem;
                        else
                                yywarning2("The CPU system usage statistics is not available on this system\n");
                  }
#line 6222 "src/y.tab.c"
    break;

  case 680: /* resourcecpuid: CPUWAIT  */
#line 2416 "src/p.y"
                          {
                        if (systeminfo.statisticsAvailable & Statistics_CpuIOWait)
                                (yyval.number) = Resource_CpuWait;
                        else
                                yywarning2("The CPU I/O wait usage statistics is not available on this system\n");
                  }
#line 6233 "src/y.tab.c"
    break;

  case 681: /* resourcecpuid: CPUNICE  */
#line 2422 "src/p.y"
                          {
                        if (systeminfo.statisticsAvailable & Statistics_CpuNice)
                                (yyval.number) = Resource_CpuNice;
                        else
                                yywarning2("The CPU nice usage statistics is not available on this system\n");
                  }
#line 6244 "src/y.tab.c"
    break;

  case 682: /* resourcecpuid: CPUHARDIRQ  */
#line 2428 "src/p.y"
                             {
                        if (systeminfo.statisticsAvailable & Statistics_CpuHardIRQ)
                                (yyval.number) = Resource_CpuHardIRQ;
                        else
                                yywarning2("The CPU hardware IRQ usage statistics is not available on this system\n");
                  }
#line 6255 "src/y.tab.c"
    break;

  case 683: /* resourcecpuid: CPUSOFTIRQ  */
#line 2434 "src/p.y"
                             {
                        if (systeminfo.statisticsAvailable & Statistics_CpuSoftIRQ)
                                (yyval.number) = Resource_CpuSoftIRQ;
                        else
                                yywarning2("The CPU software IRQ usage statistics is not available on this system\n");
                  }
#line 6266 "src/y.tab.c"
    break;

  case 684: /* resourcecpuid: CPUSTEAL  */
#line 2440 "src/p.y"
                           {
                        if (systeminfo.statisticsAvailable & Statistics_CpuSteal)
                                (yyval.number) = Resource_CpuSteal;
                        else
                                yywarning2("The CPU steal usage statistics is not available on this system\n");
                  }
#line 6277 "src/y.tab.c"
    break;

  case 685: /* resourcecpuid: CPUGUEST  */
#line 2446 "src/p.y"
                           {
                        if (systeminfo.statisticsAvailable & Statistics_CpuGuest)
                                (yyval.number) = Resource_CpuGuest;
                        else
                                yywarning2("The CPU guest usage statistics is not available on this system\n");
                  }
#line 6288 "src/y.tab.c"
    break;

  case 686: /* resourcecpuid: CPUGUESTNICE  */
#line 2452 "src/p.y"
                               {
                        if (systeminfo.statisticsAvailable & Statistics_CpuGuestNice)
                                (yyval.number) = Resource_CpuGuestNice;
                        else
                                yywarning2("The CPU guest nice usage statistics is not available on this system\n");
                  }
#line 6299 "src/y.tab.c"
    break;

  case 687: /* resourcecpuid: CPU  */
#line 2458 "src/p.y"
                      {
                        (yyval.number) = Resource_CpuPercent;
                  }
#line 6307 "src/y.tab.c"
    break;

  case 688: /* resourcemem: MEMORY operator value unit  */
#line 2463 "src/p.y"
                                             {
                        resourceset.resource_id = Resource_MemoryKbyte;
                        resourceset.operator = (yyvsp[-2].number);
                        resourceset.limit = (yyvsp[-1].real) * (yyvsp[0].number);
                  }
#line 6317 "src/y.tab.c"
    break;

  case 689: /* resourcemem: MEMORY operator value PERCENT  */
#line 2468 "src/p.y"
                                                {
                        resourceset.resource_id = Resource_MemoryPercent;
                        resourceset.operator = (yyvsp[-2].number);
                        resourceset.limit = (yyvsp[-1].real);
                  }
#line 6327 "src/y.tab.c"
    break;

  case 690: /* resourcememproc: MEMORY operator value unit  */
#line 2475 "src/p.y"
                                             {
                        resourceset.resource_id = Resource_MemoryKbyte;
                        resourceset.operator = (yyvsp[-2].number);
                        resourceset.limit = (yyvsp[-1].real) * (yyvsp[0].number);
                  }
#line 6337 "src/y.tab.c"
    break;

  case 691: /* resourcememproc: MEMORY operator value PERCENT  */
#line 2480 "src/p.y"
                                                {
                        resourceset.resource_id = Resource_MemoryPercent;
                        resourceset.operator = (yyvsp[-2].number);
                        resourceset.limit = (yyvsp[-1].real);
                  }
#line 6347 "src/y.tab.c"
    break;

  case 692: /* resourcememproc: TOTALMEMORY operator value unit  */
#line 2485 "src/p.y"
                                                  {
                        resourceset.resource_id = Resource_MemoryKbyteTotal;
                        resourceset.operator = (yyvsp[-2].number);
                        resourceset.limit = (yyvsp[-1].real) * (yyvsp[0].number);
                  }
#line 6357 "src/y.tab.c"
    break;

  case 693: /* resourcememproc: TOTALMEMORY operator value PERCENT  */
#line 2490 "src/p.y"
                                                      {
                        resourceset.resource_id = Resource_MemoryPercentTotal;
                        resourceset.operator = (yyvsp[-2].number);
                        resourceset.limit = (yyvsp[-1].real);
                  }
#line 6367 "src/y.tab.c"
    break;

  case 694: /* resourceswap: SWAP operator value unit  */
#line 2497 "src/p.y"
                                           {
                        resourceset.resource_id = Resource_SwapKbyte;
                        resourceset.operator = (yyvsp[-2].number);
                        resourceset.limit = (yyvsp[-1].real) * (yyvsp[0].number);
                  }
#line 6377 "src/y.tab.c"
    break;

  case 695: /* resourceswap: SWAP operator value PERCENT  */
#line 2502 "src/p.y"
                                              {
                        resourceset.resource_id = Resource_SwapPercent;
                        resourceset.operator = (yyvsp[-2].number);
                        resourceset.limit = (yyvsp[-1].real);
                  }
#line 6387 "src/y.tab.c"
    break;

  case 696: /* resourcethreads: THREADS operator NUMBER  */
#line 2509 "src/p.y"
                                          {
                        resourceset.resource_id = Resource_Threads;
                        resourceset.operator = (yyvsp[-1].number);
                        resourceset.limit = (yyvsp[0].number);
                  }
#line 6397 "src/y.tab.c"
    break;

  case 697: /* resourcechild: CHILDREN operator NUMBER  */
#line 2516 "src/p.y"
                                           {
                        resourceset.resource_id = Resource_Children;
                        resourceset.operator = (yyvsp[-1].number);
                        resourceset.limit = (yyvsp[0].number);
                  }
#line 6407 "src/y.tab.c"
    break;

  case 698: /* resourceload: resourceloadavg coremultiplier operator value  */
#line 2523 "src/p.y"
                                                                {
                        switch ((yyvsp[-3].number)) {
                                case Resource_LoadAverage1m:
                                        resourceset.resource_id = (yyvsp[-2].number) > 1 ? Resource_LoadAveragePerCore1m : (yyvsp[-3].number);
                                        break;
                                case Resource_LoadAverage5m:
                                        resourceset.resource_id = (yyvsp[-2].number) > 1 ? Resource_LoadAveragePerCore5m : (yyvsp[-3].number);
                                        break;
                                case Resource_LoadAverage15m:
                                        resourceset.resource_id = (yyvsp[-2].number) > 1 ? Resource_LoadAveragePerCore15m : (yyvsp[-3].number);
                                        break;
                                default:
                                        resourceset.resource_id = (yyvsp[-3].number);
                                        break;
                        }
                        resourceset.operator = (yyvsp[-1].number);
                        resourceset.limit = (yyvsp[0].real);
                  }
#line 6430 "src/y.tab.c"
    break;

  case 699: /* resourceloadavg: LOADAVG1  */
#line 2543 "src/p.y"
                            { (yyval.number) = Resource_LoadAverage1m; }
#line 6436 "src/y.tab.c"
    break;

  case 700: /* resourceloadavg: LOADAVG5  */
#line 2544 "src/p.y"
                            { (yyval.number) = Resource_LoadAverage5m; }
#line 6442 "src/y.tab.c"
    break;

  case 701: /* resourceloadavg: LOADAVG15  */
#line 2545 "src/p.y"
                            { (yyval.number) = Resource_LoadAverage15m; }
#line 6448 "src/y.tab.c"
    break;

  case 702: /* coremultiplier: %empty  */
#line 2548 "src/p.y"
                              { (yyval.number) = 1; }
#line 6454 "src/y.tab.c"
    break;

  case 703: /* coremultiplier: CORE  */
#line 2549 "src/p.y"
                              { (yyval.number) = systeminfo.cpu.count; }
#line 6460 "src/y.tab.c"
    break;

  case 704: /* resourceread: READ operator value unit currenttime  */
#line 2553 "src/p.y"
                                                       {
                        resourceset.resource_id = Resource_ReadBytes;
                        resourceset.operator = (yyvsp[-3].number);
                        resourceset.limit = (yyvsp[-2].real) * (yyvsp[-1].number);
                  }
#line 6470 "src/y.tab.c"
    break;

  case 705: /* resourceread: DISK READ operator value unit currenttime  */
#line 2558 "src/p.y"
                                                            {
                        resourceset.resource_id = Resource_ReadBytesPhysical;
                        resourceset.operator = (yyvsp[-3].number);
                        resourceset.limit = (yyvsp[-2].real) * (yyvsp[-1].number);
                  }
#line 6480 "src/y.tab.c"
    break;

  case 706: /* resourceread: DISK READ operator NUMBER OPERATION  */
#line 2563 "src/p.y"
                                                      {
                        resourceset.resource_id = Resource_ReadOperations;
                        resourceset.operator = (yyvsp[-2].number);
                        resourceset.limit = (yyvsp[-1].number);
                  }
#line 6490 "src/y.tab.c"
    break;

  case 707: /* resourcewrite: WRITE operator value unit currenttime  */
#line 2570 "src/p.y"
                                                        {
                        resourceset.resource_id = Resource_WriteBytes;
                        resourceset.operator = (yyvsp[-3].number);
                        resourceset.limit = (yyvsp[-2].real) * (yyvsp[-1].number);
                  }
#line 6500 "src/y.tab.c"
    break;

  case 708: /* resourcewrite: DISK WRITE operator value unit currenttime  */
#line 2575 "src/p.y"
                                                             {
                        resourceset.resource_id = Resource_WriteBytesPhysical;
                        resourceset.operator = (yyvsp[-3].number);
                        resourceset.limit = (yyvsp[-2].real) * (yyvsp[-1].number);
                  }
#line 6510 "src/y.tab.c"
    break;

  case 709: /* resourcewrite: DISK WRITE operator NUMBER OPERATION  */
#line 2580 "src/p.y"
                                                       {
                        resourceset.resource_id = Resource_WriteOperations;
                        resourceset.operator = (yyvsp[-2].number);
                        resourceset.limit = (yyvsp[-1].number);
                  }
#line 6520 "src/y.tab.c"
    break;

  case 710: /* value: REAL  */
#line 2587 "src/p.y"
                       { (yyval.real) = (yyvsp[0].real); }
#line 6526 "src/y.tab.c"
    break;

  case 711: /* value: NUMBER  */
#line 2588 "src/p.y"
                         { (yyval.real) = (float) (yyvsp[0].number); }
#line 6532 "src/y.tab.c"
    break;

  case 712: /* timestamptype: TIME  */
#line 2591 "src/p.y"
                        { (yyval.number) = Timestamp_Default; }
#line 6538 "src/y.tab.c"
    break;

  case 713: /* timestamptype: ATIME  */
#line 2592 "src/p.y"
                        { (yyval.number) = Timestamp_Access; }
#line 6544 "src/y.tab.c"
    break;

  case 714: /* timestamptype: CTIME  */
#line 2593 "src/p.y"
                        { (yyval.number) = Timestamp_Change; }
#line 6550 "src/y.tab.c"
    break;

  case 715: /* timestamptype: MTIME  */
#line 2594 "src/p.y"
                        { (yyval.number) = Timestamp_Modification; }
#line 6556 "src/y.tab.c"
    break;

  case 716: /* timestamp: IF timestamptype operator NUMBER time rate1 THEN action1 recovery_success  */
#line 2597 "src/p.y"
                                                                                            {
                        timestampset.type = (yyvsp[-7].number);
                        timestampset.operator = (yyvsp[-6].number);
                        timestampset.time = ((yyvsp[-5].number) * (yyvsp[-4].number));
                        addeventaction(&(timestampset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addtimestamp(&timestampset);
                  }
#line 6568 "src/y.tab.c"
    break;

  case 717: /* timestamp: IF CHANGED timestamptype rate1 THEN action1  */
#line 2604 "src/p.y"
                                                              {
                        timestampset.type = (yyvsp[-3].number);
                        timestampset.test_changes = true;
                        addeventaction(&(timestampset).action, (yyvsp[0].number), Action_Ignored);
                        addtimestamp(&timestampset);
                  }
#line 6579 "src/y.tab.c"
    break;

  case 718: /* operator: %empty  */
#line 2612 "src/p.y"
                                 { (yyval.number) = Operator_Equal; }
#line 6585 "src/y.tab.c"
    break;

  case 719: /* operator: GREATER  */
#line 2613 "src/p.y"
                                 { (yyval.number) = Operator_Greater; }
#line 6591 "src/y.tab.c"
    break;

  case 720: /* operator: GREATEROREQUAL  */
#line 2614 "src/p.y"
                                 { (yyval.number) = Operator_GreaterOrEqual; }
#line 6597 "src/y.tab.c"
    break;

  case 721: /* operator: LESS  */
#line 2615 "src/p.y"
                                 { (yyval.number) = Operator_Less; }
#line 6603 "src/y.tab.c"
    break;

  case 722: /* operator: LESSOREQUAL  */
#line 2616 "src/p.y"
                                 { (yyval.number) = Operator_LessOrEqual; }
#line 6609 "src/y.tab.c"
    break;

  case 723: /* operator: EQUAL  */
#line 2617 "src/p.y"
                                 { (yyval.number) = Operator_Equal; }
#line 6615 "src/y.tab.c"
    break;

  case 724: /* operator: NOTEQUAL  */
#line 2618 "src/p.y"
                                 { (yyval.number) = Operator_NotEqual; }
#line 6621 "src/y.tab.c"
    break;

  case 725: /* operator: CHANGED  */
#line 2619 "src/p.y"
                                 { (yyval.number) = Operator_Changed; }
#line 6627 "src/y.tab.c"
    break;

  case 726: /* time: %empty  */
#line 2622 "src/p.y"
                              { (yyval.number) = Time_Second; }
#line 6633 "src/y.tab.c"
    break;

  case 727: /* time: SECOND  */
#line 2623 "src/p.y"
                              { (yyval.number) = Time_Second; }
#line 6639 "src/y.tab.c"
    break;

  case 728: /* time: MINUTE  */
#line 2624 "src/p.y"
                              { (yyval.number) = Time_Minute; }
#line 6645 "src/y.tab.c"
    break;

  case 729: /* time: HOUR  */
#line 2625 "src/p.y"
                              { (yyval.number) = Time_Hour; }
#line 6651 "src/y.tab.c"
    break;

  case 730: /* time: DAY  */
#line 2626 "src/p.y"
                              { (yyval.number) = Time_Day; }
#line 6657 "src/y.tab.c"
    break;

  case 731: /* time: MONTH  */
#line 2627 "src/p.y"
                              { (yyval.number) = Time_Month; }
#line 6663 "src/y.tab.c"
    break;

  case 732: /* totaltime: MINUTE  */
#line 2630 "src/p.y"
                              { (yyval.number) = Time_Minute; }
#line 6669 "src/y.tab.c"
    break;

  case 733: /* totaltime: HOUR  */
#line 2631 "src/p.y"
                              { (yyval.number) = Time_Hour; }
#line 6675 "src/y.tab.c"
    break;

  case 734: /* totaltime: DAY  */
#line 2632 "src/p.y"
                              { (yyval.number) = Time_Day; }
#line 6681 "src/y.tab.c"
    break;

  case 735: /* currenttime: %empty  */
#line 2634 "src/p.y"
                              { (yyval.number) = Time_Second; }
#line 6687 "src/y.tab.c"
    break;

  case 736: /* currenttime: SECOND  */
#line 2635 "src/p.y"
                              { (yyval.number) = Time_Second; }
#line 6693 "src/y.tab.c"
    break;

  case 737: /* repeat: %empty  */
#line 2637 "src/p.y"
                              {
                        repeat = 0;
                  }
#line 6701 "src/y.tab.c"
    break;

  case 738: /* repeat: REPEAT EVERY CYCLE  */
#line 2640 "src/p.y"
                                     {
                        repeat = 1;
                  }
#line 6709 "src/y.tab.c"
    break;

  case 739: /* repeat: REPEAT EVERY NUMBER CYCLE  */
#line 2643 "src/p.y"
                                            {
                        if ((yyvsp[-1].number) < 0) {
                                yyerror2("The number of repeat cycles must be greater or equal to 0");
                        }
                        repeat = (yyvsp[-1].number);
                  }
#line 6720 "src/y.tab.c"
    break;

  case 740: /* action: ALERT  */
#line 2651 "src/p.y"
                        {
                        (yyval.number) = Action_Alert;
                  }
#line 6728 "src/y.tab.c"
    break;

  case 741: /* action: EXEC argumentlist repeat  */
#line 2654 "src/p.y"
                                           {
                        (yyval.number) = Action_Exec;
                  }
#line 6736 "src/y.tab.c"
    break;

  case 742: /* action: EXEC argumentlist useroptionlist repeat  */
#line 2658 "src/p.y"
                  {
                        (yyval.number) = Action_Exec;
                  }
#line 6744 "src/y.tab.c"
    break;

  case 743: /* action: RESTART  */
#line 2661 "src/p.y"
                          {
                        (yyval.number) = Action_Restart;
                  }
#line 6752 "src/y.tab.c"
    break;

  case 744: /* action: START  */
#line 2664 "src/p.y"
                        {
                        (yyval.number) = Action_Start;
                  }
#line 6760 "src/y.tab.c"
    break;

  case 745: /* action: STOP  */
#line 2667 "src/p.y"
                       {
                        (yyval.number) = Action_Stop;
                  }
#line 6768 "src/y.tab.c"
    break;

  case 746: /* action: UNMONITOR  */
#line 2670 "src/p.y"
                            {
                        (yyval.number) = Action_Unmonitor;
                  }
#line 6776 "src/y.tab.c"
    break;

  case 747: /* action1: action  */
#line 2675 "src/p.y"
                         {
                        (yyval.number) = (yyvsp[0].number);
                        if ((yyvsp[0].number) == Action_Exec && command) {
                                repeat1 = repeat;
                                repeat = 0;
                                command1 = command;
                                command = NULL;
                        }
                  }
#line 6790 "src/y.tab.c"
    break;

  case 748: /* action2: action  */
#line 2686 "src/p.y"
                         {
                        (yyval.number) = (yyvsp[0].number);
                        if ((yyvsp[0].number) == Action_Exec && command) {
                                repeat2 = repeat;
                                repeat = 0;
                                command2 = command;
                                command = NULL;
                        }
                  }
#line 6804 "src/y.tab.c"
    break;

  case 749: /* rateXcycles: NUMBER CYCLE  */
#line 2697 "src/p.y"
                               {
                        if ((yyvsp[-1].number) < 1 || (unsigned long)(yyvsp[-1].number) > BITMAP_MAX) {
                                yyerror2("The number of cycles must be between 1 and %zu", BITMAP_MAX);
                        } else {
                                rate.count  = (yyvsp[-1].number);
                                rate.cycles = (yyvsp[-1].number);
                        }
                  }
#line 6817 "src/y.tab.c"
    break;

  case 750: /* rateXYcycles: NUMBER NUMBER CYCLE  */
#line 2707 "src/p.y"
                                      {
                        if ((yyvsp[-1].number) < 1 || (unsigned long)(yyvsp[-1].number) > BITMAP_MAX) {
                                yyerror2("The number of cycles must be between 1 and %zu", BITMAP_MAX);
                        } else if ((yyvsp[-2].number) < 1 || (yyvsp[-2].number) > (yyvsp[-1].number)) {
                                yyerror2("The number of events must be between 1 and less then poll cycles");
                        } else {
                                rate.count  = (yyvsp[-2].number);
                                rate.cycles = (yyvsp[-1].number);
                        }
                  }
#line 6832 "src/y.tab.c"
    break;

  case 752: /* rate1: rateXcycles  */
#line 2720 "src/p.y"
                              {
                        rate1.count = rate.count;
                        rate1.cycles = rate.cycles;
                        reset_rateset(&rate);
                  }
#line 6842 "src/y.tab.c"
    break;

  case 753: /* rate1: rateXYcycles  */
#line 2725 "src/p.y"
                               {
                        rate1.count = rate.count;
                        rate1.cycles = rate.cycles;
                        reset_rateset(&rate);
                }
#line 6852 "src/y.tab.c"
    break;

  case 755: /* rate2: rateXcycles  */
#line 2733 "src/p.y"
                              {
                        rate2.count = rate.count;
                        rate2.cycles = rate.cycles;
                        reset_rateset(&rate);
                  }
#line 6862 "src/y.tab.c"
    break;

  case 756: /* rate2: rateXYcycles  */
#line 2738 "src/p.y"
                               {
                        rate2.count = rate.count;
                        rate2.cycles = rate.cycles;
                        reset_rateset(&rate);
                }
#line 6872 "src/y.tab.c"
    break;

  case 757: /* recovery_success: %empty  */
#line 2745 "src/p.y"
                               {
                        (yyval.number) = Action_Alert;
                  }
#line 6880 "src/y.tab.c"
    break;

  case 758: /* recovery_success: ELSE action2  */
#line 2748 "src/p.y"
                               {
                        (yyval.number) = (yyvsp[0].number);
                  }
#line 6888 "src/y.tab.c"
    break;

  case 759: /* recovery_success: ELSE IF RECOVERED rate2 THEN action2  */
#line 2751 "src/p.y"
                                                       {
                        (yyval.number) = (yyvsp[0].number);
                  }
#line 6896 "src/y.tab.c"
    break;

  case 760: /* recovery_success: ELSE IF PASSED rate2 THEN action2  */
#line 2754 "src/p.y"
                                                    {
                        (yyval.number) = (yyvsp[0].number);
                  }
#line 6904 "src/y.tab.c"
    break;

  case 761: /* recovery_success: ELSE IF SUCCEEDED rate2 THEN action2  */
#line 2757 "src/p.y"
                                                       {
                        (yyval.number) = (yyvsp[0].number);
                  }
#line 6912 "src/y.tab.c"
    break;

  case 762: /* recovery_failure: %empty  */
#line 2762 "src/p.y"
                               {
                        (yyval.number) = Action_Alert;
                  }
#line 6920 "src/y.tab.c"
    break;

  case 763: /* recovery_failure: ELSE action2  */
#line 2765 "src/p.y"
                               {
                        (yyval.number) = (yyvsp[0].number);
                  }
#line 6928 "src/y.tab.c"
    break;

  case 764: /* recovery_failure: ELSE IF FAILED rate2 THEN action2  */
#line 2768 "src/p.y"
                                                    {
                        (yyval.number) = (yyvsp[0].number);
                  }
#line 6936 "src/y.tab.c"
    break;

  case 765: /* checksum: IF FAILED hashtype CHECKSUM rate1 THEN action1 recovery_success  */
#line 2773 "src/p.y"
                                                                                  {
                        addeventaction(&(checksumset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addchecksum(&checksumset);
                  }
#line 6945 "src/y.tab.c"
    break;

  case 766: /* checksum: IF FAILED hashtype CHECKSUM EXPECT STRING rate1 THEN action1 recovery_success  */
#line 2778 "src/p.y"
                                   {
                        snprintf(checksumset.hash, sizeof(checksumset.hash), "%s", (yyvsp[-4].string));
                        FREE((yyvsp[-4].string));
                        addeventaction(&(checksumset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addchecksum(&checksumset);
                  }
#line 6956 "src/y.tab.c"
    break;

  case 767: /* checksum: IF CHANGED hashtype CHECKSUM rate1 THEN action1  */
#line 2784 "src/p.y"
                                                                  {
                        checksumset.test_changes = true;
                        addeventaction(&(checksumset).action, (yyvsp[0].number), Action_Ignored);
                        addchecksum(&checksumset);
                  }
#line 6966 "src/y.tab.c"
    break;

  case 768: /* hashtype: %empty  */
#line 2790 "src/p.y"
                              { checksumset.type = Hash_Unknown; }
#line 6972 "src/y.tab.c"
    break;

  case 769: /* hashtype: MD5HASH  */
#line 2791 "src/p.y"
                              { checksumset.type = Hash_Md5; }
#line 6978 "src/y.tab.c"
    break;

  case 770: /* hashtype: SHA1HASH  */
#line 2792 "src/p.y"
                              { checksumset.type = Hash_Sha1; }
#line 6984 "src/y.tab.c"
    break;

  case 771: /* inode: IF INODE operator NUMBER rate1 THEN action1 recovery_success  */
#line 2795 "src/p.y"
                                                                               {
                        filesystemset.resource = Resource_Inode;
                        filesystemset.operator = (yyvsp[-5].number);
                        filesystemset.limit_absolute = (yyvsp[-4].number);
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 6996 "src/y.tab.c"
    break;

  case 772: /* inode: IF INODE operator value PERCENT rate1 THEN action1 recovery_success  */
#line 2802 "src/p.y"
                                                                                      {
                        filesystemset.resource = Resource_Inode;
                        filesystemset.operator = (yyvsp[-6].number);
                        filesystemset.limit_percent = (yyvsp[-5].real);
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 7008 "src/y.tab.c"
    break;

  case 773: /* inode: IF INODE TFREE operator NUMBER rate1 THEN action1 recovery_success  */
#line 2809 "src/p.y"
                                                                                     {
                        filesystemset.resource = Resource_InodeFree;
                        filesystemset.operator = (yyvsp[-5].number);
                        filesystemset.limit_absolute = (yyvsp[-4].number);
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 7020 "src/y.tab.c"
    break;

  case 774: /* inode: IF INODE TFREE operator value PERCENT rate1 THEN action1 recovery_success  */
#line 2816 "src/p.y"
                                                                                            {
                        filesystemset.resource = Resource_InodeFree;
                        filesystemset.operator = (yyvsp[-6].number);
                        filesystemset.limit_percent = (yyvsp[-5].real);
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 7032 "src/y.tab.c"
    break;

  case 775: /* space: IF SPACE operator value unit rate1 THEN action1 recovery_success  */
#line 2825 "src/p.y"
                                                                                   {
                        filesystemset.resource = Resource_Space;
                        filesystemset.operator = (yyvsp[-6].number);
                        filesystemset.limit_absolute = (yyvsp[-5].real) * (yyvsp[-4].number);
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 7044 "src/y.tab.c"
    break;

  case 776: /* space: IF SPACE operator value PERCENT rate1 THEN action1 recovery_success  */
#line 2832 "src/p.y"
                                                                                      {
                        filesystemset.resource = Resource_Space;
                        filesystemset.operator = (yyvsp[-6].number);
                        filesystemset.limit_percent = (yyvsp[-5].real);
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 7056 "src/y.tab.c"
    break;

  case 777: /* space: IF SPACE TFREE operator value unit rate1 THEN action1 recovery_success  */
#line 2839 "src/p.y"
                                                                                         {
                        filesystemset.resource = Resource_SpaceFree;
                        filesystemset.operator = (yyvsp[-6].number);
                        filesystemset.limit_absolute = (yyvsp[-5].real) * (yyvsp[-4].number);
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 7068 "src/y.tab.c"
    break;

  case 778: /* space: IF SPACE TFREE operator value PERCENT rate1 THEN action1 recovery_success  */
#line 2846 "src/p.y"
                                                                                            {
                        filesystemset.resource = Resource_SpaceFree;
                        filesystemset.operator = (yyvsp[-6].number);
                        filesystemset.limit_percent = (yyvsp[-5].real);
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 7080 "src/y.tab.c"
    break;

  case 779: /* read: IF READ operator value unit currenttime rate1 THEN action1 recovery_success  */
#line 2855 "src/p.y"
                                                                                              {
                        filesystemset.resource = Resource_ReadBytes;
                        filesystemset.operator = (yyvsp[-7].number);
                        filesystemset.limit_absolute = (yyvsp[-6].real) * (yyvsp[-5].number);
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 7092 "src/y.tab.c"
    break;

  case 780: /* read: IF READ operator NUMBER OPERATION rate1 THEN action1 recovery_success  */
#line 2862 "src/p.y"
                                                                                        {
                        filesystemset.resource = Resource_ReadOperations;
                        filesystemset.operator = (yyvsp[-6].number);
                        filesystemset.limit_absolute = (yyvsp[-5].number);
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 7104 "src/y.tab.c"
    break;

  case 781: /* write: IF WRITE operator value unit currenttime rate1 THEN action1 recovery_success  */
#line 2871 "src/p.y"
                                                                                               {
                        filesystemset.resource = Resource_WriteBytes;
                        filesystemset.operator = (yyvsp[-7].number);
                        filesystemset.limit_absolute = (yyvsp[-6].real) * (yyvsp[-5].number);
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 7116 "src/y.tab.c"
    break;

  case 782: /* write: IF WRITE operator NUMBER OPERATION rate1 THEN action1 recovery_success  */
#line 2878 "src/p.y"
                                                                                         {
                        filesystemset.resource = Resource_WriteOperations;
                        filesystemset.operator = (yyvsp[-6].number);
                        filesystemset.limit_absolute = (yyvsp[-5].number);
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 7128 "src/y.tab.c"
    break;

  case 783: /* servicetime: IF SERVICETIME operator NUMBER MILLISECOND rate1 THEN action1 recovery_success  */
#line 2887 "src/p.y"
                                                                                                 {
                        filesystemset.resource = Resource_ServiceTime;
                        filesystemset.operator = (yyvsp[-6].number);
                        filesystemset.limit_absolute = (yyvsp[-5].number);
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 7140 "src/y.tab.c"
    break;

  case 784: /* servicetime: IF SERVICETIME operator value SECOND rate1 THEN action1 recovery_success  */
#line 2894 "src/p.y"
                                                                                           {
                        filesystemset.resource = Resource_ServiceTime;
                        filesystemset.operator = (yyvsp[-6].number);
                        filesystemset.limit_absolute = (yyvsp[-5].real) * 1000;
                        addeventaction(&(filesystemset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addfilesystem(&filesystemset);
                  }
#line 7152 "src/y.tab.c"
    break;

  case 785: /* fsflag: IF CHANGED FSFLAG rate1 THEN action1  */
#line 2903 "src/p.y"
                                                       {
                        addeventaction(&(fsflagset).action, (yyvsp[0].number), Action_Ignored);
                        addfsflag(&fsflagset);
                  }
#line 7161 "src/y.tab.c"
    break;

  case 786: /* unit: %empty  */
#line 2909 "src/p.y"
                               { (yyval.number) = Unit_Byte; }
#line 7167 "src/y.tab.c"
    break;

  case 787: /* unit: BYTE  */
#line 2910 "src/p.y"
                               { (yyval.number) = Unit_Byte; }
#line 7173 "src/y.tab.c"
    break;

  case 788: /* unit: KILOBYTE  */
#line 2911 "src/p.y"
                               { (yyval.number) = Unit_Kilobyte; }
#line 7179 "src/y.tab.c"
    break;

  case 789: /* unit: MEGABYTE  */
#line 2912 "src/p.y"
                               { (yyval.number) = Unit_Megabyte; }
#line 7185 "src/y.tab.c"
    break;

  case 790: /* unit: GIGABYTE  */
#line 2913 "src/p.y"
                               { (yyval.number) = Unit_Gigabyte; }
#line 7191 "src/y.tab.c"
    break;

  case 791: /* permission: IF FAILED PERMISSION NUMBER rate1 THEN action1 recovery_success  */
#line 2916 "src/p.y"
                                                                                  {
                        permset.perm = check_perm((yyvsp[-4].number));
                        addeventaction(&(permset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addperm(&permset);
                  }
#line 7201 "src/y.tab.c"
    break;

  case 792: /* permission: IF CHANGED PERMISSION rate1 THEN action1 recovery_success  */
#line 2921 "src/p.y"
                                                                            {
                        permset.test_changes = true;
                        addeventaction(&(permset).action, (yyvsp[-1].number), Action_Ignored);
                        addperm(&permset);
                  }
#line 7211 "src/y.tab.c"
    break;

  case 793: /* programmatch: IF CONTENT urloperator STRING rate1 THEN action1  */
#line 2928 "src/p.y"
                                                                   {
                        matchset.not = (yyvsp[-4].number) == Operator_Equal ? false : true;
                        matchset.ignore = false;
                        matchset.match_path = NULL;
                        matchset.match_string = (yyvsp[-3].string);
                        addmatch(&matchset, (yyvsp[0].number), 0);
                  }
#line 7223 "src/y.tab.c"
    break;

  case 794: /* match: IF CONTENT urloperator PATH rate1 THEN action1  */
#line 2937 "src/p.y"
                                                                 {
                        matchset.not = (yyvsp[-4].number) == Operator_Equal ? false : true;
                        matchset.ignore = false;
                        matchset.match_path = (yyvsp[-3].string);
                        matchset.match_string = NULL;
                        addmatchpath(&matchset, (yyvsp[0].number));
                        FREE((yyvsp[-3].string));
                  }
#line 7236 "src/y.tab.c"
    break;

  case 795: /* match: IF CONTENT urloperator STRING rate1 THEN action1  */
#line 2945 "src/p.y"
                                                                   {
                        matchset.not = (yyvsp[-4].number) == Operator_Equal ? false : true;
                        matchset.ignore = false;
                        matchset.match_path = NULL;
                        matchset.match_string = (yyvsp[-3].string);
                        addmatch(&matchset, (yyvsp[0].number), 0);
                  }
#line 7248 "src/y.tab.c"
    break;

  case 796: /* match: IGNORE CONTENT urloperator PATH  */
#line 2952 "src/p.y"
                                                  {
                        matchset.not = (yyvsp[-1].number) == Operator_Equal ? false : true;
                        matchset.ignore = true;
                        matchset.match_path = (yyvsp[0].string);
                        matchset.match_string = NULL;
                        addmatchpath(&matchset, Action_Ignored);
                        FREE((yyvsp[0].string));
                  }
#line 7261 "src/y.tab.c"
    break;

  case 797: /* match: IGNORE CONTENT urloperator STRING  */
#line 2960 "src/p.y"
                                                    {
                        matchset.not = (yyvsp[-1].number) == Operator_Equal ? false : true;
                        matchset.ignore = true;
                        matchset.match_path = NULL;
                        matchset.match_string = (yyvsp[0].string);
                        addmatch(&matchset, Action_Ignored, 0);
                  }
#line 7273 "src/y.tab.c"
    break;

  case 798: /* match: IF matchflagnot MATCH PATH rate1 THEN action1  */
#line 2968 "src/p.y"
                                                                {
                        matchset.ignore = false;
                        matchset.match_path = (yyvsp[-3].string);
                        matchset.match_string = NULL;
                        addmatchpath(&matchset, (yyvsp[0].number));
                        FREE((yyvsp[-3].string));
                  }
#line 7285 "src/y.tab.c"
    break;

  case 799: /* match: IF matchflagnot MATCH STRING rate1 THEN action1  */
#line 2975 "src/p.y"
                                                                  {
                        matchset.ignore = false;
                        matchset.match_path = NULL;
                        matchset.match_string = (yyvsp[-3].string);
                        addmatch(&matchset, (yyvsp[0].number), 0);
                  }
#line 7296 "src/y.tab.c"
    break;

  case 800: /* match: IGNORE matchflagnot MATCH PATH  */
#line 2981 "src/p.y"
                                                 {
                        matchset.ignore = true;
                        matchset.match_path = (yyvsp[0].string);
                        matchset.match_string = NULL;
                        addmatchpath(&matchset, Action_Ignored);
                        FREE((yyvsp[0].string));
                  }
#line 7308 "src/y.tab.c"
    break;

  case 801: /* match: IGNORE matchflagnot MATCH STRING  */
#line 2988 "src/p.y"
                                                   {
                        matchset.ignore = true;
                        matchset.match_path = NULL;
                        matchset.match_string = (yyvsp[0].string);
                        addmatch(&matchset, Action_Ignored, 0);
                  }
#line 7319 "src/y.tab.c"
    break;

  case 802: /* matchflagnot: %empty  */
#line 2996 "src/p.y"
                              {
                        matchset.not = false;
                  }
#line 7327 "src/y.tab.c"
    break;

  case 803: /* matchflagnot: NOT  */
#line 2999 "src/p.y"
                      {
                        matchset.not = true;
                  }
#line 7335 "src/y.tab.c"
    break;

  case 804: /* size: IF SIZE operator NUMBER unit rate1 THEN action1 recovery_success  */
#line 3005 "src/p.y"
                                                                                   {
                        sizeset.operator = (yyvsp[-6].number);
                        sizeset.size = ((unsigned long long)(yyvsp[-5].number) * (yyvsp[-4].number));
                        addeventaction(&(sizeset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addsize(&sizeset);
                  }
#line 7346 "src/y.tab.c"
    break;

  case 805: /* size: IF CHANGED SIZE rate1 THEN action1  */
#line 3011 "src/p.y"
                                                     {
                        sizeset.test_changes = true;
                        addeventaction(&(sizeset).action, (yyvsp[0].number), Action_Ignored);
                        addsize(&sizeset);
                  }
#line 7356 "src/y.tab.c"
    break;

  case 806: /* uid: IF FAILED UID STRING rate1 THEN action1 recovery_success  */
#line 3018 "src/p.y"
                                                                           {
                        uidset.uid = get_uid((yyvsp[-4].string), 0);
                        addeventaction(&(uidset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        current->uid = adduid(&uidset);
                        FREE((yyvsp[-4].string));
                  }
#line 7367 "src/y.tab.c"
    break;

  case 807: /* uid: IF FAILED UID NUMBER rate1 THEN action1 recovery_success  */
#line 3024 "src/p.y"
                                                                           {
                    uidset.uid = get_uid(NULL, (yyvsp[-4].number));
                    addeventaction(&(uidset).action, (yyvsp[-1].number), (yyvsp[0].number));
                    current->uid = adduid(&uidset);
                  }
#line 7377 "src/y.tab.c"
    break;

  case 808: /* euid: IF FAILED EUID STRING rate1 THEN action1 recovery_success  */
#line 3031 "src/p.y"
                                                                            {
                        uidset.uid = get_uid((yyvsp[-4].string), 0);
                        addeventaction(&(uidset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        current->euid = adduid(&uidset);
                        FREE((yyvsp[-4].string));
                  }
#line 7388 "src/y.tab.c"
    break;

  case 809: /* euid: IF FAILED EUID NUMBER rate1 THEN action1 recovery_success  */
#line 3037 "src/p.y"
                                                                            {
                        uidset.uid = get_uid(NULL, (yyvsp[-4].number));
                        addeventaction(&(uidset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        current->euid = adduid(&uidset);
                  }
#line 7398 "src/y.tab.c"
    break;

  case 810: /* secattr: IF FAILED SECURITY ATTRIBUTE STRING rate1 THEN action1 recovery_success  */
#line 3044 "src/p.y"
                                                                                          {
                        addsecurityattribute((yyvsp[-4].string), (yyvsp[-1].number), (yyvsp[0].number));
                  }
#line 7406 "src/y.tab.c"
    break;

  case 811: /* secattr: IF FAILED SECURITY ATTRIBUTE PATH rate1 THEN action1 recovery_success  */
#line 3047 "src/p.y"
                                                                                        {
                        addsecurityattribute((yyvsp[-4].string), (yyvsp[-1].number), (yyvsp[0].number));
                  }
#line 7414 "src/y.tab.c"
    break;

  case 812: /* filedescriptorssystem: IF FILEDESCRIPTORS operator NUMBER rate1 THEN action1 recovery_success  */
#line 3052 "src/p.y"
                                                                                               {
                        if (systeminfo.statisticsAvailable & Statistics_FiledescriptorsPerSystem)
                                addfiledescriptors((yyvsp[-5].number), false, (long long)(yyvsp[-4].number), -1., (yyvsp[-1].number), (yyvsp[0].number));
                        else
                                yywarning("The per-system filedescriptors statistics is not available on this system\n");
                  }
#line 7425 "src/y.tab.c"
    break;

  case 813: /* filedescriptorssystem: IF FILEDESCRIPTORS operator value PERCENT rate1 THEN action1 recovery_success  */
#line 3058 "src/p.y"
                                                                                                {
                        if (systeminfo.statisticsAvailable & Statistics_FiledescriptorsPerSystem)
                                addfiledescriptors((yyvsp[-6].number), false, -1LL, (yyvsp[-5].real), (yyvsp[-1].number), (yyvsp[0].number));
                        else
                                yywarning("The per-system filedescriptors statistics is not available on this system\n");
                  }
#line 7436 "src/y.tab.c"
    break;

  case 814: /* filedescriptorsprocess: IF FILEDESCRIPTORS operator NUMBER rate1 THEN action1 recovery_success  */
#line 3066 "src/p.y"
                                                                                                {
                        if (systeminfo.statisticsAvailable & Statistics_FiledescriptorsPerProcess)
                                addfiledescriptors((yyvsp[-5].number), false, (long long)(yyvsp[-4].number), -1., (yyvsp[-1].number), (yyvsp[0].number));
                        else
                                yywarning("The per-process filedescriptors statistics is not available on this system\n");
                  }
#line 7447 "src/y.tab.c"
    break;

  case 815: /* filedescriptorsprocess: IF FILEDESCRIPTORS operator value PERCENT rate1 THEN action1 recovery_success  */
#line 3072 "src/p.y"
                                                                                                {
                        if (systeminfo.statisticsAvailable & Statistics_FiledescriptorsPerProcessMax)
                                addfiledescriptors((yyvsp[-6].number), false, -1LL, (yyvsp[-5].real), (yyvsp[-1].number), (yyvsp[0].number));
                        else
                                yywarning("The per-process filedescriptors maximum is not exposed on this system, so we cannot compute usage %%, please use the test with absolute value\n");
                  }
#line 7458 "src/y.tab.c"
    break;

  case 816: /* filedescriptorsprocesstotal: IF TOTAL FILEDESCRIPTORS operator NUMBER rate1 THEN action1 recovery_success  */
#line 3080 "src/p.y"
                                                                                                           {
                        if (systeminfo.statisticsAvailable & Statistics_FiledescriptorsPerProcess)
                                addfiledescriptors((yyvsp[-5].number), true, (long long)(yyvsp[-4].number), -1., (yyvsp[-1].number), (yyvsp[0].number));
                        else
                                yywarning("The per-process filedescriptors statistics is not available on this system\n");
                  }
#line 7469 "src/y.tab.c"
    break;

  case 817: /* gid: IF FAILED GID STRING rate1 THEN action1 recovery_success  */
#line 3088 "src/p.y"
                                                                           {
                        gidset.gid = get_gid((yyvsp[-4].string), 0);
                        addeventaction(&(gidset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        current->gid = addgid(&gidset);
                        FREE((yyvsp[-4].string));
                  }
#line 7480 "src/y.tab.c"
    break;

  case 818: /* gid: IF FAILED GID NUMBER rate1 THEN action1 recovery_success  */
#line 3094 "src/p.y"
                                                                           {
                        gidset.gid = get_gid(NULL, (yyvsp[-4].number));
                        addeventaction(&(gidset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        current->gid = addgid(&gidset);
                  }
#line 7490 "src/y.tab.c"
    break;

  case 819: /* linkstatus: IF FAILED LINK rate1 THEN action1 recovery_success  */
#line 3101 "src/p.y"
                                                                  { /* Deprecated */
                        addeventaction(&(linkstatusset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addlinkstatus(current, &linkstatusset);
                  }
#line 7499 "src/y.tab.c"
    break;

  case 820: /* linkstatus: IF LINK DOWN rate1 THEN action1 recovery_success  */
#line 3105 "src/p.y"
                                                                {
                        linkstatusset.check_invers = false;
                        addeventaction(&(linkstatusset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addlinkstatus(current, &linkstatusset);
                  }
#line 7509 "src/y.tab.c"
    break;

  case 821: /* linkstatus: IF LINK UP rate1 THEN action1 recovery_failure  */
#line 3110 "src/p.y"
                                                              {
                        linkstatusset.check_invers = true;
                        addeventaction(&(linkstatusset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addlinkstatus(current, &linkstatusset);
                  }
#line 7519 "src/y.tab.c"
    break;

  case 822: /* linkspeed: IF CHANGED LINK rate1 THEN action1 recovery_success  */
#line 3117 "src/p.y"
                                                                   {
                        addeventaction(&(linkspeedset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addlinkspeed(current, &linkspeedset);
                  }
#line 7528 "src/y.tab.c"
    break;

  case 823: /* linksaturation: IF SATURATION operator NUMBER PERCENT rate1 THEN action1 recovery_success  */
#line 3122 "src/p.y"
                                                                                           {
                        linksaturationset.operator = (yyvsp[-6].number);
                        linksaturationset.limit = (unsigned long long)(yyvsp[-5].number);
                        addeventaction(&(linksaturationset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addlinksaturation(current, &linksaturationset);
                  }
#line 7539 "src/y.tab.c"
    break;

  case 824: /* upload: IF UPLOAD operator NUMBER unit currenttime rate1 THEN action1 recovery_success  */
#line 3130 "src/p.y"
                                                                                                 {
                        bandwidthset.operator = (yyvsp[-7].number);
                        bandwidthset.limit = ((unsigned long long)(yyvsp[-6].number) * (yyvsp[-5].number));
                        bandwidthset.rangecount = 1;
                        bandwidthset.range = (yyvsp[-4].number);
                        addeventaction(&(bandwidthset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addbandwidth(&(current->uploadbyteslist), &bandwidthset);
                  }
#line 7552 "src/y.tab.c"
    break;

  case 825: /* upload: IF TOTAL UPLOAD operator NUMBER unit totaltime rate1 THEN action1 recovery_success  */
#line 3138 "src/p.y"
                                                                                                     {
                        bandwidthset.operator = (yyvsp[-7].number);
                        bandwidthset.limit = ((unsigned long long)(yyvsp[-6].number) * (yyvsp[-5].number));
                        bandwidthset.rangecount = 1;
                        bandwidthset.range = (yyvsp[-4].number);
                        addeventaction(&(bandwidthset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addbandwidth(&(current->uploadbyteslist), &bandwidthset);
                  }
#line 7565 "src/y.tab.c"
    break;

  case 826: /* upload: IF TOTAL UPLOAD operator NUMBER unit NUMBER totaltime rate1 THEN action1 recovery_success  */
#line 3146 "src/p.y"
                                                                                                            {
                        bandwidthset.operator = (yyvsp[-8].number);
                        bandwidthset.limit = ((unsigned long long)(yyvsp[-7].number) * (yyvsp[-6].number));
                        bandwidthset.rangecount = (yyvsp[-5].number);
                        bandwidthset.range = (yyvsp[-4].number);
                        addeventaction(&(bandwidthset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addbandwidth(&(current->uploadbyteslist), &bandwidthset);
                  }
#line 7578 "src/y.tab.c"
    break;

  case 827: /* upload: IF UPLOAD operator NUMBER PACKET currenttime rate1 THEN action1 recovery_success  */
#line 3154 "src/p.y"
                                                                                                   {
                        bandwidthset.operator = (yyvsp[-7].number);
                        bandwidthset.limit = (unsigned long long)(yyvsp[-6].number);
                        bandwidthset.rangecount = 1;
                        bandwidthset.range = (yyvsp[-4].number);
                        addeventaction(&(bandwidthset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addbandwidth(&(current->uploadpacketslist), &bandwidthset);
                  }
#line 7591 "src/y.tab.c"
    break;

  case 828: /* upload: IF TOTAL UPLOAD operator NUMBER PACKET totaltime rate1 THEN action1 recovery_success  */
#line 3162 "src/p.y"
                                                                                                       {
                        bandwidthset.operator = (yyvsp[-7].number);
                        bandwidthset.limit = (unsigned long long)(yyvsp[-6].number);
                        bandwidthset.rangecount = 1;
                        bandwidthset.range = (yyvsp[-4].number);
                        addeventaction(&(bandwidthset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addbandwidth(&(current->uploadpacketslist), &bandwidthset);
                  }
#line 7604 "src/y.tab.c"
    break;

  case 829: /* upload: IF TOTAL UPLOAD operator NUMBER PACKET NUMBER totaltime rate1 THEN action1 recovery_success  */
#line 3170 "src/p.y"
                                                                                                              {
                        bandwidthset.operator = (yyvsp[-8].number);
                        bandwidthset.limit = (unsigned long long)(yyvsp[-7].number);
                        bandwidthset.rangecount = (yyvsp[-5].number);
                        bandwidthset.range = (yyvsp[-4].number);
                        addeventaction(&(bandwidthset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addbandwidth(&(current->uploadpacketslist), &bandwidthset);
                  }
#line 7617 "src/y.tab.c"
    break;

  case 830: /* download: IF DOWNLOAD operator NUMBER unit currenttime rate1 THEN action1 recovery_success  */
#line 3180 "src/p.y"
                                                                                                   {
                        bandwidthset.operator = (yyvsp[-7].number);
                        bandwidthset.limit = ((unsigned long long)(yyvsp[-6].number) * (yyvsp[-5].number));
                        bandwidthset.rangecount = 1;
                        bandwidthset.range = (yyvsp[-4].number);
                        addeventaction(&(bandwidthset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addbandwidth(&(current->downloadbyteslist), &bandwidthset);
                  }
#line 7630 "src/y.tab.c"
    break;

  case 831: /* download: IF TOTAL DOWNLOAD operator NUMBER unit totaltime rate1 THEN action1 recovery_success  */
#line 3188 "src/p.y"
                                                                                                       {
                        bandwidthset.operator = (yyvsp[-7].number);
                        bandwidthset.limit = ((unsigned long long)(yyvsp[-6].number) * (yyvsp[-5].number));
                        bandwidthset.rangecount = 1;
                        bandwidthset.range = (yyvsp[-4].number);
                        addeventaction(&(bandwidthset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addbandwidth(&(current->downloadbyteslist), &bandwidthset);
                  }
#line 7643 "src/y.tab.c"
    break;

  case 832: /* download: IF TOTAL DOWNLOAD operator NUMBER unit NUMBER totaltime rate1 THEN action1 recovery_success  */
#line 3196 "src/p.y"
                                                                                                              {
                        bandwidthset.operator = (yyvsp[-8].number);
                        bandwidthset.limit = ((unsigned long long)(yyvsp[-7].number) * (yyvsp[-6].number));
                        bandwidthset.rangecount = (yyvsp[-5].number);
                        bandwidthset.range = (yyvsp[-4].number);
                        addeventaction(&(bandwidthset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addbandwidth(&(current->downloadbyteslist), &bandwidthset);
                  }
#line 7656 "src/y.tab.c"
    break;

  case 833: /* download: IF DOWNLOAD operator NUMBER PACKET currenttime rate1 THEN action1 recovery_success  */
#line 3204 "src/p.y"
                                                                                                     {
                        bandwidthset.operator = (yyvsp[-7].number);
                        bandwidthset.limit = (unsigned long long)(yyvsp[-6].number);
                        bandwidthset.rangecount = 1;
                        bandwidthset.range = (yyvsp[-4].number);
                        addeventaction(&(bandwidthset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addbandwidth(&(current->downloadpacketslist), &bandwidthset);
                  }
#line 7669 "src/y.tab.c"
    break;

  case 834: /* download: IF TOTAL DOWNLOAD operator NUMBER PACKET totaltime rate1 THEN action1 recovery_success  */
#line 3212 "src/p.y"
                                                                                                         {
                        bandwidthset.operator = (yyvsp[-7].number);
                        bandwidthset.limit = (unsigned long long)(yyvsp[-6].number);
                        bandwidthset.rangecount = 1;
                        bandwidthset.range = (yyvsp[-4].number);
                        addeventaction(&(bandwidthset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addbandwidth(&(current->downloadpacketslist), &bandwidthset);
                  }
#line 7682 "src/y.tab.c"
    break;

  case 835: /* download: IF TOTAL DOWNLOAD operator NUMBER PACKET NUMBER totaltime rate1 THEN action1 recovery_success  */
#line 3220 "src/p.y"
                                                                                                                {
                        bandwidthset.operator = (yyvsp[-8].number);
                        bandwidthset.limit = (unsigned long long)(yyvsp[-7].number);
                        bandwidthset.rangecount = (yyvsp[-5].number);
                        bandwidthset.range = (yyvsp[-4].number);
                        addeventaction(&(bandwidthset).action, (yyvsp[-1].number), (yyvsp[0].number));
                        addbandwidth(&(current->downloadpacketslist), &bandwidthset);
                  }
#line 7695 "src/y.tab.c"
    break;

  case 836: /* icmptype: TYPE ICMPECHO  */
#line 3230 "src/p.y"
                                { (yyval.number) = ICMP_ECHO; }
#line 7701 "src/y.tab.c"
    break;

  case 837: /* reminder: %empty  */
#line 3233 "src/p.y"
                                        { mailset.reminder = 0; }
#line 7707 "src/y.tab.c"
    break;

  case 838: /* reminder: REMINDER NUMBER  */
#line 3234 "src/p.y"
                                        { mailset.reminder = (yyvsp[0].number); }
#line 7713 "src/y.tab.c"
    break;

  case 839: /* reminder: REMINDER NUMBER CYCLE  */
#line 3235 "src/p.y"
                                        { mailset.reminder = (yyvsp[-1].number); }
#line 7719 "src/y.tab.c"
    break;


#line 7723 "src/y.tab.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 3238 "src/p.y"



/* -------------------------------------------------------- Parser interface */


/**
 * Syntactic error routine
 *
 * This routine is automatically called by the lexer!
 */
void yyerror(const char *s, ...) {
        ASSERT(s);
        char *msg = NULL;
        va_list ap;
        va_start(ap, s);
        msg = Str_vcat(s, ap);
        va_end(ap);
        Log_error("%s:%i: %s '%s'\n", currentfile, lineno, msg, yytext);
        cfg_errflag++;
        FREE(msg);
}


/**
 * Syntactical warning routine
 */
void yywarning(const char *s, ...) {
        ASSERT(s);
        char *msg = NULL;
        va_list ap;
        va_start(ap, s);
        msg = Str_vcat(s, ap);
        va_end(ap);
        Log_warning("%s:%i: %s '%s'\n", currentfile, lineno, msg, yytext);
        FREE(msg);
}


/**
 * Argument error routine
 */
void yyerror2(const char *s, ...) {
        ASSERT(s);
        char *msg = NULL;
        va_list ap;
        va_start(ap, s);
        msg = Str_vcat(s, ap);
        va_end(ap);
        Log_error("%s:%i: %s '%s'\n", argcurrentfile, arglineno, msg, argyytext);
        cfg_errflag++;
        FREE(msg);
}


/**
 * Argument warning routine
 */
void yywarning2(const char *s, ...) {
        ASSERT(s);
        char *msg = NULL;
        va_list ap;
        va_start(ap, s);
        msg = Str_vcat(s, ap);
        va_end(ap);
        Log_warning("%s:%i: %s '%s'\n", argcurrentfile, arglineno, msg, argyytext);
        FREE(msg);
}


/*
 * The Parser hook - start parsing the control file
 * Returns true if parsing succeeded, otherwise false
 */
bool parse(char *controlfile) {
        ASSERT(controlfile);

        if ((yyin = fopen(controlfile,"r")) == (FILE *)NULL) {
                Log_error("Cannot open the control file '%s' -- %s\n", controlfile, STRERROR);
                return false;
        }

        currentfile = Str_dup(controlfile);

        available_statistics(&systeminfo);

        /*
         * Creation of the global service list is synchronized
         */
        LOCK(Run.mutex)
        {
                preparse();
                yyparse();
                fclose(yyin);
                postparse();
        }
        END_LOCK;

        FREE(currentfile);

        if (argyytext != NULL)
                FREE(argyytext);

        /*
         * Secure check the monitrc file. The run control file must have the
         * same uid as the REAL uid of this process, it must have permissions
         * no greater than 700 and it must not be a symbolic link.
         */
        if (! file_checkStat(controlfile, "control file", S_IRUSR|S_IWUSR|S_IXUSR))
                return false;

        return cfg_errflag == 0;
}


/* ----------------------------------------------------------------- Private */


/**
 * Initialize objects used by the parser.
 */
static void preparse() {
        servicelist = tail = current = NULL;
        /* Set instance incarnation ID */
        time(&Run.incarnation);
        /* Reset lexer */
        buffer_stack_ptr            = 0;
        lineno                      = 1;
        arglineno                   = 1;
        argcurrentfile              = NULL;
        argyytext                   = NULL;
        /* Reset parser */
        Run.limits.sendExpectBuffer  = LIMIT_SENDEXPECTBUFFER;
        Run.limits.fileContentBuffer = LIMIT_FILECONTENTBUFFER;
        Run.limits.httpContentBuffer = LIMIT_HTTPCONTENTBUFFER;
        Run.limits.programOutput     = LIMIT_PROGRAMOUTPUT;
        Run.limits.networkTimeout    = LIMIT_NETWORKTIMEOUT;
        Run.limits.programTimeout    = LIMIT_PROGRAMTIMEOUT;
        Run.limits.stopTimeout       = LIMIT_STOPTIMEOUT;
        Run.limits.startTimeout      = LIMIT_STARTTIMEOUT;
        Run.limits.restartTimeout    = LIMIT_RESTARTTIMEOUT;
        Run.onreboot                 = Onreboot_Start;
        Run.mmonitcredentials        = NULL;
        Run.httpd.flags              = Httpd_Disabled | Httpd_Signature;
        Run.httpd.credentials        = NULL;
        memset(&(Run.httpd.socket), 0, sizeof(Run.httpd.socket));
        Run.mailserver_timeout       = SMTP_TIMEOUT;
        Run.eventlist_dir            = NULL;
        Run.eventlist_slots          = -1;
        Run.system                   = NULL;
        Run.mmonits                  = NULL;
        Run.maillist                 = NULL;
        Run.mailservers              = NULL;
        Run.MailFormat.from          = NULL;
        Run.MailFormat.replyto       = NULL;
        Run.MailFormat.subject       = NULL;
        Run.MailFormat.message       = NULL;
        depend_list                  = NULL;
        Run.flags |= Run_HandlerInit | Run_MmonitCredentials;
        for (int i = 0; i <= Handler_Max; i++)
                Run.handler_queue[i] = 0;

        /*
         * Initialize objects
         */
        reset_uidset();
        reset_gidset();
        reset_statusset();
        reset_sizeset();
        reset_mailset();
        reset_sslset();
        reset_mailserverset();
        reset_mmonitset();
        reset_responsetimeset();
        reset_portset();
        reset_permset();
        reset_icmpset();
        reset_linkstatusset();
        reset_linkspeedset();
        reset_linksaturationset();
        reset_bandwidthset();
        reset_rateset(&rate);
        reset_rateset(&rate1);
        reset_rateset(&rate2);
        reset_filesystemset();
        reset_resourceset();
        reset_checksumset();
        reset_timestampset();
        reset_actionrateset();
}


/*
 * Check that values are reasonable after parsing
 */
static void postparse() {
        if (cfg_errflag)
                return;

        /* If defined - add the last service to the service list */
        if (current) {
                addservice(current);
                current = NULL;
        }

        /* Check that we do not start monit in daemon mode without having a poll time */
        if (! Run.polltime && ((Run.flags & Run_Daemon) || (Run.flags & Run_Foreground))) {
                Log_error("Poll time is invalid or not defined. Please define poll time in the control file\nas a number (> 0)  or use the -d option when starting monit\n");
                cfg_errflag++;
        }

        if (Run.files.log)
                Run.flags |= Run_Log;

        /* Add the default general system service if not specified explicitly: service name default to hostname */
        if (! Run.system) {
                char hostname[STRLEN];
                if (gethostname(hostname, sizeof(hostname))) {
                        Log_error("Cannot get system hostname -- please add 'check system <name>'\n");
                        cfg_errflag++;
                }
                if (Util_existService(hostname)) {
                        Log_error("'check system' not defined in control file, failed to add automatic configuration (service name %s is used already) -- please add 'check system <name>' manually\n", hostname);
                        cfg_errflag++;
                }
                Run.system = createservice(Service_System, Str_dup(hostname), NULL, check_system);
                addservice(Run.system);
        }
        addeventaction(&(Run.system->action_MONIT_START), Action_Start, Action_Ignored);
        addeventaction(&(Run.system->action_MONIT_STOP), Action_Stop,  Action_Ignored);

        if (Run.mmonits) {
                if (Run.httpd.flags & Httpd_Net) {
                        if (Run.flags & Run_MmonitCredentials) {
                                Auth_T c;
                                for (c = Run.httpd.credentials; c; c = c->next) {
                                        if (c->digesttype == Digest_Cleartext && ! c->is_readonly) {
                                                Run.mmonitcredentials = c;
                                                break;
                                        }
                                }
                                if (! Run.mmonitcredentials)
                                        Log_warning("M/Monit registration with credentials enabled, but no suitable credentials found in monit configuration file -- please add 'allow user:password' option to 'set httpd' statement\n");
                        }
                } else if (Run.httpd.flags & Httpd_Unix) {
                        Log_warning("M/Monit enabled but Monit httpd is using unix socket -- please change 'set httpd' statement to use TCP port in order to be able to manage services on Monit\n");
                } else {
                        Log_warning("M/Monit enabled but no httpd allowed -- please add 'set httpd' statement\n");
                }
        }

        /* Check the sanity of any dependency graph */
        check_depend();

#if defined HAVE_OPENSSL && defined OPENSSL_FIPS
        Ssl_setFipsMode(Run.flags & Run_FipsEnabled);
#endif

        Processor_setHttpPostLimit();
}


static bool _parseOutgoingAddress(char *ip, Outgoing_T *outgoing) {
        struct addrinfo *result, hints = {.ai_flags = AI_NUMERICHOST};
        int status = getaddrinfo(ip, NULL, &hints, &result);
        if (status == 0) {
                outgoing->ip = ip;
                outgoing->addrlen = result->ai_addrlen;
                memcpy(&(outgoing->addr), result->ai_addr, result->ai_addrlen);
                freeaddrinfo(result);
                return true;
        } else {
                yyerror2("IP address parsing failed for %s -- %s", ip, status == EAI_SYSTEM ? STRERROR : gai_strerror(status));
        }
        return false;
}


/*
 * Create a new service object and add any current objects to the
 * service list.
 */
static Service_T createservice(Service_Type type, char *name, char *value, State_Type (*check)(Service_T s)) {
        ASSERT(name);

        check_name(name);

        if (current)
                addservice(current);

        NEW(current);
        current->type = type;
        switch (type) {
                case Service_Directory:
                        NEW(current->inf.directory);
                        break;
                case Service_Fifo:
                        NEW(current->inf.fifo);
                        break;
                case Service_File:
                        NEW(current->inf.file);
                        break;
                case Service_Filesystem:
                        NEW(current->inf.filesystem);
                        break;
                case Service_Net:
                        NEW(current->inf.net);
                        break;
                case Service_Process:
                        NEW(current->inf.process);
                        break;
                default:
                        break;
        }
        Util_resetInfo(current);

        if (type == Service_Program) {
                NEW(current->program);
                current->program->args = command;
                command = NULL;
                current->program->timeout = Run.limits.programTimeout;
        }

        /* Set default values */
        current->onrebootRestored = false;
        current->mode     = Monitor_Active;
        current->monitor  = Monitor_Init;
        current->onreboot = Run.onreboot;
        current->name     = name;
        current->name_urlescaped = Util_urlEncode(name, false);
        current->name_htmlescaped = escapeHTML(StringBuffer_create(16), name);
        current->check    = check;
        current->path     = value;

        /* Initialize general event handlers */
        addeventaction(&(current)->action_DATA,     Action_Alert,     Action_Alert);
        addeventaction(&(current)->action_EXEC,     Action_Alert,     Action_Alert);
        addeventaction(&(current)->action_INVALID,  Action_Restart,   Action_Alert);

        /* Initialize internal event handlers */
        addeventaction(&(current)->action_ACTION,       Action_Alert, Action_Ignored);

        gettimeofday(&current->collected, NULL);

        return current;
}


/*
 * Add a service object to the servicelist
 */
static void addservice(Service_T s) {
        ASSERT(s);

        // Test sanity check
        switch (s->type) {
                case Service_Host:
                        // Verify that a remote service has a port or an icmp list
                        if (! s->portlist && ! s->icmplist) {
                                Log_error("'check host' statement is incomplete: Please specify a port number to test\n or an icmp test at the remote host: '%s'\n", s->name);
                                cfg_errflag++;
                        }
                        break;
                case Service_Program:
                        // Verify that a program test has a status test
                        if (! s->statuslist && ! s->matchlist) {
                                Log_error("'check program %s' is incomplete: Please add a 'status' or 'content' test\n", s->name);
                                cfg_errflag++;
                        }
                        char program[PATH_MAX];
                        strncpy(program, s->program->args->arg[0], sizeof(program) - 1);
                        // Require that the program exist before creating the Command object
                        if (File_isExecutable(program)) {
                                s->program->C = Command_new(program, NULL);
                                for (int i = 1; i < s->program->args->length; i++) {
                                        Command_appendArgument(s->program->C, s->program->args->arg[i]);
                                        snprintf(program + strlen(program), sizeof(program) - strlen(program) - 1, " %s", s->program->args->arg[i]);
                                }
                                s->path = Str_dup(program);
                                if (s->program->args->has_uid)
                                        Command_setUid(s->program->C, s->program->args->uid);
                                if (s->program->args->has_gid)
                                        Command_setGid(s->program->C, s->program->args->gid);
                                // Set environment
                                Command_setEnv(s->program->C, "MONIT_SERVICE", s->name);
                        } else {
                                Log_error("A 'check program' statement requires the program to exist '%s'\n", program);
                                cfg_errflag++;
                        }
                        break;
                case Service_Net:
                        if (! s->linkstatuslist) {
                                // Add link status test if not defined
                                addeventaction(&(linkstatusset).action, Action_Alert, Action_Alert);
                                addlinkstatus(s, &linkstatusset);
                        }
                        break;
                case Service_Filesystem:
                        if (! s->nonexistlist && ! s->existlist) {
                                // Add non-existence test if not defined
                                addeventaction(&(nonexistset).action, Action_Restart, Action_Alert);
                                addnonexist(&nonexistset);
                        }
                        if (! s->fsflaglist) {
                                // Add filesystem flags change test if not defined
                                addeventaction(&(fsflagset).action, Action_Alert, Action_Ignored);
                                addfsflag(&fsflagset);
                        }
                        break;
                case Service_Directory:
                case Service_Fifo:
                case Service_File:
                case Service_Process:
                        if (! s->nonexistlist && ! s->existlist) {
                                // Add existence test if not defined
                                addeventaction(&(nonexistset).action, Action_Restart, Action_Alert);
                                addnonexist(&nonexistset);
                        }
                        break;
                default:
                        break;
        }

        // No "every" statement was used, monitor each cycle
        if (s->every.type == Every_Initializing)
                s->every.type = Every_Cycle;

        /* Add the service to the end of the service list */
        if (tail != NULL) {
                tail->next = s;
                tail->next_conf = s;
        } else {
                servicelist = s;
                servicelist_conf = s;
        }
        tail = s;
}


/*
 * Add entry to service group list
 */
static void addservicegroup(char *name) {
        ServiceGroup_T g;

        ASSERT(name);

        /* Check if service group with the same name is defined already */
        for (g = servicegrouplist; g; g = g->next)
                if (IS(g->name, name))
                        break;

        if (! g) {
                NEW(g);
                g->name = Str_dup(name);
                g->members = List_new();
                g->next = servicegrouplist;
                servicegrouplist = g;
        }

        List_append(g->members, current);
}


/*
 * Add a dependant entry to the current service dependant list
 */
static void adddependant(char *dependant) {
        Dependant_T d;

        ASSERT(dependant);

        NEW(d);

        if (current->dependantlist)
                d->next = current->dependantlist;

        d->dependant = dependant;
        d->dependant_urlescaped = Util_urlEncode(dependant, false);
        d->dependant_htmlescaped = escapeHTML(StringBuffer_create(16), dependant);
        current->dependantlist = d;

}


/*
 * Add the given mailaddress with the appropriate alert notification
 * values and mail attributes to the given mailinglist.
 */
static void addmail(char *mailto, Mail_T f, Mail_T *l) {
        Mail_T m;

        ASSERT(mailto);

        NEW(m);
        m->to       = mailto;
        m->from     = f->from;
        m->replyto  = f->replyto;
        m->subject  = f->subject;
        m->message  = f->message;
        m->events   = f->events;
        m->reminder = f->reminder;

        m->next = *l;
        *l = m;

        reset_mailset();
}


/*
 * Add the given portset to the current service's portlist
 */
static void addport(Port_T *list, Port_T port) {
        ASSERT(port);

        if (port->protocol->check == check_radius && port->type != Socket_Udp)
                yyerror("Radius protocol test supports UDP only");

        Port_T p;
        NEW(p);
        p->is_available       = Connection_Init;
        p->check_invers       = port->check_invers;
        p->type               = port->type;
        p->socket             = port->socket;
        p->family             = port->family;
        p->action             = port->action;
        p->timeout            = port->timeout;
        p->retry              = port->retry;
        p->protocol           = port->protocol;
        p->hostname           = port->hostname;
        p->url_request        = port->url_request;
        p->outgoing           = port->outgoing;

        if (p->family == Socket_Unix) {
                p->target.unix.pathname = port->target.unix.pathname;
        } else {
                p->target.net.port = port->target.net.port;
                if (sslset.flags) {
#ifdef HAVE_OPENSSL
                        p->target.net.ssl.certificate.minimumDays = port->target.net.ssl.certificate.minimumDays;
                        if (sslset.flags && (p->target.net.port == 25 || p->target.net.port == 143 || p->target.net.port == 587))
                                sslset.flags = SSL_StartTLS;
                        _setSSLOptions(&(p->target.net.ssl.options));
#else
                        yyerror("SSL check cannot be activated -- Monit was not built with SSL support");
#endif
                }
        }
        memcpy(&p->parameters, &port->parameters, sizeof(port->parameters));

        if (p->protocol->check == check_http) {
                if (p->parameters.http.checksum) {
                        cleanup_hash_string(p->parameters.http.checksum);
                        if (strlen(p->parameters.http.checksum) == 32)
                                p->parameters.http.hashtype = Hash_Md5;
                        else if (strlen(p->parameters.http.checksum) == 40)
                                p->parameters.http.hashtype = Hash_Sha1;
                        else
                                yyerror2("invalid checksum [%s]", p->parameters.http.checksum);
                } else {
                        p->parameters.http.hashtype = Hash_Unknown;
                }
                if (! p->parameters.http.method) {
                        p->parameters.http.method = Http_Get;
                } else if (p->parameters.http.method == Http_Head) {
                        // Sanity check: if content or checksum test is used, the method Http_Head is not allowed, as we need the content
                        if ((p->url_request && p->url_request->regex) || p->parameters.http.checksum) {
                                yyerror2("if response content or checksum test is enabled, the HEAD method is not allowed");
                        }
                }
        } else if (p->protocol->check == check_mysql) {
                if (p->parameters.mysql.rsaChecksum) {
                        if (! p->parameters.mysql.username)
                                yyerror2("the rsakey checksum test requires credentials to be defined");
                        if (p->target.net.ssl.options.flags != SSL_Disabled)
                                yyerror2("the rsakey checksum test can be used just with unsecured mysql protocol");
                }
        }

        p->responsetime.limit    = responsetimeset.limit;
        p->responsetime.current  = responsetimeset.current;
        p->responsetime.operator = responsetimeset.operator;

        p->next = *list;
        *list = p;

        reset_sslset();
        reset_responsetimeset();
        reset_portset();

}


static void addhttpheader(Port_T port, char *header) {
        if (! port->parameters.http.headers) {
                port->parameters.http.headers = List_new();
        }
        if (Str_startsWith(header, "Connection:") && ! Str_sub(header, "close")) {
                yywarning("We don't recommend setting the Connection header. Monit will always close the connection even if 'keep-alive' is set\n");
        }
        List_append(port->parameters.http.headers, header);
}


/*
 * Add a new resource object to the current service resource list
 */
static void addresource(Resource_T rr) {
        ASSERT(rr);
        if (Run.flags & Run_ProcessEngineEnabled) {
                Resource_T r;
                NEW(r);
                r->resource_id = rr->resource_id;
                r->limit       = rr->limit;
                r->action      = rr->action;
                r->operator    = rr->operator;
                r->next        = current->resourcelist;
                current->resourcelist = r;
        } else {
                yywarning("Cannot activate service check. The process status engine was disabled. On certain systems you must run monit as root to utilize this feature)\n");
        }
        reset_resourceset();
}


/*
 * Add a new file object to the current service timestamp list
 */
static void addtimestamp(Timestamp_T ts) {
        ASSERT(ts);

        Timestamp_T t;
        NEW(t);
        t->type         = ts->type;
        t->operator     = ts->operator;
        t->time         = ts->time;
        t->action       = ts->action;
        t->test_changes = ts->test_changes;

        t->next = current->timestamplist;
        current->timestamplist = t;

        reset_timestampset();
}


/*
 * Add a new object to the current service actionrate list
 */
static void addactionrate(ActionRate_T ar) {
        ActionRate_T a;

        ASSERT(ar);

        if (ar->count > ar->cycle)
                yyerror2("The number of restarts must be less than poll cycles");
        if (ar->count <= 0 || ar->cycle <= 0)
                yyerror2("Zero or negative values not allowed in a action rate statement");

        NEW(a);
        a->count  = ar->count;
        a->cycle  = ar->cycle;
        a->action = ar->action;

        a->next = current->actionratelist;
        current->actionratelist = a;

        reset_actionrateset();
}



/*
 * Add a new Size object to the current service size list
 */
static void addsize(Size_T ss) {
        Size_T s;
        struct stat buf;

        ASSERT(ss);

        NEW(s);
        s->operator     = ss->operator;
        s->size         = ss->size;
        s->action       = ss->action;
        s->test_changes = ss->test_changes;
        /* Get the initial size for future comparison, if the file exists */
        if (s->test_changes) {
                s->initialized = ! stat(current->path, &buf);
                if (s->initialized)
                        s->size = (unsigned long long)buf.st_size;
        }

        s->next = current->sizelist;
        current->sizelist = s;

        reset_sizeset();
}


/*
 * Add a new Uptime object to the current service uptime list
 */
static void adduptime(Uptime_T uu) {
        Uptime_T u;

        ASSERT(uu);

        NEW(u);
        u->operator = uu->operator;
        u->uptime = uu->uptime;
        u->action = uu->action;

        u->next = current->uptimelist;
        current->uptimelist = u;

        reset_uptimeset();
}


/*
 * Add a new Pid object to the current service pid list
 */
static void addpid(Pid_T pp) {
        ASSERT(pp);

        Pid_T p;
        NEW(p);
        p->action = pp->action;

        p->next = current->pidlist;
        current->pidlist = p;

        reset_pidset();
}


/*
 * Add a new PPid object to the current service ppid list
 */
static void addppid(Pid_T pp) {
        ASSERT(pp);

        Pid_T p;
        NEW(p);
        p->action = pp->action;

        p->next = current->ppidlist;
        current->ppidlist = p;

        reset_ppidset();
}


/*
 * Add a new Fsflag object to the current service fsflag list
 */
static void addfsflag(FsFlag_T ff) {
        ASSERT(ff);

        FsFlag_T f;
        NEW(f);
        f->action = ff->action;

        f->next = current->fsflaglist;
        current->fsflaglist = f;

        reset_fsflagset();
}


/*
 * Add a new Nonexist object to the current service list
 */
static void addnonexist(NonExist_T ff) {
        ASSERT(ff);

        NonExist_T f;
        NEW(f);
        f->action = ff->action;

        f->next = current->nonexistlist;
        current->nonexistlist = f;

        reset_nonexistset();
}


static void addexist(Exist_T rule) {
        ASSERT(rule);
        Exist_T r;
        NEW(r);
        r->action = rule->action;
        r->next = current->existlist;
        current->existlist = r;
        reset_existset();
}


/*
 * Set Checksum object in the current service
 */
static void addchecksum(Checksum_T cs) {
        ASSERT(cs);

        cs->initialized = true;

        if (STR_UNDEF(cs->hash)) {
                if (cs->type == Hash_Unknown)
                        cs->type = Hash_Default;
                if (! (Checksum_getChecksum(current->path, cs->type, cs->hash, sizeof(cs->hash)))) {
                        /* If the file doesn't exist, set dummy value */
                        snprintf(cs->hash, sizeof(cs->hash), cs->type == Hash_Md5 ? "00000000000000000000000000000000" : "0000000000000000000000000000000000000000");
                        cs->initialized = false;
                        yywarning2("Cannot compute a checksum for file %s", current->path);
                }
        }

        int len = cleanup_hash_string(cs->hash);
        if (cs->type == Hash_Unknown) {
                if (len == 32) {
                        cs->type = Hash_Md5;
                } else if (len == 40) {
                        cs->type = Hash_Sha1;
                } else {
                        yyerror2("Unknown checksum type [%s] for file %s", cs->hash, current->path);
                        reset_checksumset();
                        return;
                }
        } else if ((cs->type == Hash_Md5 && len != 32) || (cs->type == Hash_Sha1 && len != 40)) {
                yyerror2("Invalid checksum [%s] for file %s", cs->hash, current->path);
                reset_checksumset();
                return;
        }

        Checksum_T c;
        NEW(c);
        c->type         = cs->type;
        c->test_changes = cs->test_changes;
        c->initialized  = cs->initialized;
        c->action       = cs->action;
        snprintf(c->hash, sizeof(c->hash), "%s", cs->hash);

        current->checksum = c;

        reset_checksumset();

}


/*
 * Set Perm object in the current service
 */
static void addperm(Perm_T ps) {
        ASSERT(ps);

        Perm_T p;
        NEW(p);
        p->action = ps->action;
        p->test_changes = ps->test_changes;
        if (p->test_changes) {
                if (! File_exist(current->path))
                        DEBUG("The path '%s' used in the PERMISSION statement refer to a non-existing object\n", current->path);
                else if ((p->perm = File_mod(current->path)) < 0)
                        yyerror2("Cannot get the timestamp for '%s'", current->path);
                else
                        p->perm &= 07777;
        } else {
                p->perm = ps->perm;
        }
        current->perm = p;
        reset_permset();
}


static void addlinkstatus(Service_T s, LinkStatus_T L) {
        ASSERT(L);

        LinkStatus_T l;

        // Sanity check: we don't support link up/down tests mix
        for (l = s->linkstatuslist; l; l = l->next) {
                if (l->check_invers != L->check_invers)
                        yyerror2("Mixing link up and down tests is not supported");
        }
                        
        if (L->check_invers)
                s->inverseStatus = true;

        NEW(l);
        l->check_invers = L->check_invers;
        l->action = L->action;

        l->next = s->linkstatuslist;
        s->linkstatuslist = l;

        reset_linkstatusset();
}


static void addlinkspeed(Service_T s, LinkSpeed_T L) {
        ASSERT(L);

        LinkSpeed_T l;
        NEW(l);
        l->action = L->action;

        l->next = s->linkspeedlist;
        s->linkspeedlist = l;

        reset_linkspeedset();
}


static void addlinksaturation(Service_T s, LinkSaturation_T L) {
        ASSERT(L);

        LinkSaturation_T l;
        NEW(l);
        l->operator = L->operator;
        l->limit = L->limit;
        l->action = L->action;

        l->next = s->linksaturationlist;
        s->linksaturationlist = l;

        reset_linksaturationset();
}


/*
 * Return Bandwidth object
 */
static void addbandwidth(Bandwidth_T *list, Bandwidth_T b) {
        ASSERT(list);
        ASSERT(b);

        if (b->rangecount * b->range > 24 * Time_Hour) {
                yyerror2("Maximum range for total test is 24 hours");
        } else if (b->range == Time_Minute && b->rangecount > 60) {
                yyerror2("Maximum value for [minute(s)] unit is 60");
        } else if (b->range == Time_Hour && b->rangecount > 24) {
                yyerror2("Maximum value for [hour(s)] unit is 24");
        } else if (b->range == Time_Day && b->rangecount > 1) {
                yyerror2("Maximum value for [day(s)] unit is 1");
        } else {
                if (b->range == Time_Day) {
                        // translate last day -> last 24 hours
                        b->rangecount = 24;
                        b->range = Time_Hour;
                }
                Bandwidth_T bandwidth;
                NEW(bandwidth);
                bandwidth->operator = b->operator;
                bandwidth->limit = b->limit;
                bandwidth->rangecount = b->rangecount;
                bandwidth->range = b->range;
                bandwidth->action = b->action;
                bandwidth->next = *list;
                *list = bandwidth;
        }
        reset_bandwidthset();
}


static void appendmatch(Match_T *list, Match_T item) {
        if (*list) {
                /* Find the end of the list (keep the same patterns order as in the config file) */
                Match_T last;
                for (last = *list; last->next; last = last->next)
                        ;
                last->next = item;
        } else {
                *list = item;
        }
}


/*
 * Set Match object in the current service
 */
static void addmatch(Match_T ms, int actionnumber, int linenumber) {
        Match_T m;

        ASSERT(ms);

        NEW(m);
        NEW(m->regex_comp);

        m->match_string = ms->match_string;
        m->match_path   = ms->match_path ? Str_dup(ms->match_path) : NULL;
        m->action       = ms->action;
        m->not          = ms->not;
        m->ignore       = ms->ignore;
        m->next         = NULL;

        addeventaction(&(m->action), actionnumber, Action_Ignored);

        int reg_return = regcomp(m->regex_comp, ms->match_string, REG_NOSUB|REG_EXTENDED);

        if (reg_return != 0) {
                char errbuf[STRLEN];
                regerror(reg_return, ms->regex_comp, errbuf, STRLEN);
                if (m->match_path != NULL)
                        yyerror2("Regex parsing error: %s on line %i of", errbuf, linenumber);
                else
                        yyerror2("Regex parsing error: %s", errbuf);
        }
        appendmatch(m->ignore ? &current->matchignorelist : &current->matchlist, m);
}


static void addmatchpath(Match_T ms, Action_Type actionnumber) {
        ASSERT(ms->match_path);

        FILE *handle = fopen(ms->match_path, "r");
        if (handle == NULL) {
                yyerror2("Cannot read regex match file (%s)", ms->match_path);
                return;
        }

        // The addeventaction() called from addmatch() will reset the command1 to NULL, but we need to duplicate the command for each line, thus need to save it here
        command_t savecommand = command1;
        for (int linenumber = 1; ! feof(handle); linenumber++) {
                char buf[2048];

                if (! fgets(buf, sizeof(buf), handle))
                        continue;

                size_t len = strlen(buf);

                if (len == 0 || buf[0] == '\n')
                        continue;

                if (buf[len - 1] == '\n')
                        buf[len - 1] = 0;

                ms->match_string = Str_dup(buf);

                if (actionnumber == Action_Exec) {
                        if (command1 == NULL) {
                                ASSERT(savecommand);
                                command1 = copycommand(savecommand);
                        }
                }

                addmatch(ms, actionnumber, linenumber);
        }
        if (actionnumber == Action_Exec && savecommand)
                gccmd(&savecommand);

        fclose(handle);
}


/*
 * Set exit status test object in the current service
 */
static void addstatus(Status_T status) {
        Status_T s;
        ASSERT(status);
        NEW(s);
        s->initialized = status->initialized;
        s->return_value = status->return_value;
        s->operator = status->operator;
        s->action = status->action;
        s->next = current->statuslist;
        current->statuslist = s;

        reset_statusset();
}


/*
 * Set Uid object in the current service
 */
static Uid_T adduid(Uid_T u) {
        ASSERT(u);

        Uid_T uid;
        NEW(uid);
        uid->uid = u->uid;
        uid->action = u->action;
        reset_uidset();
        return uid;
}


/*
 * Set Gid object in the current service
 */
static Gid_T addgid(Gid_T g) {
        ASSERT(g);

        Gid_T gid;
        NEW(gid);
        gid->gid = g->gid;
        gid->action = g->action;
        reset_gidset();
        return gid;
}


/*
 * Add a new filesystem to the current service's filesystem list
 */
static void addfilesystem(FileSystem_T ds) {
        FileSystem_T dev;

        ASSERT(ds);

        NEW(dev);
        dev->resource           = ds->resource;
        dev->operator           = ds->operator;
        dev->limit_absolute     = ds->limit_absolute;
        dev->limit_percent      = ds->limit_percent;
        dev->action             = ds->action;

        dev->next               = current->filesystemlist;
        current->filesystemlist = dev;

        reset_filesystemset();

}


/*
 * Add a new icmp object to the current service's icmp list
 */
static void addicmp(Icmp_T is) {
        Icmp_T icmp;

        ASSERT(is);

        NEW(icmp);
        icmp->family        = is->family;
        icmp->type          = is->type;
        icmp->size          = is->size;
        icmp->count         = is->count;
        icmp->timeout       = is->timeout;
        icmp->action        = is->action;
        icmp->outgoing      = is->outgoing;
        icmp->check_invers  = is->check_invers;
        icmp->is_available  = Connection_Init;

        icmp->responsetime.limit    = responsetimeset.limit;
        icmp->responsetime.current  = responsetimeset.current;
        icmp->responsetime.operator = responsetimeset.operator;

        icmp->next          = current->icmplist;
        current->icmplist   = icmp;

        reset_responsetimeset();
        reset_icmpset();
}


/*
 * Set EventAction object
 */
static void addeventaction(EventAction_T *_ea, Action_Type failed, Action_Type succeeded) {
        EventAction_T ea;

        ASSERT(_ea);

        NEW(ea);
        NEW(ea->failed);
        NEW(ea->succeeded);

        ea->failed->id = failed;
        ea->failed->repeat = repeat1;
        ea->failed->count = rate1.count;
        ea->failed->cycles = rate1.cycles;
        if (failed == Action_Exec) {
                ASSERT(command1);
                ea->failed->exec = command1;
                command1 = NULL;
        }

        ea->succeeded->id = succeeded;
        ea->succeeded->repeat = repeat2;
        ea->succeeded->count = rate2.count;
        ea->succeeded->cycles = rate2.cycles;
        if (succeeded == Action_Exec) {
                ASSERT(command2);
                ea->succeeded->exec = command2;
                command2 = NULL;
        }
        *_ea = ea;
        reset_rateset(&rate);
        reset_rateset(&rate1);
        reset_rateset(&rate2);
        repeat = repeat1 = repeat2 = 0;
}


/*
 * Add a generic protocol handler to
 */
static void addgeneric(Port_T port, char *send, char *expect) {
        Generic_T g = port->parameters.generic.sendexpect;
        if (! g) {
                NEW(g);
                port->parameters.generic.sendexpect = g;
        } else {
                while (g->next)
                        g = g->next;
                NEW(g->next);
                g = g->next;
        }
        if (send) {
                g->send = send;
                g->expect = NULL;
        } else if (expect) {
                int reg_return;
                NEW(g->expect);
                reg_return = regcomp(g->expect, expect, REG_NOSUB|REG_EXTENDED);
                FREE(expect);
                if (reg_return != 0) {
                        char errbuf[STRLEN];
                        regerror(reg_return, g->expect, errbuf, STRLEN);
                        yyerror2("Regex parsing error: %s", errbuf);
                }
                g->send = NULL;
        }
}


/*
 * Add the current command object to the current service object's
 * start or stop program.
 */
static void addcommand(int what, unsigned int timeout) {

        switch (what) {
                case START:   current->start = command; break;
                case STOP:    current->stop = command; break;
                case RESTART: current->restart = command; break;
        }

        command->timeout = timeout;

        command = NULL;

}


/*
 * Add a new argument to the argument list
 */
static void addargument(char *argument) {

        ASSERT(argument);

        if (! command) {
                check_exec(argument);
                NEW(command);
        }

        command->arg[command->length++] = argument;
        command->arg[command->length] = NULL;

        if (command->length >= ARGMAX)
                yyerror("Exceeded maximum number of program arguments");

}


/*
 * Setup a url request for the current port object
 */
static void prepare_urlrequest(URL_T U) {

        ASSERT(U);

        /* Only the HTTP protocol is supported for URLs currently. See also the lexer if this is to be changed in the future */
        portset.protocol = Protocol_get(Protocol_HTTP);

        if (urlrequest == NULL)
                NEW(urlrequest);
        urlrequest->url = U;
        portset.hostname = Str_dup(U->hostname);
        portset.target.net.port = U->port;
        portset.url_request = urlrequest;
        portset.type = Socket_Tcp;
        portset.parameters.http.request = Str_cat("%s%s%s", U->path, U->query ? "?" : "", U->query ? U->query : "");
        if (IS(U->protocol, "https"))
                sslset.flags = SSL_Enabled;
}


/*
 * Set the url request for a port
 */
static void  seturlrequest(int operator, char *regex) {

        ASSERT(regex);

        if (! urlrequest)
                NEW(urlrequest);
        urlrequest->operator = operator;
        int reg_return;
        NEW(urlrequest->regex);
        reg_return = regcomp(urlrequest->regex, regex, REG_NOSUB|REG_EXTENDED);
        if (reg_return != 0) {
                char errbuf[STRLEN];
                regerror(reg_return, urlrequest->regex, errbuf, STRLEN);
                yyerror2("Regex parsing error: %s", errbuf);
        }
}


/*
 * Add a new data recipient server to the mmonit server list
 */
static void addmmonit(Mmonit_T mmonit) {
        ASSERT(mmonit->url);

        Mmonit_T c;
        NEW(c);
        c->url = mmonit->url;
        c->compress = MmonitCompress_Init;
        _setSSLOptions(&(c->ssl));
        if (IS(c->url->protocol, "https")) {
#ifdef HAVE_OPENSSL
                c->ssl.flags = SSL_Enabled;
#else
                yyerror("SSL check cannot be activated -- SSL disabled");
#endif
        }
        c->timeout = mmonit->timeout;
        c->next = NULL;

        if (Run.mmonits) {
                Mmonit_T C;
                for (C = Run.mmonits; C->next; C = C->next)
                        /* Empty */ ;
                C->next = c;
        } else {
                Run.mmonits = c;
        }
        reset_sslset();
        reset_mmonitset();
}


/*
 * Add a new smtp server to the mail server list
 */
static void addmailserver(MailServer_T mailserver) {

        MailServer_T s;

        ASSERT(mailserver->host);

        NEW(s);
        s->host        = mailserver->host;
        s->port        = mailserver->port;
        s->username    = mailserver->username;
        s->password    = mailserver->password;

        if (sslset.flags && (mailserver->port == 25 || mailserver->port == 587))
                sslset.flags = SSL_StartTLS;
        _setSSLOptions(&(s->ssl));

        s->next = NULL;

        if (Run.mailservers) {
                MailServer_T l;
                for (l = Run.mailservers; l->next; l = l->next)
                        /* empty */;
                l->next = s;
        } else {
                Run.mailservers = s;
        }
        reset_mailserverset();
}


/*
 * Return uid if found on the system. If the parameter user is NULL
 * the uid parameter is used for looking up the user id on the system,
 * otherwise the user parameter is used.
 */
static uid_t get_uid(char *user, uid_t uid) {
        char buf[4096];
        struct passwd pwd, *result = NULL;
        if (user) {
                if (getpwnam_r(user, &pwd, buf, sizeof(buf), &result) != 0 || ! result) {
                        yyerror2("Requested user not found on the system");
                        return(0);
                }
        } else {
                if (getpwuid_r(uid, &pwd, buf, sizeof(buf), &result) != 0 || ! result) {
                        yyerror2("Requested uid not found on the system");
                        return(0);
                }
        }
        return(pwd.pw_uid);
}


/*
 * Return gid if found on the system. If the parameter group is NULL
 * the gid parameter is used for looking up the group id on the system,
 * otherwise the group parameter is used.
 */
static gid_t get_gid(char *group, gid_t gid) {
        struct group *grd;

        if (group) {
                grd = getgrnam(group);

                if (! grd) {
                        yyerror2("Requested group not found on the system");
                        return(0);
                }

        } else {

                if (! (grd = getgrgid(gid))) {
                        yyerror2("Requested gid not found on the system");
                        return(0);
                }

        }

        return(grd->gr_gid);

}


/*
 * Add a new user id to the current command object.
 */
static void addeuid(uid_t uid) {
        if (! getuid()) {
                command->has_uid = true;
                command->uid = uid;
        } else {
                yyerror("UID statement requires root privileges");
        }
}


/*
 * Add a new group id to the current command object.
 */
static void addegid(gid_t gid) {
        if (! getuid()) {
                command->has_gid = true;
                command->gid = gid;
        } else {
                yyerror("GID statement requires root privileges");
        }
}


/*
 * Reset the logfile if changed
 */
static void setlogfile(char *logfile) {
        if (Run.files.log) {
                if (IS(Run.files.log, logfile)) {
                        FREE(logfile);
                        return;
                } else {
                        FREE(Run.files.log);
                }
        }
        Run.files.log = logfile;
}


/*
 * Reset the pidfile if changed
 */
static void setpidfile(char *pidfile) {
        if (Run.files.pid) {
                if (IS(Run.files.pid, pidfile)) {
                        FREE(pidfile);
                        return;
                } else {
                        FREE(Run.files.pid);
                }
        }
        Run.files.pid = pidfile;
}


/*
 * Read a apache htpasswd file and add credentials found for username
 */
static void addhtpasswdentry(char *filename, char *username, Digest_Type dtype) {
        char *ht_username = NULL;
        char *ht_passwd = NULL;
        char buf[STRLEN];
        FILE *handle = NULL;
        int credentials_added = 0;

        ASSERT(filename);

        handle = fopen(filename, "r");

        if (handle == NULL) {
                if (username != NULL)
                        yyerror2("Cannot read htpasswd (%s) for user %s", filename, username);
                else
                        yyerror2("Cannot read htpasswd (%s)", filename);
                return;
        }

        while (! feof(handle)) {
                char *colonindex = NULL;

                if (! fgets(buf, STRLEN, handle))
                        continue;

                Str_rtrim(buf);
                Str_curtail(buf, "#");

                if (NULL == (colonindex = strchr(buf, ':')))
                continue;

                ht_passwd = Str_dup(colonindex+1);
                *colonindex = '\0';

                /* In case we have a file in /etc/passwd or /etc/shadow style we
                 *  want to remove ":.*$" and Crypt and MD5 hashed dont have a colon
                 */

                if ((NULL != (colonindex = strchr(ht_passwd, ':'))) && (dtype != Digest_Cleartext))
                        *colonindex = '\0';

                ht_username = Str_dup(buf);

                if (username == NULL) {
                        if (addcredentials(ht_username, ht_passwd, dtype, false))
                                credentials_added++;
                } else if (Str_cmp(username, ht_username) == 0)  {
                        if (addcredentials(ht_username, ht_passwd, dtype, false))
                                credentials_added++;
                } else {
                        FREE(ht_passwd);
                        FREE(ht_username);
                }
        }

        if (credentials_added == 0) {
                if (username == NULL)
                        yywarning2("htpasswd file (%s) has no usable credentials", filename);
                else
                        yywarning2("htpasswd file (%s) has no usable credentials for user %s", filename, username);
        }
        fclose(handle);
}


#ifdef HAVE_LIBPAM
static void addpamauth(char* groupname, int readonly) {
        Auth_T prev = NULL;

        ASSERT(groupname);

        if (! Run.httpd.credentials)
                NEW(Run.httpd.credentials);

        Auth_T c = Run.httpd.credentials;
        do {
                if (c->groupname != NULL && IS(c->groupname, groupname)) {
                        yywarning2("PAM group %s was added already, entry ignored", groupname);
                        FREE(groupname);
                        return;
                }
                prev = c;
                c = c->next;
        } while (c != NULL);

        NEW(prev->next);
        c = prev->next;

        c->next        = NULL;
        c->uname       = NULL;
        c->passwd      = NULL;
        c->groupname   = groupname;
        c->digesttype  = Digest_Pam;
        c->is_readonly = readonly;

        DEBUG("Adding PAM group '%s'\n", groupname);

        return;
}
#endif


/*
 * Add Basic Authentication credentials
 */
static bool addcredentials(char *uname, char *passwd, Digest_Type dtype, bool readonly) {
        Auth_T c;

        ASSERT(uname);
        ASSERT(passwd);

        if (strlen(passwd) > Str_compareConstantTimeStringLength) {
                yyerror2("Password for user %s is too long, maximum %d allowed", uname, Str_compareConstantTimeStringLength);
                FREE(uname);
                FREE(passwd);
                return false;
        }

        if (! Run.httpd.credentials) {
                NEW(Run.httpd.credentials);
                c = Run.httpd.credentials;
        } else {
                if (Util_getUserCredentials(uname) != NULL) {
                        yywarning2("Credentials for user %s were already added, entry ignored", uname);
                        FREE(uname);
                        FREE(passwd);
                        return false;
                }
                c = Run.httpd.credentials;
                while (c->next != NULL)
                        c = c->next;
                NEW(c->next);
                c = c->next;
        }

        c->next        = NULL;
        c->uname       = uname;
        c->passwd      = passwd;
        c->groupname   = NULL;
        c->digesttype  = dtype;
        c->is_readonly = readonly;

        DEBUG("Adding credentials for user '%s'\n", uname);

        return true;

}


/*
 * Set the syslog and the facilities to be used
 */
static void setsyslog(char *facility) {

        if (! Run.files.log || ihp.logfile) {
                ihp.logfile = true;
                setlogfile(Str_dup("syslog"));
                Run.flags |= Run_UseSyslog;
                Run.flags |= Run_Log;
        }

        if (facility) {
                if (IS(facility,"log_local0"))
                        Run.facility = LOG_LOCAL0;
                else if (IS(facility, "log_local1"))
                        Run.facility = LOG_LOCAL1;
                else if (IS(facility, "log_local2"))
                        Run.facility = LOG_LOCAL2;
                else if (IS(facility, "log_local3"))
                        Run.facility = LOG_LOCAL3;
                else if (IS(facility, "log_local4"))
                        Run.facility = LOG_LOCAL4;
                else if (IS(facility, "log_local5"))
                        Run.facility = LOG_LOCAL5;
                else if (IS(facility, "log_local6"))
                        Run.facility = LOG_LOCAL6;
                else if (IS(facility, "log_local7"))
                        Run.facility = LOG_LOCAL7;
                else if (IS(facility, "log_daemon"))
                        Run.facility = LOG_DAEMON;
                else
                        yyerror2("Invalid syslog facility");
        } else {
                Run.facility = LOG_USER;
        }

}


/*
 * Reset the current sslset for reuse
 */
static void reset_sslset() {
        memset(&sslset, 0, sizeof(struct SslOptions_T));
        sslset.version = sslset.verify = sslset.allowSelfSigned = -1;
}


/*
 * Reset the current mailset for reuse
 */
static void reset_mailset() {
        memset(&mailset, 0, sizeof(struct Mail_T));
}


/*
 * Reset the mailserver set to default values
 */
static void reset_mailserverset() {
        memset(&mailserverset, 0, sizeof(struct MailServer_T));
        mailserverset.port = PORT_SMTP;
}


/*
 * Reset the mmonit set to default values
 */
static void reset_mmonitset() {
        memset(&mmonitset, 0, sizeof(struct Mmonit_T));
        mmonitset.timeout = Run.limits.networkTimeout;
}


/*
 * Reset the Port set to default values
 */
static void reset_portset() {
        memset(&portset, 0, sizeof(struct Port_T));
        portset.check_invers = false;
        portset.socket = -1;
        portset.type = Socket_Tcp;
        portset.family = Socket_Ip;
        portset.timeout = Run.limits.networkTimeout;
        portset.retry = 1;
        portset.protocol = Protocol_get(Protocol_DEFAULT);
        urlrequest = NULL;
}


/*
 * Reset the Proc set to default values
 */
static void reset_resourceset() {
        resourceset.resource_id = 0;
        resourceset.limit = 0;
        resourceset.action = NULL;
        resourceset.operator = Operator_Equal;
}


/*
 * Reset the Timestamp set to default values
 */
static void reset_timestampset() {
        timestampset.type = Timestamp_Default;
        timestampset.operator = Operator_Equal;
        timestampset.time = 0;
        timestampset.test_changes = false;
        timestampset.initialized = false;
        timestampset.action = NULL;
}


/*
 * Reset the ActionRate set to default values
 */
static void reset_actionrateset() {
        actionrateset.count = 0;
        actionrateset.cycle = 0;
        actionrateset.action = NULL;
}


/*
 * Reset the Size set to default values
 */
static void reset_sizeset() {
        sizeset.operator = Operator_Equal;
        sizeset.size = 0;
        sizeset.test_changes = false;
        sizeset.action = NULL;
}


/*
 * Reset the Uptime set to default values
 */
static void reset_uptimeset() {
        uptimeset.operator = Operator_Equal;
        uptimeset.uptime = 0;
        uptimeset.action = NULL;
}


static void reset_responsetimeset() {
        responsetimeset.operator = Operator_Less;
        responsetimeset.current = 0.;
        responsetimeset.limit = -1.;
}


static void reset_linkstatusset() {
        linkstatusset.check_invers = false;
        linkstatusset.action = NULL;
}


static void reset_linkspeedset() {
        linkspeedset.action = NULL;
}


static void reset_linksaturationset() {
        linksaturationset.limit = 0.;
        linksaturationset.operator = Operator_Equal;
        linksaturationset.action = NULL;
}


/*
 * Reset the Bandwidth set to default values
 */
static void reset_bandwidthset() {
        bandwidthset.operator = Operator_Equal;
        bandwidthset.limit = 0ULL;
        bandwidthset.action = NULL;
}


/*
 * Reset the Pid set to default values
 */
static void reset_pidset() {
        pidset.action = NULL;
}


/*
 * Reset the PPid set to default values
 */
static void reset_ppidset() {
        ppidset.action = NULL;
}


/*
 * Reset the Fsflag set to default values
 */
static void reset_fsflagset() {
        fsflagset.action = NULL;
}


/*
 * Reset the Nonexist set to default values
 */
static void reset_nonexistset() {
        nonexistset.action = NULL;
}


static void reset_existset() {
        existset.action = NULL;
}


/*
 * Reset the Checksum set to default values
 */
static void reset_checksumset() {
        checksumset.type         = Hash_Unknown;
        checksumset.test_changes = false;
        checksumset.action       = NULL;
        *checksumset.hash        = 0;
}


/*
 * Reset the Perm set to default values
 */
static void reset_permset() {
        permset.test_changes = false;
        permset.perm = 0;
        permset.action = NULL;
}


/*
 * Reset the Status set to default values
 */
static void reset_statusset() {
        statusset.initialized = false;
        statusset.return_value = 0;
        statusset.operator = Operator_Equal;
        statusset.action = NULL;
}


/*
 * Reset the Uid set to default values
 */
static void reset_uidset() {
        uidset.uid = 0;
        uidset.action = NULL;
}


/*
 * Reset the Gid set to default values
 */
static void reset_gidset() {
        gidset.gid = 0;
        gidset.action = NULL;
}


/*
 * Reset the Filesystem set to default values
 */
static void reset_filesystemset() {
        filesystemset.resource = 0;
        filesystemset.operator = Operator_Equal;
        filesystemset.limit_absolute = -1;
        filesystemset.limit_percent = -1.;
        filesystemset.action = NULL;
}


/*
 * Reset the ICMP set to default values
 */
static void reset_icmpset() {
        icmpset.type = ICMP_ECHO;
        icmpset.size = ICMP_SIZE;
        icmpset.count = ICMP_ATTEMPT_COUNT;
        icmpset.timeout = Run.limits.networkTimeout;
        icmpset.check_invers = false;
        icmpset.action = NULL;
}


/*
 * Reset the Rate set to default values
 */
static void reset_rateset(struct rate_t *r) {
        r->count = 1;
        r->cycles = 1;
}


/* ---------------------------------------------------------------- Checkers */


/*
 * Check for unique service name
 */
static void check_name(char *name) {
        ASSERT(name);

        if (Util_existService(name) || (current && IS(name, current->name)))
                yyerror2("Service name conflict, %s already defined", name);
        if (name && *name == '/')
                yyerror2("Service name '%s' must not start with '/' -- ", name);
}


/*
 * Permission statement semantic check
 */
static int check_perm(int perm) {
        int result;
        char *status;
        char buf[STRLEN];

        snprintf(buf, STRLEN, "%d", perm);

        result = (int)strtol(buf, &status, 8);

        if (*status != '\0' || result < 0 || result > 07777)
                yyerror2("Permission statements must have an octal value between 0 and 7777");

        return result;
}


/*
 * Check the dependency graph for errors
 * by doing a topological sort, thereby finding any cycles.
 * Assures that graph is a Directed Acyclic Graph (DAG).
 */
static void check_depend() {
        Service_T depends_on = NULL;
        Service_T* dlt = &depend_list; /* the current tail of it                                 */
        bool done;                /* no unvisited nodes left?                               */
        bool found_some;          /* last iteration found anything new ?                    */
        depend_list = NULL;            /* depend_list will be the topological sorted servicelist */

        do {
                done = true;
                found_some = false;
                for (Service_T s = servicelist; s; s = s->next) {
                        Dependant_T d;
                        if (s->visited)
                                continue;
                        done = false; // still unvisited nodes
                        depends_on = NULL;
                        for (d = s->dependantlist; d; d = d->next) {
                                Service_T dp = Util_getService(d->dependant);
                                if (! dp) {
                                        Log_error("Depending service '%s' is not defined in the control file\n", d->dependant);
                                        exit(1);
                                }
                                if (! dp->visited) {
                                        depends_on = dp;
                                }
                        }

                        if (! depends_on) {
                                s->visited = true;
                                found_some = true;
                                *dlt = s;
                                dlt = &s->next_depend;
                        }
                }
        } while (found_some && ! done);

        if (! done) {
                ASSERT(depends_on);
                Log_error("Found a depend loop in the control file involving the service '%s'\n", depends_on->name);
                exit(1);
        }

        ASSERT(depend_list);
        servicelist = depend_list;

        for (Service_T s = depend_list; s; s = s->next_depend)
                s->next = s->next_depend;
}


// Check and warn if the executable does not exist
static void check_exec(char *exec) {
        if (! File_exist(exec))
                yywarning2("Program does not exist:");
        else if (! File_isExecutable(exec))
                yywarning2("Program is not executable:");
}


/* Return a valid max forward value for SIP header */
static int verifyMaxForward(int mf) {
        if (mf == 0) {
                return INT_MAX; // Differentiate uninitialized (0) and explicit zero
        } else if (mf > 0 && mf <= 255) {
                return mf;
        }
        yywarning2("SIP max forward is outside the range [0..255]. Setting max forward to 70");
        return 70;
}


/* -------------------------------------------------------------------- Misc */


/*
 * Cleans up a hash string, tolower and remove byte separators
 */
static int cleanup_hash_string(char *hashstring) {
        int i = 0, j = 0;

        ASSERT(hashstring);

        while (hashstring[i]) {
                if (isxdigit((int)hashstring[i])) {
                        hashstring[j] = tolower((int)hashstring[i]);
                        j++;
                }
                i++;
        }
        hashstring[j] = 0;
        return j;
}


/* Return deep copy of the command */
static command_t copycommand(command_t source) {
        int i;
        command_t copy = NULL;

        NEW(copy);
        copy->length = source->length;
        copy->has_uid = source->has_uid;
        copy->uid = source->uid;
        copy->has_gid = source->has_gid;
        copy->gid = source->gid;
        copy->timeout = source->timeout;
        for (i = 0; i < copy->length; i++)
                copy->arg[i] = Str_dup(source->arg[i]);
        copy->arg[copy->length] = NULL;

        return copy;
}


static void _setPEM(char **store, char *path, const char *description, bool isFile) {
        if (*store) {
                yyerror2("Duplicate %s", description);
                FREE(path);
        } else if (! File_exist(path)) {
                yyerror2("%s doesn't exist", description);
                FREE(path);
        } else if (! (isFile ? File_isFile(path) : File_isDirectory(path))) {
                yyerror2("%s is not a %s", description, isFile ? "file" : "directory");
                FREE(path);
        } else if (! File_isReadable(path)) {
                yyerror2("Cannot read %s", description);
                FREE(path);
        } else {
                sslset.flags = SSL_Enabled;
                *store = path;
        }
}


static void _setSSLOptions(SslOptions_T options) {
        options->allowSelfSigned = sslset.allowSelfSigned;
        options->CACertificateFile = sslset.CACertificateFile;
        options->CACertificatePath = sslset.CACertificatePath;
        options->checksum = sslset.checksum;
        options->checksumType = sslset.checksumType;
        options->ciphers = sslset.ciphers;
        options->clientpemfile = sslset.clientpemfile;
        options->flags = sslset.flags;
        options->pemfile = sslset.pemfile;
        options->pemchain = sslset.pemchain;
        options->pemkey = sslset.pemkey;
        options->verify = sslset.verify;
        options->version = sslset.version;
        reset_sslset();
}


#ifdef HAVE_OPENSSL
static void _setSSLVersion(short version) {
        sslset.flags = SSL_Enabled;
        if (sslset.version == -1)
                sslset.version = version;
        else
                sslset.version |= version;
}
#endif


static void _unsetSSLVersion(short version) {
        if (sslset.version != -1)
                sslset.version &= ~version;
}


static void addsecurityattribute(char *value, Action_Type failed, Action_Type succeeded) {
        SecurityAttribute_T attr;
        NEW(attr);
        addeventaction(&(attr->action), failed, succeeded);
        attr->attribute = value;
        attr->next = current->secattrlist;
        current->secattrlist = attr;
}


static void addfiledescriptors(Operator_Type operator, bool total, long long value_absolute, float value_percent, Action_Type failed, Action_Type succeeded) {
        Filedescriptors_T fds;
        NEW(fds);
        addeventaction(&(fds->action), failed, succeeded);
        fds->total = total;
        fds->limit_absolute = value_absolute;
        fds->limit_percent = value_percent;
        fds->operator = operator;
        fds->next = current->filedescriptorslist;
        current->filedescriptorslist = fds;
}

static void _sanityCheckEveryStatement(Service_T s) {
        if (s->every.type != Every_Initializing) {
                yywarning2("The 'every' statement can be specified only once, the last value will be used\n");
                switch (s->every.type) {
                        case Every_Cron:
                        case Every_NotInCron:
                                FREE(s->every.spec.cron);
                                break;
                        default:
                                break;
                }
        }
}

