/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_SRC_Y_TAB_H_INCLUDED
# define YY_YY_SRC_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    IF = 258,                      /* IF  */
    ELSE = 259,                    /* ELSE  */
    THEN = 260,                    /* THEN  */
    FAILED = 261,                  /* FAILED  */
    SET = 262,                     /* SET  */
    LOGFILE = 263,                 /* LOGFILE  */
    FACILITY = 264,                /* FACILITY  */
    DAEMON = 265,                  /* DAEMON  */
    SYSLOG = 266,                  /* SYSLOG  */
    MAILSERVER = 267,              /* MAILSERVER  */
    HTTPD = 268,                   /* HTTPD  */
    ALLOW = 269,                   /* ALLOW  */
    REJECTOPT = 270,               /* REJECTOPT  */
    ADDRESS = 271,                 /* ADDRESS  */
    INIT = 272,                    /* INIT  */
    TERMINAL = 273,                /* TERMINAL  */
    BATCH = 274,                   /* BATCH  */
    READONLY = 275,                /* READONLY  */
    CLEARTEXT = 276,               /* CLEARTEXT  */
    MD5HASH = 277,                 /* MD5HASH  */
    SHA1HASH = 278,                /* SHA1HASH  */
    CRYPT = 279,                   /* CRYPT  */
    DELAY = 280,                   /* DELAY  */
    PEMFILE = 281,                 /* PEMFILE  */
    PEMKEY = 282,                  /* PEMKEY  */
    PEMCHAIN = 283,                /* PEMCHAIN  */
    ENABLE = 284,                  /* ENABLE  */
    DISABLE = 285,                 /* DISABLE  */
    SSLTOKEN = 286,                /* SSLTOKEN  */
    CIPHER = 287,                  /* CIPHER  */
    CLIENTPEMFILE = 288,           /* CLIENTPEMFILE  */
    ALLOWSELFCERTIFICATION = 289,  /* ALLOWSELFCERTIFICATION  */
    SELFSIGNED = 290,              /* SELFSIGNED  */
    VERIFY = 291,                  /* VERIFY  */
    CERTIFICATE = 292,             /* CERTIFICATE  */
    CACERTIFICATEFILE = 293,       /* CACERTIFICATEFILE  */
    CACERTIFICATEPATH = 294,       /* CACERTIFICATEPATH  */
    VALID = 295,                   /* VALID  */
    INTERFACE = 296,               /* INTERFACE  */
    LINK = 297,                    /* LINK  */
    PACKET = 298,                  /* PACKET  */
    BYTEIN = 299,                  /* BYTEIN  */
    BYTEOUT = 300,                 /* BYTEOUT  */
    PACKETIN = 301,                /* PACKETIN  */
    PACKETOUT = 302,               /* PACKETOUT  */
    SPEED = 303,                   /* SPEED  */
    SATURATION = 304,              /* SATURATION  */
    UPLOAD = 305,                  /* UPLOAD  */
    DOWNLOAD = 306,                /* DOWNLOAD  */
    TOTAL = 307,                   /* TOTAL  */
    UP = 308,                      /* UP  */
    DOWN = 309,                    /* DOWN  */
    IDFILE = 310,                  /* IDFILE  */
    STATEFILE = 311,               /* STATEFILE  */
    SEND = 312,                    /* SEND  */
    EXPECT = 313,                  /* EXPECT  */
    CYCLE = 314,                   /* CYCLE  */
    COUNT = 315,                   /* COUNT  */
    REMINDER = 316,                /* REMINDER  */
    REPEAT = 317,                  /* REPEAT  */
    LIMITS = 318,                  /* LIMITS  */
    SENDEXPECTBUFFER = 319,        /* SENDEXPECTBUFFER  */
    EXPECTBUFFER = 320,            /* EXPECTBUFFER  */
    FILECONTENTBUFFER = 321,       /* FILECONTENTBUFFER  */
    HTTPCONTENTBUFFER = 322,       /* HTTPCONTENTBUFFER  */
    PROGRAMOUTPUT = 323,           /* PROGRAMOUTPUT  */
    NETWORKTIMEOUT = 324,          /* NETWORKTIMEOUT  */
    PROGRAMTIMEOUT = 325,          /* PROGRAMTIMEOUT  */
    STARTTIMEOUT = 326,            /* STARTTIMEOUT  */
    STOPTIMEOUT = 327,             /* STOPTIMEOUT  */
    RESTARTTIMEOUT = 328,          /* RESTARTTIMEOUT  */
    PIDFILE = 329,                 /* PIDFILE  */
    START = 330,                   /* START  */
    STOP = 331,                    /* STOP  */
    PATHTOK = 332,                 /* PATHTOK  */
    RSAKEY = 333,                  /* RSAKEY  */
    HOST = 334,                    /* HOST  */
    HOSTNAME = 335,                /* HOSTNAME  */
    PORT = 336,                    /* PORT  */
    IPV4 = 337,                    /* IPV4  */
    IPV6 = 338,                    /* IPV6  */
    TYPE = 339,                    /* TYPE  */
    UDP = 340,                     /* UDP  */
    TCP = 341,                     /* TCP  */
    TCPSSL = 342,                  /* TCPSSL  */
    PROTOCOL = 343,                /* PROTOCOL  */
    CONNECTION = 344,              /* CONNECTION  */
    ALERT = 345,                   /* ALERT  */
    NOALERT = 346,                 /* NOALERT  */
    MAILFORMAT = 347,              /* MAILFORMAT  */
    UNIXSOCKET = 348,              /* UNIXSOCKET  */
    SIGNATURE = 349,               /* SIGNATURE  */
    TIMEOUT = 350,                 /* TIMEOUT  */
    RETRY = 351,                   /* RETRY  */
    RESTART = 352,                 /* RESTART  */
    CHECKSUM = 353,                /* CHECKSUM  */
    EVERY = 354,                   /* EVERY  */
    NOTEVERY = 355,                /* NOTEVERY  */
    DEFAULT = 356,                 /* DEFAULT  */
    HTTP = 357,                    /* HTTP  */
    HTTPS = 358,                   /* HTTPS  */
    APACHESTATUS = 359,            /* APACHESTATUS  */
    FTP = 360,                     /* FTP  */
    SMTP = 361,                    /* SMTP  */
    SMTPS = 362,                   /* SMTPS  */
    POP = 363,                     /* POP  */
    POPS = 364,                    /* POPS  */
    IMAP = 365,                    /* IMAP  */
    IMAPS = 366,                   /* IMAPS  */
    CLAMAV = 367,                  /* CLAMAV  */
    NNTP = 368,                    /* NNTP  */
    NTP3 = 369,                    /* NTP3  */
    MYSQL = 370,                   /* MYSQL  */
    MYSQLS = 371,                  /* MYSQLS  */
    DNS = 372,                     /* DNS  */
    WEBSOCKET = 373,               /* WEBSOCKET  */
    MQTT = 374,                    /* MQTT  */
    SSH = 375,                     /* SSH  */
    DWP = 376,                     /* DWP  */
    LDAP2 = 377,                   /* LDAP2  */
    LDAP3 = 378,                   /* LDAP3  */
    RDATE = 379,                   /* RDATE  */
    RSYNC = 380,                   /* RSYNC  */
    TNS = 381,                     /* TNS  */
    PGSQL = 382,                   /* PGSQL  */
    POSTFIXPOLICY = 383,           /* POSTFIXPOLICY  */
    SIP = 384,                     /* SIP  */
    LMTP = 385,                    /* LMTP  */
    GPS = 386,                     /* GPS  */
    RADIUS = 387,                  /* RADIUS  */
    MEMCACHE = 388,                /* MEMCACHE  */
    REDIS = 389,                   /* REDIS  */
    MONGODB = 390,                 /* MONGODB  */
    SIEVE = 391,                   /* SIEVE  */
    SPAMASSASSIN = 392,            /* SPAMASSASSIN  */
    FAIL2BAN = 393,                /* FAIL2BAN  */
    STRING = 394,                  /* STRING  */
    PATH = 395,                    /* PATH  */
    MAILADDR = 396,                /* MAILADDR  */
    MAILFROM = 397,                /* MAILFROM  */
    MAILREPLYTO = 398,             /* MAILREPLYTO  */
    MAILSUBJECT = 399,             /* MAILSUBJECT  */
    MAILBODY = 400,                /* MAILBODY  */
    SERVICENAME = 401,             /* SERVICENAME  */
    STRINGNAME = 402,              /* STRINGNAME  */
    NUMBER = 403,                  /* NUMBER  */
    PERCENT = 404,                 /* PERCENT  */
    LOGLIMIT = 405,                /* LOGLIMIT  */
    CLOSELIMIT = 406,              /* CLOSELIMIT  */
    DNSLIMIT = 407,                /* DNSLIMIT  */
    KEEPALIVELIMIT = 408,          /* KEEPALIVELIMIT  */
    REPLYLIMIT = 409,              /* REPLYLIMIT  */
    REQUESTLIMIT = 410,            /* REQUESTLIMIT  */
    STARTLIMIT = 411,              /* STARTLIMIT  */
    WAITLIMIT = 412,               /* WAITLIMIT  */
    GRACEFULLIMIT = 413,           /* GRACEFULLIMIT  */
    CLEANUPLIMIT = 414,            /* CLEANUPLIMIT  */
    REAL = 415,                    /* REAL  */
    CHECKPROC = 416,               /* CHECKPROC  */
    CHECKFILESYS = 417,            /* CHECKFILESYS  */
    CHECKFILE = 418,               /* CHECKFILE  */
    CHECKDIR = 419,                /* CHECKDIR  */
    CHECKHOST = 420,               /* CHECKHOST  */
    CHECKSYSTEM = 421,             /* CHECKSYSTEM  */
    CHECKFIFO = 422,               /* CHECKFIFO  */
    CHECKPROGRAM = 423,            /* CHECKPROGRAM  */
    CHECKNET = 424,                /* CHECKNET  */
    THREADS = 425,                 /* THREADS  */
    CHILDREN = 426,                /* CHILDREN  */
    METHOD = 427,                  /* METHOD  */
    GET = 428,                     /* GET  */
    HEAD = 429,                    /* HEAD  */
    STATUS = 430,                  /* STATUS  */
    ORIGIN = 431,                  /* ORIGIN  */
    VERSIONOPT = 432,              /* VERSIONOPT  */
    READ = 433,                    /* READ  */
    WRITE = 434,                   /* WRITE  */
    OPERATION = 435,               /* OPERATION  */
    SERVICETIME = 436,             /* SERVICETIME  */
    DISK = 437,                    /* DISK  */
    RESOURCE = 438,                /* RESOURCE  */
    MEMORY = 439,                  /* MEMORY  */
    TOTALMEMORY = 440,             /* TOTALMEMORY  */
    LOADAVG1 = 441,                /* LOADAVG1  */
    LOADAVG5 = 442,                /* LOADAVG5  */
    LOADAVG15 = 443,               /* LOADAVG15  */
    SWAP = 444,                    /* SWAP  */
    MODE = 445,                    /* MODE  */
    ACTIVE = 446,                  /* ACTIVE  */
    PASSIVE = 447,                 /* PASSIVE  */
    MANUAL = 448,                  /* MANUAL  */
    ONREBOOT = 449,                /* ONREBOOT  */
    NOSTART = 450,                 /* NOSTART  */
    LASTSTATE = 451,               /* LASTSTATE  */
    CORE = 452,                    /* CORE  */
    CPU = 453,                     /* CPU  */
    TOTALCPU = 454,                /* TOTALCPU  */
    CPUUSER = 455,                 /* CPUUSER  */
    CPUSYSTEM = 456,               /* CPUSYSTEM  */
    CPUWAIT = 457,                 /* CPUWAIT  */
    CPUNICE = 458,                 /* CPUNICE  */
    CPUHARDIRQ = 459,              /* CPUHARDIRQ  */
    CPUSOFTIRQ = 460,              /* CPUSOFTIRQ  */
    CPUSTEAL = 461,                /* CPUSTEAL  */
    CPUGUEST = 462,                /* CPUGUEST  */
    CPUGUESTNICE = 463,            /* CPUGUESTNICE  */
    GROUP = 464,                   /* GROUP  */
    REQUEST = 465,                 /* REQUEST  */
    DEPENDS = 466,                 /* DEPENDS  */
    BASEDIR = 467,                 /* BASEDIR  */
    SLOT = 468,                    /* SLOT  */
    EVENTQUEUE = 469,              /* EVENTQUEUE  */
    SECRET = 470,                  /* SECRET  */
    HOSTHEADER = 471,              /* HOSTHEADER  */
    UID = 472,                     /* UID  */
    EUID = 473,                    /* EUID  */
    GID = 474,                     /* GID  */
    MMONIT = 475,                  /* MMONIT  */
    INSTANCE = 476,                /* INSTANCE  */
    USERNAME = 477,                /* USERNAME  */
    PASSWORD = 478,                /* PASSWORD  */
    DATABASE = 479,                /* DATABASE  */
    TIME = 480,                    /* TIME  */
    ATIME = 481,                   /* ATIME  */
    CTIME = 482,                   /* CTIME  */
    MTIME = 483,                   /* MTIME  */
    CHANGED = 484,                 /* CHANGED  */
    MILLISECOND = 485,             /* MILLISECOND  */
    SECOND = 486,                  /* SECOND  */
    MINUTE = 487,                  /* MINUTE  */
    HOUR = 488,                    /* HOUR  */
    DAY = 489,                     /* DAY  */
    MONTH = 490,                   /* MONTH  */
    SSLV2 = 491,                   /* SSLV2  */
    SSLV3 = 492,                   /* SSLV3  */
    TLSV1 = 493,                   /* TLSV1  */
    TLSV11 = 494,                  /* TLSV11  */
    TLSV12 = 495,                  /* TLSV12  */
    TLSV13 = 496,                  /* TLSV13  */
    CERTMD5 = 497,                 /* CERTMD5  */
    AUTO = 498,                    /* AUTO  */
    NOSSLV2 = 499,                 /* NOSSLV2  */
    NOSSLV3 = 500,                 /* NOSSLV3  */
    NOTLSV1 = 501,                 /* NOTLSV1  */
    NOTLSV11 = 502,                /* NOTLSV11  */
    NOTLSV12 = 503,                /* NOTLSV12  */
    NOTLSV13 = 504,                /* NOTLSV13  */
    BYTE = 505,                    /* BYTE  */
    KILOBYTE = 506,                /* KILOBYTE  */
    MEGABYTE = 507,                /* MEGABYTE  */
    GIGABYTE = 508,                /* GIGABYTE  */
    INODE = 509,                   /* INODE  */
    SPACE = 510,                   /* SPACE  */
    TFREE = 511,                   /* TFREE  */
    PERMISSION = 512,              /* PERMISSION  */
    SIZE = 513,                    /* SIZE  */
    MATCH = 514,                   /* MATCH  */
    NOT = 515,                     /* NOT  */
    IGNORE = 516,                  /* IGNORE  */
    ACTION = 517,                  /* ACTION  */
    UPTIME = 518,                  /* UPTIME  */
    RESPONSETIME = 519,            /* RESPONSETIME  */
    EXEC = 520,                    /* EXEC  */
    UNMONITOR = 521,               /* UNMONITOR  */
    PING = 522,                    /* PING  */
    PING4 = 523,                   /* PING4  */
    PING6 = 524,                   /* PING6  */
    ICMP = 525,                    /* ICMP  */
    ICMPECHO = 526,                /* ICMPECHO  */
    NONEXIST = 527,                /* NONEXIST  */
    EXIST = 528,                   /* EXIST  */
    INVALID = 529,                 /* INVALID  */
    DATA = 530,                    /* DATA  */
    RECOVERED = 531,               /* RECOVERED  */
    PASSED = 532,                  /* PASSED  */
    SUCCEEDED = 533,               /* SUCCEEDED  */
    URL = 534,                     /* URL  */
    CONTENT = 535,                 /* CONTENT  */
    PID = 536,                     /* PID  */
    PPID = 537,                    /* PPID  */
    FSFLAG = 538,                  /* FSFLAG  */
    REGISTER = 539,                /* REGISTER  */
    CREDENTIALS = 540,             /* CREDENTIALS  */
    URLOBJECT = 541,               /* URLOBJECT  */
    ADDRESSOBJECT = 542,           /* ADDRESSOBJECT  */
    TARGET = 543,                  /* TARGET  */
    TIMESPEC = 544,                /* TIMESPEC  */
    HTTPHEADER = 545,              /* HTTPHEADER  */
    MAXFORWARD = 546,              /* MAXFORWARD  */
    FIPS = 547,                    /* FIPS  */
    SECURITY = 548,                /* SECURITY  */
    ATTRIBUTE = 549,               /* ATTRIBUTE  */
    FILEDESCRIPTORS = 550,         /* FILEDESCRIPTORS  */
    GREATER = 551,                 /* GREATER  */
    GREATEROREQUAL = 552,          /* GREATEROREQUAL  */
    LESS = 553,                    /* LESS  */
    LESSOREQUAL = 554,             /* LESSOREQUAL  */
    EQUAL = 555,                   /* EQUAL  */
    NOTEQUAL = 556                 /* NOTEQUAL  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define IF 258
#define ELSE 259
#define THEN 260
#define FAILED 261
#define SET 262
#define LOGFILE 263
#define FACILITY 264
#define DAEMON 265
#define SYSLOG 266
#define MAILSERVER 267
#define HTTPD 268
#define ALLOW 269
#define REJECTOPT 270
#define ADDRESS 271
#define INIT 272
#define TERMINAL 273
#define BATCH 274
#define READONLY 275
#define CLEARTEXT 276
#define MD5HASH 277
#define SHA1HASH 278
#define CRYPT 279
#define DELAY 280
#define PEMFILE 281
#define PEMKEY 282
#define PEMCHAIN 283
#define ENABLE 284
#define DISABLE 285
#define SSLTOKEN 286
#define CIPHER 287
#define CLIENTPEMFILE 288
#define ALLOWSELFCERTIFICATION 289
#define SELFSIGNED 290
#define VERIFY 291
#define CERTIFICATE 292
#define CACERTIFICATEFILE 293
#define CACERTIFICATEPATH 294
#define VALID 295
#define INTERFACE 296
#define LINK 297
#define PACKET 298
#define BYTEIN 299
#define BYTEOUT 300
#define PACKETIN 301
#define PACKETOUT 302
#define SPEED 303
#define SATURATION 304
#define UPLOAD 305
#define DOWNLOAD 306
#define TOTAL 307
#define UP 308
#define DOWN 309
#define IDFILE 310
#define STATEFILE 311
#define SEND 312
#define EXPECT 313
#define CYCLE 314
#define COUNT 315
#define REMINDER 316
#define REPEAT 317
#define LIMITS 318
#define SENDEXPECTBUFFER 319
#define EXPECTBUFFER 320
#define FILECONTENTBUFFER 321
#define HTTPCONTENTBUFFER 322
#define PROGRAMOUTPUT 323
#define NETWORKTIMEOUT 324
#define PROGRAMTIMEOUT 325
#define STARTTIMEOUT 326
#define STOPTIMEOUT 327
#define RESTARTTIMEOUT 328
#define PIDFILE 329
#define START 330
#define STOP 331
#define PATHTOK 332
#define RSAKEY 333
#define HOST 334
#define HOSTNAME 335
#define PORT 336
#define IPV4 337
#define IPV6 338
#define TYPE 339
#define UDP 340
#define TCP 341
#define TCPSSL 342
#define PROTOCOL 343
#define CONNECTION 344
#define ALERT 345
#define NOALERT 346
#define MAILFORMAT 347
#define UNIXSOCKET 348
#define SIGNATURE 349
#define TIMEOUT 350
#define RETRY 351
#define RESTART 352
#define CHECKSUM 353
#define EVERY 354
#define NOTEVERY 355
#define DEFAULT 356
#define HTTP 357
#define HTTPS 358
#define APACHESTATUS 359
#define FTP 360
#define SMTP 361
#define SMTPS 362
#define POP 363
#define POPS 364
#define IMAP 365
#define IMAPS 366
#define CLAMAV 367
#define NNTP 368
#define NTP3 369
#define MYSQL 370
#define MYSQLS 371
#define DNS 372
#define WEBSOCKET 373
#define MQTT 374
#define SSH 375
#define DWP 376
#define LDAP2 377
#define LDAP3 378
#define RDATE 379
#define RSYNC 380
#define TNS 381
#define PGSQL 382
#define POSTFIXPOLICY 383
#define SIP 384
#define LMTP 385
#define GPS 386
#define RADIUS 387
#define MEMCACHE 388
#define REDIS 389
#define MONGODB 390
#define SIEVE 391
#define SPAMASSASSIN 392
#define FAIL2BAN 393
#define STRING 394
#define PATH 395
#define MAILADDR 396
#define MAILFROM 397
#define MAILREPLYTO 398
#define MAILSUBJECT 399
#define MAILBODY 400
#define SERVICENAME 401
#define STRINGNAME 402
#define NUMBER 403
#define PERCENT 404
#define LOGLIMIT 405
#define CLOSELIMIT 406
#define DNSLIMIT 407
#define KEEPALIVELIMIT 408
#define REPLYLIMIT 409
#define REQUESTLIMIT 410
#define STARTLIMIT 411
#define WAITLIMIT 412
#define GRACEFULLIMIT 413
#define CLEANUPLIMIT 414
#define REAL 415
#define CHECKPROC 416
#define CHECKFILESYS 417
#define CHECKFILE 418
#define CHECKDIR 419
#define CHECKHOST 420
#define CHECKSYSTEM 421
#define CHECKFIFO 422
#define CHECKPROGRAM 423
#define CHECKNET 424
#define THREADS 425
#define CHILDREN 426
#define METHOD 427
#define GET 428
#define HEAD 429
#define STATUS 430
#define ORIGIN 431
#define VERSIONOPT 432
#define READ 433
#define WRITE 434
#define OPERATION 435
#define SERVICETIME 436
#define DISK 437
#define RESOURCE 438
#define MEMORY 439
#define TOTALMEMORY 440
#define LOADAVG1 441
#define LOADAVG5 442
#define LOADAVG15 443
#define SWAP 444
#define MODE 445
#define ACTIVE 446
#define PASSIVE 447
#define MANUAL 448
#define ONREBOOT 449
#define NOSTART 450
#define LASTSTATE 451
#define CORE 452
#define CPU 453
#define TOTALCPU 454
#define CPUUSER 455
#define CPUSYSTEM 456
#define CPUWAIT 457
#define CPUNICE 458
#define CPUHARDIRQ 459
#define CPUSOFTIRQ 460
#define CPUSTEAL 461
#define CPUGUEST 462
#define CPUGUESTNICE 463
#define GROUP 464
#define REQUEST 465
#define DEPENDS 466
#define BASEDIR 467
#define SLOT 468
#define EVENTQUEUE 469
#define SECRET 470
#define HOSTHEADER 471
#define UID 472
#define EUID 473
#define GID 474
#define MMONIT 475
#define INSTANCE 476
#define USERNAME 477
#define PASSWORD 478
#define DATABASE 479
#define TIME 480
#define ATIME 481
#define CTIME 482
#define MTIME 483
#define CHANGED 484
#define MILLISECOND 485
#define SECOND 486
#define MINUTE 487
#define HOUR 488
#define DAY 489
#define MONTH 490
#define SSLV2 491
#define SSLV3 492
#define TLSV1 493
#define TLSV11 494
#define TLSV12 495
#define TLSV13 496
#define CERTMD5 497
#define AUTO 498
#define NOSSLV2 499
#define NOSSLV3 500
#define NOTLSV1 501
#define NOTLSV11 502
#define NOTLSV12 503
#define NOTLSV13 504
#define BYTE 505
#define KILOBYTE 506
#define MEGABYTE 507
#define GIGABYTE 508
#define INODE 509
#define SPACE 510
#define TFREE 511
#define PERMISSION 512
#define SIZE 513
#define MATCH 514
#define NOT 515
#define IGNORE 516
#define ACTION 517
#define UPTIME 518
#define RESPONSETIME 519
#define EXEC 520
#define UNMONITOR 521
#define PING 522
#define PING4 523
#define PING6 524
#define ICMP 525
#define ICMPECHO 526
#define NONEXIST 527
#define EXIST 528
#define INVALID 529
#define DATA 530
#define RECOVERED 531
#define PASSED 532
#define SUCCEEDED 533
#define URL 534
#define CONTENT 535
#define PID 536
#define PPID 537
#define FSFLAG 538
#define REGISTER 539
#define CREDENTIALS 540
#define URLOBJECT 541
#define ADDRESSOBJECT 542
#define TARGET 543
#define TIMESPEC 544
#define HTTPHEADER 545
#define MAXFORWARD 546
#define FIPS 547
#define SECURITY 548
#define ATTRIBUTE 549
#define FILEDESCRIPTORS 550
#define GREATER 551
#define GREATEROREQUAL 552
#define LESS 553
#define LESSOREQUAL 554
#define EQUAL 555
#define NOTEQUAL 556

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 325 "src/p.y"

        URL_T url;
        Address_T address;
        float real;
        int   number;
        char *string;

#line 677 "src/y.tab.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;


int yyparse (void);


#endif /* !YY_YY_SRC_Y_TAB_H_INCLUDED  */
